<?php
$totalMudancas = rand(1, 100);
$totalCash     = rand(1, 1000);
$totalM3       = rand(1, 1000);

?>
<div class="container ptb-20">
    <div class="row">
        <div class="col-md-12 pb-10">
            <h3 class="text-center">Mudança no período</h3>
        </div>
    </div>

    <div class="row">
        <div class="form-group col-md-2">
            <div class="input-group">
                <input type="text" class="form-control datepicker" id="date_start" placeholder="Início" value="<?php echo date('01/01/Y'); ?>">
                <div class="input-group-append">
                    <div class="input-group-text">
                        <i class="fa fa-calendar"></i>
                    </div>
                </div>
            </div>
        </div>

        <div class="form-group col-md-2">
            <div class="input-group">
                <input type="text" class="form-control datepicker" id="date_end" placeholder="Fim" value="<?php echo date('d/m/Y', strtotime('Last day of December')); ?>">
                <div class="input-group-append">
                    <div class="input-group-text">
                        <i class="fa fa-calendar"></i>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-md-2">
            <button type="button" class="btn btn-dark" id="filtrar">Filtrar</button>
        </div>

        <div class="col-md-6 text-right">
            <div class="dropdown show">
                <a class="dropdown-toggle fa fa-ellipsis-v" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"></a>

                <div class="dropdown-menu" aria-labelledby="dropdownMenuLink">
                    <a class="dropdown-item" href="#">Adicionar pedido</a>
                    <a class="dropdown-item" href="<?php echo base_url('?mod=dashboard&view=orcamentos'); ?>">Orçamentos anteriores</a>
                    <a class="dropdown-item" href="<?php echo base_url(''); ?>">Logout</a>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-8">
            <h4 class="text-center">Volume em m³</h4>
            <canvas id="canvas-volume-m3"></canvas>
        </div>

        <div class="col-md-4 pt-70">
            <div class="totais">
                <div class="card mb-20">
                    <div class="card-body">
                        <i class="fa fa-truck"></i>
                        <span id="total-mudancas">
                            <?php echo $totalMudancas; ?>
                            Mudança<?php echo $totalMudancas > 1 ? 's' : ''; ?>
                        </span>
                    </div>
                </div>
                <div class="card mb-20">
                    <div class="card-body">
                        <i class="fa fa-cubes"></i>
                        <span id="total-m3">
                            <?php echo number_format($totalM3, 2); ?>m³
                        </span>
                    </div>
                </div>
                <div class="card mb-20">
                    <div class="card-body">
                        <i class="fa fa-euro"></i>
                        <span id="total-cash">
                            <?php echo number_format($totalCash, 2); ?>&euro;
                        </span>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-md-12 pb-20"></div>

        <div class="col-md-6">
            <h4 class="text-center">Volume em €</h4>
            <canvas id="canvas-volume-cash"></canvas>
        </div>

        <div class="col-md-6">
            <h4 class="text-center">Concentração dos orçamentos</h4>
            <div id="map-heat" style="height: 250px;"></div>
        </div>


        <div class="col-md-12 pt-20">
            <h3>Últimos orçamentos em aberto</h3>
            <?php
                $comissao = 10; // 10%
                $pedidos = array(
                    array(
                        'nome' => 'Franco Silva',
                        'origem' => 'Carnide',
                        'destino' => 'Alvalade'
                    ),
                    array(
                        'nome' => 'Matheus Ferreira',
                        'origem' => 'Marvila',
                        'destino' => 'Olivais'
                    ),
                    array(
                        'nome' => 'Rui Viegas',
                        'origem' => 'Estrela',
                        'destino' => 'Benfica'
                    ),
                );
            ?>
            <table class="table table-bordered">
                <thead>
                <tr>
                    <th>#</th>
                    <th>Loja Worten</th>
                    <th>Data Mudança</th>
                    <th>Estado</th>
                    <th>Nome</th>
                    <th>Origem</th>
                    <th>Destino</th>
                    <th>m3</th>
                    <th>Total (Worten)</th>
                    <th>Comissão</th>
                    <th>Total (cityMover)</th>
                </tr>
                </thead>
                <tbody>
                <?php foreach ($pedidos as $pedido): $cash = mt_rand(50, 500); ?>
                    <?php $total_citymover = $cash - ($comissao / 100 * $cash) ?>
                    <tr>
                        <th><?php echo mt_rand(14, 100); ?></th>
                        <td>#WRT<?php echo mt_rand(138, 240) ?></td>
                        <td><?php echo date('d/m/Y', mt_rand(strtotime('-1 month'), time())); ?></td>
                        <td>
                            <h5><?php echo Dashboard::statusToLabel(mt_rand(0, 1)); ?></h5>
                        </td>
                        <td><?php echo $pedido['nome']; ?></td>
                        <td><?php echo $pedido['origem']; ?></td>
                        <td><?php echo $pedido['destino']; ?></td>
                        <td class="text-center"><?php echo number_format(mt_rand(10, 100), 2); ?></td>
                        <td class="text-center"><?php echo number_format($cash, 2); ?>&euro;</td>
                        <td class="text-center">
                            <h5>
                                <span class="badge badge-success"><?php echo number_format($comissao / 100 * $cash, 2); ?>&euro;</span>
                            </h5>
                        </td>
                        <td><?php echo number_format($total_citymover, 2) ?>&euro;</td>
                    </tr>
                <?php endforeach; ?>
                </tbody>
            </table>
        </div>
    </div>
</div>
