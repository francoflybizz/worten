<div class="container ptb-20">
    <div class="row">
        <div class="col-md-12 pt-20">
            <h3 class="pb-10">Orçamentos</h3>
            <div class="clearfix"></div>

            <div class="pull-left">
                <div class="input-group mb-3">
                    <input type="text" class="form-control" placeholder="Pesquisar..." aria-describedby="basic-addon2">
                    <div class="input-group-append">
                        <button class="btn btn-primary" type="button"><i class="fa fa-search"></i></button>
                    </div>
                </div>
            </div>

            <a href="#" class="btn btn-success pull-right">Novo orçamento</a>
            <div class="clearfix pb-10"></div>
            <?php
            $comissao = 10; // 10%

            $freguesias = array(
                'Ajuda',
                'Alcântara',
                'Alvalade',
                'Areeiro',
                'Arroios',
                'Avenidas Novas',
                'Beato',
                'Belém',
                'Benfica',
                'Campo de Ourique',
                'Campolide',
                'Carnide',
                'Estrela',
                'Lumiar',
                'Marvila',
                'Misericórdia',
                'Olivais',
                'Parque das Nações',
                'Penha de França',
                'Santa Clara',
                'Santa Maria Maior',
                'Santo António',
                'São Domingos de Benfica',
                'São Vicente'
            );

            $pedidos = array(
                array(
                    'nome' => 'Franco Silva',
                ),
                array(
                    'nome' => 'Matheus Ferreira',
                ),
                array(
                    'nome' => 'Rui Viegas',
                ),
                array(
                    'nome' => 'Silvio Lygia',
                ),
                array(
                    'nome' => 'Caio Olegário',
                ),
                array(
                    'nome' => 'Gonçalo Florencio',
                ),
                array(
                    'nome' => 'Elvira Iago',
                ),
                array(
                    'nome' => 'Cândido Teodósio',
                ),
                array(
                    'nome' => 'Luiza Neves',
                ),
                array(
                    'nome' => 'Estela Iria',
                ),
            );
            ?>
            <table class="table table-bordered">
                <thead>
                <tr>
                    <th>#</th>
                    <th>Data Mudança</th>
                    <th>Estado</th>
                    <th>Nome</th>
                    <th>Origem</th>
                    <th>Destino</th>
                    <th>m3</th>
                    <th>Total</th>
                    <th>Comissão</th>
                </tr>
                </thead>
                <tbody>
                <?php foreach ($pedidos as $pedido): $cash = mt_rand(50, 500); ?>
                    <tr>
                        <th><?php echo mt_rand(14, 100); ?></th>
                        <td><?php echo date('d/m/Y', mt_rand(strtotime('-1 month'), time())); ?></td>
                        <td>
                            <h5><?php echo Dashboard::statusToLabel(mt_rand(0, 4)); ?></h5>
                        </td>
                        <td><?php echo $pedido['nome']; ?></td>
                        <td><?php echo $freguesias[array_rand($freguesias)]; ?></td>
                        <td><?php echo $freguesias[array_rand($freguesias)]; ?></td>
                        <td class="text-center"><?php echo number_format(mt_rand(10, 100), 2); ?></td>
                        <td class="text-center"><?php echo number_format($cash, 2); ?>&euro;</td>
                        <td class="text-center">
                            <h5>
                                <span class="badge badge-success"><?php echo number_format($comissao / 100 * $cash, 2); ?>&euro;</span>
                            </h5>
                        </td>
                    </tr>
                <?php endforeach; ?>
                </tbody>
            </table>

            <nav aria-label="Page navigation example" class="pull-right">
                <ul class="pagination">
                    <li class="page-item"><a class="page-link" href="#">Anterior</a></li>
                    <li class="page-item"><a class="page-link" href="#">1</a></li>
                    <li class="page-item"><a class="page-link" href="#">2</a></li>
                    <li class="page-item"><a class="page-link" href="#">3</a></li>
                    <li class="page-item"><a class="page-link" href="#">Próxima</a></li>
                </ul>
            </nav>
            <div class="clearfix"></div>
        </div>
    </div>
</div>