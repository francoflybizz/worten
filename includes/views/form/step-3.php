<?php

if ($_SESSION['insurance_']=='no') {
	$_SESSION['insurance_value'] = "";
	$_SESSION['insurance_final'] = "";
	$_SESSION['items_insurance_value'] = $_SESSION['packed_insurance_value'] = $_SESSION['boxes_insurance_value'] = "";
}

# SEGURO
$sql = "SELECT price FROM `insurance` WHERE `type` = '{$_SESSION['type']}'";
$price = mysql_fetch_array(mysql_query($sql), MYSQL_ASSOC);
$display = $_SESSION['insurance_']=="yes"?"block":"none";
$insurance_value = $_SESSION['insurance_value'] != ""?$_SESSION['insurance_value']:'';
?>

<div class='container optional-items mt-20'>
	<form method='post' id='form' name='form' enctype='multipart/form-data' action='<?php echo CONFIRMACAO ?>'>
		<input type='hidden' name='step' value='4' />
		<input type='hidden' name='insurance_price' id='insurance_price' value='<?php echo $price['price'] ?>' />

		<h4 class="section-title"><?php echo SEGURO ?></h4>
		<div class="form-group">
			<label><?php echo NECESSITA_SEGURO ?></label>
			<div>
			<label class="radio-inline"><input type='radio' name='insurance' value='yes' <?php echo ($_SESSION['insurance_']=='yes'?"checked":"") ?> onclick='javascript:Insurance_Yes();' /> <?php echo SIM ?></label>
			<label class="radio-inline"><input type='radio' name='insurance' value='no' <?php echo ($_SESSION['insurance_']=='no'?"checked":"") ?> onclick='javascript:Insurance_No();' /> <?php echo NAO ?></label>
			</div>

			<div class="clearfix"></div>
			<div id='err_insurance' style='display:none; color:red;'><?php echo NECESSITA_SEGURO_ERRO ?></div>

			<!-- seguro items -->
			<div id='SeguroItems' style='display:<?php echo $display ?>;'>

				<br>
				<!--h4><?php echo ITENS_SEGURAR ?></h4-->
				<hr />
				<?php //echo tools::Message(10) ?>
				<div id='err_insurance_items' style='display:none; color:red;'><?php echo VALOR_ITEMS_ERRO ?></div>

				<?php echo tools::items(); ?>			
				<?php echo tools::boxes(); ?>

				<div class="form-group">
					<label><?php echo VALOR_ITEMS_EUROS ?></label>
					<input class="form-control" type='text' name='insurance_value' id='insurance_value' size='7' maxlength='7' value='<?php echo number_format($insurance_value, 2) ?>' readonly='readonly'>
				</div>

				<div class="form-group">
					<label><?php echo VALOR_SEGURO_EUROS ?></label>
					<input class="form-control" type='text' name='insurance_final' id='insurance_final' size='7' maxlength='7' value='<?php echo number_format($_SESSION['insurance_final'], 2) ?>' readonly='readonly'>
				</div>

			</div>
			<!-- /seguro items -->


		<!-- elevador -->
		<?php if ($_SESSION['dep_lift'] == 'yes' || $_SESSION['dest_lift'] == 'yes'): ?>
			<?php $display = $_SESSION['lift'] == "yes" ? "block" : "none"; ?>
	

			<h4 class="section-title"><?php echo ELEVADOR ?></h4>
			<label>Existem itens que não cabem no elevador?</label>
			<div class="form-group">
				<label class="radio-inline"><input type='radio' name='lift' value='yes' <?php echo ($_SESSION['lift']=='yes'?"checked":"") ?> onclick='javascript:Lift_Yes();' /> <?php echo SIM ?></label>
				<label class="radio-inline"><input type='radio' name='lift' value='no' <?php echo ($_SESSION['lift']=='no'?"checked":"") ?> onclick='javascript:Lift_No();' /> <?php echo NAO ?></label>
				<div id='err_lift' style='display:none; color:red;'><?php echo ELEVADOR_ERRO ?></div>

				<!-- elevador items -->
				<div id='ElevadorItems' style='display:<?php echo $display ?>;'>
					<div class="form-group">
						<br />
						<label><?php echo NAO_CABEM_ELEVADOR ?></label>
						<!--<textarea class="form-control" name='lift_items'  id='lift_items' rows='5'><?php /*echo stripslashes($_SESSION['lift_items']) */?></textarea>-->

                        <div>
                            <table class='insurance-table table' width='100%' cellpadding='3' cellspacing='0'>

                                <?php
                                $sql = "(SELECT i.id id, c.name_pt categoria, i.name_pt item, o.total
							FROM orders_items o LEFT JOIN items i
							  ON i.id = o.id_items LEFT JOIN categories c
							  ON c.id = i.cat
							WHERE id_orders = {$_SESSION['id_order']}
							  AND c.name_pt IS NOT NULL
							ORDER BY categoria, item, o.total)
							UNION
							(SELECT CONCAT('o',id) id, 'Outros' categoria, outros item, total
							FROM orders_items
							WHERE id_orders = {$_SESSION['id_order']}
							  AND outros <> '')";

                                $query = mysql_query($sql);
                                $n_items = mysql_num_rows($query);
                                $cor = "#fff";
                                ?>

                                <?php for ($i=0;$i<$n_items;$i++): ?>
                                    <?php $r = mysql_fetch_object($query); ?>

                                    <?php if ($r->categoria != $categoria_ant): ?>
                                        <tr style="background:#dadada; color:#333;">
                                            <td><?php echo $r->categoria ?></td>
                                            <td>Quantidade</td>
                                        </tr>
                                    <?php endif; ?>

                                    <tr style='background:<?php echo $cor ?>;'>
                                        <td>
                                            <label>
                                                <input type="checkbox" name="lift_items_check[<?php echo $i; ?>]" value="<?php echo $r->id; ?>">
                                                &nbsp;
                                                <?php echo $r->item ?>
                                            </label>
                                        </td>
                                        <td>
                                            <select style="width: 50px" name="lift_items_qty[<?php echo $i; ?>]" class="form-control">
                                                <?php foreach(range(1, $r->total) as $j): ?>
                                                    <option value="<?php echo $j; ?>"><?php echo $j; ?></option>
                                                <?php endforeach; ?>
                                            </select>
                                        </td>
                                    </tr>

                                    <?php
                                    if ($cor=="#fff") $cor = "#f1f1f1";
                                    else $cor = "#fff";
                                    $cor = ($cor == "#fff") ? "#f1f1f1" : "#fff";
                                    $categoria_ant = $r->categoria;
                                    ?>
                                <?php endfor; //endfor ?>

                            </table>
                        </div>
					</div>

				</div>
				<!-- /elevador items -->
			</div>

		<?php endif; ?>

		<!-- estacionamento -->

		<h4 class="section-title mt-40"><?php echo ESTACIONAMENTO ?></h4>
		<div>
			<p>Se em frente à entrada da sua morada de origem/destino existe estacionamento regulado pela EMEL, ou outra entidade que regule o estacionamento e a sua mudança/transporte de casa e/ou escritório implica o condicionamento ou o desvio parcial de trânsito, deve ser solicitada à Camara de Lisboa (ou em função da cidade onde se encontre) a permissão para estacionamento e os respetivos pedidos de autorização para ocupar o espaço público.</p>
			<p>A Permissão para estacionamento e pedidos de autorização para ocupar o espaço público que condicione ou inviabilize as normais condições de circulação viária e pedonal na via pública, em Lisboa, são da competência da Câmara Municipal de Lisboa.</p>
			<p>O prazo para obtenção da referida autorização é de 6 dias úteis. Acrescem 2 dias para pedido de gratificado da PSP.</p>
			<p>O pedido de condicionamento de trânsito está sujeito ao pagamento de taxas, nos termos da Tabela de Taxas em vigor, que abaixo se discrimina.</p>
		</div>

		<table class='form-table table'>
			<tr>
				<th class="hidden-xs">Código Tabela</th>
				<th>Descrição da actividade / bem</th>
				<th>Valor por licença (*)</th>
				<th class="text-center">Origem</th>
				<th class="text-center">Destino</th>
			</tr>
			<tr>
				<td class="hidden-xs">TTM 5.2.4</td>
				<td>Condicionamentos temporários de trânsito (por troço de via e/ou cruzamento)</td>
				<td>110,00&euro;</td>
				<td class="text-center"><input type='checkbox' name='dep_emel' value='1' <?php echo ($_SESSION['dep_emel']==1?"checked":"") ?> /></td>
				<td class="text-center"><input type='checkbox' name='dest_emel' value='1' <?php echo ($_SESSION['dest_emel']==1?"checked":"") ?> /></td>
			</tr>
		</table>

		<div>
			<p><strong>TTM - Tabela de Taxas Municipais em vigor</strong><br />
				(*) Não sujeito a IVA, nos termos do n.º 2 do artigo 2º do Código do IVA (CIVA)<br />
				Nota: O pagamento das taxas é efetuado no momento da entrega do pedido.<br />
				(*) acresce de taxa admistrativa se tratado pelos serviços da cityMover. Os valores a cobrar são variáveis e dependem das actualizações municipais.<br />
				(*) se pretender alteração de data/local de condicionamento temporário de trânsito, após a licença emitida o valor a cobrar pela Camra de Lisboa é de 59,95€
			</p>
		</div>

		<p>Só após a autorização/licença emitida pela CML se poderá solicitar o gratificado da PSP para regulação do trânsito no local.</p>

		<table class='form-table table'>

			<tr>
				<th>Código Tabela</th>
				<th>Descrição da actividade / bem</th>
				<th>Valor por cada 4 horas (*)</th>
				<th class="text-center">Origem</th>
				<th class="text-center">Destino</th>
			</tr>
			<tr>
				<td>PSP</td>
				<td>Apoio à regulação do transito pela PSP – por cada 4 horas</td>
				<td>75,00€</td>
				<td class="text-center"><input type='checkbox' name='dep_psp' value='1' <?php echo ($_SESSION['dep_psp']==1?"checked":"") ?> /></td>
				<td class="text-center"><input type='checkbox' name='dest_psp' value='1' <?php echo ($_SESSION['dest_psp']==1?"checked":"") ?> /></td>
			</tr>
		</table>

		<p>Se a morada de origem e/ou destino da sua habitação e/ou do seu escritório esteja localizado em zona que condicione ou inviabilize as normais condições de circulação viária e pedonal na via pública, então para a realização do seu serviço de mudança, transporte, carga ou descarga a cityMover auxilia na obtenção da referida licença.</p>
		<p>A cityMover deve ser, obrigatoriamente, informada da(s) morada(s) exata(s) de origem(s) e destino(s) para uma correta apreciação das características dos locais e adequação dos meios e licenças subjacentes ao serviço.</p>
		<p>Informações imprecisas e/ou a falta das mesmas em tempo útil, no mínimo de 9 dias úteis, antes da data do serviço, que levem a coimas e/ou atrasos e/ou custos acrescidos correm por conta e risco do cliente/entidade que solicitou o serviço.</p>

		<div><label class="checkbox-inline"><input type='checkbox' required value='1' name='terms_agree' /> <strong>Tomei conhecimento</strong></label></div>

		<hr />

		<div class='row'>
			<div class='col-md-6 text-left'><input class='btn btn-default FormBtmPrev' type='button' name='go_back' value='<?php echo PASSO_ANTERIOR ?>' onclick='window.location="<?php echo MUDANCA ?>";' /> &nbsp; </div>
			<div class='col-md-6 text-right'><button type='submit' class='btn btn-success FormBtmNext' type='button' id='step_3_submit' name='step_3_submit' onclick='Validar(this.form, "3");return false;' /><?php echo PROXIMO_PASSO ?> <i class='fa fa-chevron-right'></i></button></div>
		</div>

	</form>
</div>


</div>