<div class="container mt-40 mb-40">

	<h3 class="section-title"><?php echo REFERENCIA ?>: <strong>#<?php echo sprintf("%07d",$_SESSION['id_order']) ?></strong></h3>

	<?php
		$sqlM3 = "SELECT SUM(IFNULL(i.m3, ((oi.largura / 100) * (oi.comprimento / 100) * (oi.altura / 100)))) as m3
					FROM orders_items oi
					LEFT JOIN items i ON i.id = oi.id_items
					WHERE id_orders = {$_SESSION['id_order']}";
		$resultM3 = mysql_fetch_object(mysql_query($sqlM3));

		$dep_cp4 = intval(substr($_SESSION['dep_zip'], 0, 4));
		$dest_cp4 = intval(substr($_SESSION['dest_zip'],0,4));

		$sqlZones = "SELECT id, maxorders FROM zones
				          WHERE ({$dep_cp4} between SUBSTRING(`start`,1,4) and SUBSTRING(`end`,1,4))
				          AND ({$dest_cp4} between SUBSTRING(`start`,1,4) and SUBSTRING(`end`,1,4))";

		$sqlZones = mysql_query($sqlZones);
		$totalZones = mysql_num_rows($sqlZones);
		if ($totalZones > 0) {
			$dataZones = mysql_fetch_array($sqlZones, MYSQL_ASSOC);
		}
	?>
	<?php if ($resultM3->m3 < 20
				&& $dataZones['maxorders'] > 0): ?>
		<p>Caro(a) <strong><?php echo $_SESSION['name']; ?></strong>,</p>
		<p>Agradecemos o interesse pelos nossos serviços na utilização da plataforma on-line da cityMover.</p>
		<p>O seu orçamento com a referência <strong>#<?php echo sprintf("%07d",$_SESSION['id_order']) ?></strong> foi encaminhado para o seu email.</p>
		<p>O nosso departamento comercial está disponível em <a href="mailto:+351214789400">(+351) 214 789 400</a> ou em <a href="mailto:info@citymover.pt">info@citymover.pt</a></p>
		<p>Durante as próximas 24 horas (dias úteis) entraremos em contacto e faremos o follow-up.</p>
		<p>Gratos pelo seu contato.</p>
	<?php else: ?>
		<p>Caro(a) <strong><?php echo $_SESSION['name']; ?></strong>,</p>
		<p>Agradecemos o interesse pelos nossos serviços.</p>
		<br>
		<p>A sua simulação com a referência <strong>#<?php echo sprintf("%07d",$_SESSION['id_order']) ?></strong> será encaminhada para o departamento comercial da cityMover, que nas próximas 24 horas (dias úteis) o/a irá contatar.</p>
		<p>Caso pretenda algum esclarecimento adicional, por favor ligue <a href="mailto:+351214789400">(+351) 214 789 400</a> ou escreva-nos para <a href="mailto:info@citymover.pt">info@citymover.pt</a>.</p>
		<br>
		<p>Gratos pelo seu contato.</p>
	<?php endif; ?>

	<!-- <p>Recebemos o seu pedido de orçamento!</p>

	<p>Agradecemos o seu interesse nos nossos serviços. Levamos por norma menos de 24h a responder. Caso verifique alguma demora na resposta, por favor contacte-nos diretamente através do email <a href="info@citymover.pt">info@citymover.pt</a></p> -->

	<hr />

	<form method="get" action="<?php echo base_url(); ?>">
		<button type="submit" class="btn btn-block btn-info">Finalizar</button>
	</form>

	<hr />

	<a class="btn btn-info btn-block" href="<?php echo base_url("?mod=dashboard") ?>">Ir para o dashboard</a>

	<br>

</div>
