
<?php $data = $this->mod_data->data; ?>
<?php $data = json_decode(json_encode($data), TRUE) ?>

		<div class='container mt-20'>
			<form method='post' id='form' name='form' enctype='multipart/form-data' action='<?php echo FORM_O_SEU_ORCAMENTO; ?>'>
				<input type='hidden' name='step' value='5' />

				<h2>A sua mudança</h2>
				<p><?php echo tools::Message(9) ?></p>

				<div class="row">

					<div class="col-md-6 col-xs-12 col-sm-12">
						<h4 class="section-title"><?php echo DADOS_PESSOAIS; ?></h4>

						<div class="form-group">
							<label><?php echo NOME; ?></label>
							<p><?php echo stripslashes($data['name']); ?></p>
						</div>

						<div class="form-group">
							<label><?php echo TELEFONE; ?></label>
							<p><?php echo $data['phone']; ?></p>
						</div>

						<div class="form-group">
							<label><?php echo TELEMOVEL; ?></label>
							<p><?php echo $data['mobile']; ?></p>
						</div>

						<div class="form-group">
							<label><?php echo EMAIL; ?></label>
							<p><?php echo $data['email']; ?></p>
						</div>

						<div class="form-group">
							<label><?php echo NUMERO_CONTRIBUINTE; ?></label>
							<p><?php echo $data['taxnumber']; ?></p>
						</div>

						<?php if ($_SESSION['type'] == 'office'): ?>
						<div class="form-group">
							<label><?php echo EMPRESA; ?></label>
							<p><?php echo stripslashes($data['company']); ?></p>
						</div>
						<?php endif; ?>
					</div>

					<div class="col-md-6 col-xs-12 col-sm-12">
						<!-- dados da mudanca -->
						<h4 class="section-title"><?php echo DADOS_MUDANCA; ?></h4>
						<div class="form-group">
							<label><strong><?php echo LOCALIDADE_ORIGEM; ?></strong></label>
							<p><?php echo stripslashes($data['dep_city']); ?></p>
						</div>
						<div class="form-group">
							<label><strong><?php echo MORADA_ORIGEM; ?></strong></label>
							<p><?php echo nl2br(stripslashes($data['address'])); ?></p>
						</div>
						<div class="form-group">
							<label><strong><?php echo CODIGO_POSTAL; ?></strong></label>
							<p><?php echo $data['dep_zip']; ?></p>
						</div>
						<div class="form-group">
							<label><strong><?php echo LOCALIDADE_DESTINO; ?></strong></label>
							<p><?php echo stripslashes($data['dest_city']); ?></p>
						</div>
						<div class="form-group">
							<label><strong><?php echo MORADA_DESTINO; ?></strong></label>
							<p><?php echo nl2br(stripslashes($data['address_dest'])); ?></p>
						</div>
						<div class="form-group">
							<label><strong><?php echo CODIGO_POSTAL; ?></strong></label>
							<p><?php echo $data['dest_zip']; ?></p>
						</div>
						<div class="form-group">
							<label><strong><?php echo DATA_MUDANCA; ?></strong></label>
							<p><?php echo $_SESSION['data_entrega'] ?><br /><?php echo MUDANCA_HORARIO; ?>
							</label>
						</div>
					</div>

				</div>

				<!-- itens a transportar -->
				<h4 class="section-title"><?php echo ITEMS_TRANSPORTAR; ?></h4>
				<h4 class="insurance-table-title"><?php echo ITEMS ?></h4>
				<div>
					<table class='insurance-table table' width='100%' cellpadding='3' cellspacing='0'>

					<?php
					$sql = "(SELECT i.id id, c.name_pt categoria, i.name_pt item, o.total
							FROM orders_items o LEFT JOIN items i
							  ON i.id = o.id_items LEFT JOIN categories c
							  ON c.id = i.cat
							WHERE id_orders = {$_SESSION['id_order']}
							  AND c.name_pt IS NOT NULL
							ORDER BY categoria, item, o.total)
							UNION
							(SELECT CONCAT('o',id) id, 'Outros' categoria, outros item, total
							FROM orders_items
							WHERE id_orders = {$_SESSION['id_order']}
							  AND outros <> '')";

					$query = mysql_query($sql);
					$n_items = mysql_num_rows($query);
					$cor = "#fff";
					?>

					<?php for ($i=0;$i<$n_items;$i++): ?>
					<?php $r = mysql_fetch_object($query); ?>

					<?php if ($r->categoria != $categoria_ant): ?>
					<tr style="background:#dadada; color:#333;"">
						<td><?php echo $r->categoria ?></td>
						<td align='right'><?php echo QUANTIDADE ?></td>
					</tr>
					<?php endif; ?>

					<tr style='background:<?php echo $cor ?>;'>
						<td>
							<span id='ItemLink<?php echo $r->id; ?>'><?php echo $r->item ?></span>
							<span id='Item<?php echo $r->id; ?>' style='display:none;'><?php echo stripslashes($r->item); ?></span>
						</td>
						<td align='right'><?php echo $r->total ?></td>
					</tr>

					<?php
						if ($cor=="#fff") $cor = "#f1f1f1";
						else $cor = "#fff";
						$cor = ($cor == "#fff") ? "#f1f1f1" : "#fff";
						$categoria_ant = $r->categoria;
					?>
					<?php endfor; //endfor ?>

					</table>
				</div>

				<h4 class="insurance-table-title"><?php echo CAIXAS ?></h4>

				<div>
					<table class='insurance-table table' width='100%' cellpadding='3' cellspacing='0'>

				<?php
					$sql = "SELECT name_pt, o.total
							FROM orders_boxes o LEFT JOIN boxes b ON b.id = o.id_boxes
							WHERE id_orders = {$_SESSION['id_order']}
							ORDER BY name_pt, o.total";
					$query = mysql_query($sql);
					$n_items = mysql_num_rows($query);
					?>

					<?php if ($n_items > 0): ?>
					<?php $cor = "#fff"; ?>
					<?php for ($i=0;$i<$n_items;$i++): ?>
					<?php $r = mysql_fetch_object($query); ?>

							<tr style='background:<?php echo $cor ?>;'>
								<td><?php echo $r->name_pt; ?></td>
								<td align='right'><?php echo $r->total; ?></td>
							</tr>

							<?php $cor = $cor == "#fff" ? "#f1f1f1" : "#fff"; ?>

						<?php endfor; ?>
					<?php else: ?>

						<tr style='background:#fff;'>
							<td colspan='2'><?php echo SEM_CAIXAS_SELECIONADAS; ?></td>
						</tr>

					<?php endif; ?>

					</table>
				</div>

				<h3 class="section-title"><?php echo CAIXAS_EMBALADAS; ?></h3>

			<table class='FormOrc' width='100%' cellspacing='0' cellpadding='5'><tr><td align='center' class='FormOrc'>

			<tr>
			<td colspan='2' style='padding:0;'>

				<div>
					<table class='insurance-table table' width='100%' cellpadding='3' cellspacing='0'>

					<?php
					$sql = "SELECT * FROM orders_packed
							WHERE id_orders = {$_SESSION['id_order']}
							ORDER BY nome, total";
					$query = mysql_query($sql);
					?>

					<?php if (mysql_num_rows($query)>0): ?>
					<?php $cor = "#fff"; ?>
					<?php for ($i=0;$i<mysql_num_rows($query);$i++): ?>
					<?php $s = mysql_fetch_object($query); ?>

							<tr style='background:<?php echo $cor ?>;'>
								<td><?php echo stripslashes($s->nome)." ".$s->largura."x".$s->comprimento."x".$s->altura; ?></td>
								<td align='right'><?php echo $s->total ?></td>
							</tr>

							<?php
							if ($cor=="#fff") $cor = "#f1f1f1";
							else $cor = "#fff";
							?>

						<?php endfor; ?>

					<?php else: ?>
							<tr style='background:#fff;'>
								<td colspan='2'><?php echo SEM_CAIXAS_EMBALADAS; ?></td>
							</tr>
					<?php endif; ?>

					</table>
				</div>
				</td>
				</tr>
				</table>

					<h3 class="section-title"><?php echo SEGURO ?></h3>

					<div class="form-group">
						<label><?php echo NECESSITA_SEGURO; ?></label>
						<p><strong><?php echo ($data['insurance'] == 0 ? NAO:SIM); ?></strong></p>
					</div>

					<?php if ($data['insurance'] == 1): ?>
					<h4 class="section-title"><?php echo ITENS_SEGURAR ?></h4>

					<!-- itens segurados -->
					<?php
								# PASSO 4: SEGUROS: ÍTENS
								$sql = "(SELECT i.id id, c.name_pt categoria, i.name_pt item, o.total, o.`value`
										FROM orders_items o LEFT JOIN items i
										  ON i.id = o.id_items LEFT JOIN categories c
										  ON c.id = i.cat
										WHERE id_orders = {$_SESSION['id_order']}
										  AND c.name_pt IS NOT NULL
										  AND o.`value` IS NOT NULL
										ORDER BY categoria, item, o.total)
										UNION
										(SELECT CONCAT('o',id) id, 'Outros' categoria, outros item, total, `value`
										FROM orders_items
										WHERE id_orders = {$_SESSION['id_order']}
										  AND outros <> ''
										  AND `value` IS NOT NULL
										  )";

								$query = mysql_query($sql);
								$n_items = mysql_num_rows($query);

								if ($n_items > 0) {

									$body = "
										<table class='insurance-table table' width='100%' cellpadding='5' cellspacing='0'>
										<div style='background:#525252; margin:10px 0 0 0; padding:5px; color:#fff; font-weight:bold;'>".ITEMS."</div>
										<div>
											<table class='insurance-table table' width='100%' cellpadding='3' cellspacing='0'>";

									$cor = "#fff";
									$header = false;
									for ($i=0;$i<$n_items;$i++) {
										$r = mysql_fetch_object($query);

										if ($r->categoria != $categoria_ant) {
											$body .= "
												<tr class='table-section-title'>
													<td>".$r->categoria."</td>";

											if (!$header) {
												$body .= "
														<td align='center'>".QUANTIDADE."</td>
														<td align='right'>".VALOR." &euro;&nbsp;&nbsp;&nbsp;&nbsp;</td>";
												$header = true;
											}
											else {
												$body .= "<td>&nbsp;</td><td>&nbsp;</td>";
											}
										}

										$body .= "
											</tr>
											<tr style='background:".$cor."'>
												<td>".stripslashes($r->item)."</td>
												<td align='center'>".$r->total."</td>
												<td align='right'>".number_format($r->value,2,',','.')."</td>
											</tr>";

										if ($cor=="#fff") $cor = "#f1f1f1";
										else $cor = "#fff";
										$categoria_ant = $r->categoria;
									}

									$body .= "</table>";
								}

								# PASSO 4: SEGUROS: CAIXAS
								$sql = "SELECT name_{$_SESSION['lang']} name, o.total, b.id id, o.`value`
										FROM orders_boxes o LEFT JOIN boxes b ON b.id = o.id_boxes
										WHERE id_orders = {$_SESSION['id_order']}
											AND `value` IS NOT NULL
										ORDER BY name_pt, o.total";
								$query = mysql_query($sql);
								$n_items = mysql_num_rows($query);

								if ($n_items > 0) {

									$body .= "
										<table width='100%' cellpadding='5' cellspacing='0'>
										<div style='background:#525252; margin:10px 0 0 0; padding:5px; color:#fff; font-weight:bold;'>".CAIXAS."</div>
										<div>
											<table class='insurance-table table' width='100%' cellpadding='3' cellspacing='0'>
												<tr class='table-section-title'>
													<td>&nbsp;</td>
													<td align='center'>".QUANTIDADE."</td>
													<td align='right'>".VALOR." &euro;&nbsp;&nbsp;&nbsp;&nbsp;</td>
												</tr>";

											$cor = "#fff";
											for ($i=0;$i<$n_items;$i++) {
												$r = mysql_fetch_object($query);

												$body .= "
													<tr style='background:".$cor.";'>
														<td>".$r->name."</td>
														<td align='center'>".$r->total."</td>
														<td align='right'>".number_format($r->value,2,',','.')."</td>
													</tr>";

												if ($cor=="#fff") $cor = "#f1f1f1";
												else $cor = "#fff";

											}

									$body .= "</table>";
								}

								# PASSO 4: SEGUROS: CAIXAS EMBALADAS
								$sql = "SELECT * FROM orders_packed
										WHERE id_orders = {$_SESSION['id_order']}
											AND `value` IS NOT NULL
										ORDER BY nome, total";
								$query = mysql_query($sql);
								$n_items = mysql_num_rows($query);

								if ($n_items > 0) {

									$body .= "
										<table width='100%' cellpadding='5' cellspacing='0'>
										<div style='background:#525252; margin:10px 0 0 0; padding:5px; color:#fff; font-weight:bold;'><?php echo CAIXAS_EMBALADAS; ?></div>
										<div>
											<table class='insurance-table table' width='100%' cellpadding='3' cellspacing='0'>
												<tr class='table-section-title'>
													<td>&nbsp;</td>
													<td align='center'>".QUANTIDADE."</td>
													<td align='right'>".VALOR." &euro;&nbsp;&nbsp;&nbsp;&nbsp;</td>
												</tr>";

											$cor = "#fff";
											for ($i=0;$i<$n_items;$i++) {
												$r = mysql_fetch_object($query);

												$body .= "
													<tr style='background:".$cor.";'>
														<td>".$r->nome."</td>
														<td align='center'>".$r->total."</td>
														<td align='right'>".number_format($r->value,2,',','.')."</td>
													</tr>";

												if ($cor=="#fff") $cor = "#f1f1f1";
												else $cor = "#fff";

											}

									$body .= "</table>";
								}

						?>

						<?php echo $body ?>
						<!-- /itens segurados -->

				<hr />

				<div class="form-group">
					<label><?php echo VALOR_ITEMS_EUROS ?></label>
					<p><?php echo number_format(floatval($data['insurance_value']),2); ?>&euro;</p>
				</div>

				<div class="form-group">
					<label><?php echo VALOR_SEGURO_EUROS ?></label>
                    <?php $_SESSION['insurance_final'] = str_replace(',', '.', $_SESSION['insurance_final']); ?>
					<p><?php echo number_format(floatval($_SESSION['insurance_final']), 2); ?>&euro;</p>
				</div>

			<?php endif; ?>

			<?php if ($data['lift_items'] != ""): ?>
				<h3 class="section-title"><?php echo ELEVADOR ?></h3>

				<div class="form-group">
					<label><?php echo NAO_CABEM_ELEVADOR; ?></label>
					<p><?php echo nl2br(stripslashes($data['lift_items'])); ?></p>
				</div>

			<?php endif; ?>

					<hr />

                <h3 class="section-title">Total</h3>
                <?php
                $priceM3 = $data['price_m3'];

                $sql = "SELECT SUM(IFNULL(i.m3, ((oi.largura / 100) * (oi.comprimento / 100) * (oi.altura / 100)))) as m3
                            FROM orders_items oi
                            LEFT JOIN items i ON i.id = oi.id_items
                            WHERE id_orders = {$_SESSION['id_order']}";
                $totalM3 = current(current(get_sql($sql, 'array')));

                $subTotal = $priceM3 * $totalM3;
                ?>
                    <table class="table">
                        <tr>
                            <td>Volume (<?php echo number_format($totalM3, 2); ?>m³)</td>
                            <th class="text-right" style="width: 200px"><?php echo number_format($subTotal, 2); ?>&euro;</th>
                        </tr>
                        <?php if ($data['kit_id']): $subTotal += $data['kit_price']; ?>
                        <tr>
                            <td><?php echo $data['kit_name']; ?></td>
                            <th class="text-right"><?php echo number_format($data['kit_price'], 2); ?>&euro;</th>
                        </tr>
                        <?php endif; ?>

                        

                        <?php if ($_SESSION['insurance_final'] > 0):
                            $_SESSION['insurance_final'] = str_replace(',', '.', $_SESSION['insurance_final']);
                            $subTotal += $_SESSION['insurance_final']; ?>
                            <tr>
                                <td><?php echo VALOR_SEGURO_EUROS; ?></td>
                                <th class="text-right"><?php echo number_format(floatval($_SESSION['insurance_final']), 2); ?>&euro;</th>
                            </tr>
                        <?php endif; ?>

                        <tr>
                            <td>Subtotal</td>
                            <th class="text-right"><?php echo number_format($subTotal, 2); ?>&euro;</th>
                        </tr>
                        <!--tr>
                            <td>Desconto Worten</td>
                            <th class="text-right">
                                <span class="badge badge-success fz-16">
                                    <?php
                                    $desconto = $subTotal * $data['discount_percent'] / 100;
                                    //$subTotal -= $desconto;
                                    echo number_format($desconto, 2); ?>&euro; (<?php echo number_format($data['discount_percent']); ?>%)
                                </span>
                            </th>
                        </tr-->
                        <tr>
                            <td>Total da sua mudança</td>
                            <th class="text-right color-red fz-20"><?php echo number_format($subTotal, 2); ?>&euro;</th>
                        </tr>
                    </table>

                    <?php if ($data['dep_emel'] == 1 || $data['dest_emel'] == 1): ?>
                    <h3 class="section-title mt-20">Licenças, estacionamento e serviços</h3>
                    <table class="table">
                    	<?php
                        if ($data['dep_emel'] == 1 || $data['dest_emel'] == 1):
                            $_SESSION['total_parking'] = str_replace(',', '.', $_SESSION['total_parking']);
                            $subTotal += $_SESSION['total_parking'];
                            ?>

                            <td><?php echo LICENCAS_E_ESTACIONAMENTO ?></td>
                            <!--<p>
                                <?php if ($data['dep_emel'] == 1 && $data['dest_emel'] == 0): ?>
                                    <?php echo ESTACIONAMENTO_OPCAO_1.EM." <b>".strtolower(MORADA_ORIGEM); ?></b>
                                <?php elseif ($data['dep_emel'] == 0 && $data['dest_emel'] == 1): ?>
                                    <?php echo ESTACIONAMENTO_OPCAO_1.EM." <b>".strtolower(MORADA_DESTINO); ?></b>
                                <?php elseif ($data['dep_emel'] == 1 && $data['dest_emel'] == 1): ?>
                                    <?php echo ESTACIONAMENTO_OPCAO_1.EM." <b>".strtolower(MORADA_ORIGEM)."</b>" . E . "<b>".strtolower(MORADA_DESTINO); ?></b>
                                <?php endif; ?>
                            </p>-->
                            <th class="text-right"><?php echo number_format($_SESSION['total_parking'],2); ?>&euro;</th>

                        <?php endif; ?>

                        <?php if ($_SESSION['total_psp'] > 0):
                            $_SESSION['total_psp'] = str_replace(',', '.', $_SESSION['total_psp']);
                            $subTotal += $_SESSION['total_psp'];
                        ?>
                        <tr>
                            <td>
                                <?php echo SERVICOS_DE_POLICIAMENTO ?>
                                <!--<?php if ($data['dep_psp'] == 1 && $data['dest_psp'] == 0): ?>
                                    <?php echo ESTACIONAMENTO_OPCAO_2.EM."<b>".strtolower(MORADA_ORIGEM); ?></b>
                                <?php elseif ($data['dep_psp'] == 0 && $data['dest_psp'] == 1): ?>
                                    <?php echo ESTACIONAMENTO_OPCAO_2.EM."<b>".strtolower(MORADA_DESTINO); ?></b>
                                <?php elseif ($data['dep_psp'] == 1 && $data['dest_psp'] == 1): ?>
                                    <?php echo ESTACIONAMENTO_OPCAO_2.EM."<b>".strtolower(MORADA_ORIGEM)."</b>" . E . "<b>".strtolower(MORADA_DESTINO); ?></b>
                                <?php endif; ?>-->
                            </td>
                            <th class="text-right">
                                <?php echo number_format($_SESSION['total_psp'],2); ?>&euro;
                            </th>
                        </tr>
                        <?php endif; ?>
                    </table>
                	<?php endif; ?>

					<hr />

					<div class="form-group">
						<h3 class="section-title"><?php echo COMENTARIOS ?></h3>
						<p>Poderá utilizar o campo abaixo para nos fazer chegar informação que considere relevante.</p>
						<textarea class="form-control" name='comments' id='comments' cols='40' rows='10' style='width:100%;'><?php echo stripslashes($_SESSION['comments']); ?></textarea>
					</div>

						<?php
						if ($data['insurance']==1) $id = 1;
						else $id = 2;
						?>

						<?php
						$sql = "SELECT {$_SESSION['type']}_{$_SESSION['lang_fe']} pdf FROM `contracts` WHERE id = {$id}";
						$contract = mysql_fetch_array(mysql_query($sql), MYSQL_ASSOC);
						?>

						<div class="form-group">
							<div>
								<strong><?php echo TERMOS_CONDICOES_1."<a href='//www.citymover.pt/files/Clausulas-cityMover-2016.pdf' target='_blank'>".TERMOS_CONDICOES_2; ?></strong>&nbsp;<i class="fa fa-file-pdf-o"></i></a>
							</div>

							<label class="radio-inline"><input type='radio' name='i_agree' value='yes' /> <?php echo SIM; ?> </label>
							<label class="radio-inline"><input type='radio' name='i_agree' value='no' /> <?php echo NAO; ?> </label>
							<div class='OrderError' id='err_i_agree' style='display:none;'><?php echo TERMOS_CONDICOES_ERRO; ?></div>
						</div>

						<div class="form-group">
						<label>Responsável Worten</label>
						<select class="form-control" name="worten_responsible">
							<option>João Alvarim</option>
							<option>Miguel Mendonça</option>
							<option>Sónia Sotto</option>
							<option>Mónica Santos</option>
						</select>
					</div>

		<br>
		<br>

		<hr />
		<div class='row'>
			<div class='col-md-6 text-left'><input class='btn btn-default FormBtmPrev' type='button' name='go_back' value='<?php echo PASSO_ANTERIOR; ?>' onclick='window.location="<?php echo SEGURO_E_ACESSIBILIDADES; ?>";' /></div>
			<div class='col-md-6 text-right'><button class='btn btn-success FormBtmNext' type='button' id='step_4_submit' name='step_4_submit' onclick='Validar(this.form, "4"); return false;' />Finalizar <i class='fa fa-chevron-right'></i></button></div>
		</div>

		<br>
		<br>

		</form>
		<div id='response'></div>
		</div>
