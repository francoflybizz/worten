<form method="post" action="<?php echo base_url("?mod=form&step=3"); ?>" ng-controller="Step2Ctrl as vm">

<br />
<br />
<br />

<div class="container">

<h2>Os seus itens a mover</h2>
<p>Seleccione os itens e respectivas quantidades a transportar de cada uma das divisões.</p>

<div class="row">

<?php

$type = 'home';
$sql = "SELECT id, name_" . core::lang() . " name
        FROM categories
        WHERE `level` = '" . $type . "'
        ORDER BY name_" . core::lang();
$categorias = get_sql($sql, 'array');

$icons = array(
    2  => 'fa-coffee',
    4  => 'fa-television',
    5  => 'fa-bed',
    6  => 'fa-bed',
    7  => 'fa-bed',
    8  => 'fa-cutlery',
    9  => 'fa-picture-o',
    10 => 'fa-bath',
    11 => 'fa-desktop',
    12 => 'fa-window-restore',
    14 => 'fa-cubes'
);

?>

<?php foreach ($categorias as $index => $categoria): if ($categoria['id'] == 12) continue; ?>
    <div class="col-md-2">
        <div class="house-division <?php echo $index == 0 ? "active" : ""; ?>" data-target="categoria-<?php echo $categoria["id"] ?>">
            <i class="fa <?php echo $icons[$categoria['id']] ?>"></i>
            <span><?php echo $categoria["name"] ?></span>
        </div>
    </div>
<?php endforeach ?>
<?php foreach ($categorias as $index => $categoria): if ($categoria['id'] != 12) continue; ?>
    <div class="col-md-2">
        <div class="house-division <?php echo $index == 0 ? "active" : ""; ?>" data-target="categoria-<?php echo $categoria["id"] ?>">
            <i class="fa <?php echo $icons[$categoria['id']] ?>"></i>
            <span><?php echo $categoria["name"] ?></span>
        </div>
    </div>
<?php endforeach ?>

</div>
<!-- /row -->

<div class="clearfix"></div>

<!-- listing & quantities section -->
<div class="house-division-items">
	<?php foreach ($categorias as $index => $categoria): ?>
	<div id="categoria-<?php echo $categoria["id"] ?>" class="<?php echo $index > 0 ? "d-none" : ""; ?>"
        data-categoria='<?php echo json_encode($categoria); ?>'>


		<h3><?php echo $categoria["name"] ?></h3>

        <?php
        $sql = "SELECT i.id, i.name_" . core::lang() . " name, i.m3
                FROM items i INNER JOIN categories c ON i.cat = c.id
                WHERE i.cat = " . $categoria['id'] . "
                ORDER BY i.name_" . core::lang();
        $items = get_sql($sql, 'array');

        if(count($items) > 0): ?>
		<table class="table table-striped">
			<?php foreach ($items as $item): ?>
			<tr>
				<td><?php echo $item['name'] ?></td>
				<td class="qty-changer text-center">
                    <a href="javascript:void(0)" class="btn btn-info btn-plus" ng-click="vm.items[<?php echo $categoria['id'] ?>][<?php echo $item['id']; ?>] = vm.items[<?php echo $categoria['id'] ?>][<?php echo $item['id']; ?>] >= 1 ? (vm.items[<?php echo $categoria['id'] ?>][<?php echo $item['id']; ?>]-1) : vm.items[<?php echo $categoria['id'] ?>][<?php echo $item['id']; ?>]"><i class="fa fa-minus"></i></a>
                    <input style="width: 75px; display: inline-block" class="form-control" ng-change="vm.checkLimit()" ng-model="vm.items[<?php echo $categoria['id'] ?>][<?php echo $item['id']; ?>]" ng-init="vm.items[<?php echo $categoria['id'] ?>][<?php echo $item['id']; ?>] = 0" type="number" name="item_<?php echo $item['id']; ?>" value="0" min="0" data-item='<?php echo json_encode($item); ?>'>
                    <a href="javascript:void(0)" class="btn btn-info btn-minus" ng-click="vm.items[<?php echo $categoria['id'] ?>][<?php echo $item['id']; ?>] = vm.items[<?php echo $categoria['id'] ?>][<?php echo $item['id']; ?>] + 1"><i class="fa fa-plus"></i></a>
				</td>
			</tr>
			<?php endforeach ?>
		</table>
		<?php else: ?>

		<!-- custom form for "other items" -->
		<p>Para outro tipo de bens, utilize o formulário abaixo; poderá adicionar mais que um item, indicado as dimensões do objecto o melhor possível.</p>
		<div class="row" ng-repeat="item in vm.outrosItens">
            <div class="col-md-12">
                <div class="form-group">
                    <button ng-show="!$first" type="button" class="pull-right btn btn-danger" ng-click="vm.removeOutrosItens(item.id)"><i class="fa fa-times"></i></button>
                </div>
            </div>
			<div class="col-md-7">
				<div class="form-group">
					<label>Nome</label>
					<input placeholder="Descrição" class="form-control" type="text" name="{{ 'name_outros_' + ($index + 1) }}">
				</div>
			</div>
			<div class="col-md-5">
				<div class="form-group">
					<label>Quantidade</label>
                    <div class="qty-changer qty-changer-left">
                        <a href="javascript:void(0)" class="btn btn-info btn-plus" ng-click="vm.outrosItens[item.id].quantity = vm.outrosItens[item.id].quantity >= 1 ? (vm.outrosItens[item.id].quantity-1) : vm.outrosItens[item.id].quantity"><i class="fa fa-minus"></i></a>
                        <input type="number" class="form-control" name="{{ 'outros_' + ($index + 1)}}" size="1" ng-change="vm.checkLimit()" ng-model="vm.outrosItens[item.id].quantity" />
                        <a href="javascript:void(0)" class="btn btn-info btn-minus" ng-click="vm.outrosItens[item.id].quantity = vm.outrosItens[item.id].quantity + 1"><i class="fa fa-plus"></i></a>
                    </div>
				</div>

			</div>
			<div class="col-md-12">
				<div class="form-group">
					<label>Dimensões</label>
					<div class="row">
						<div class="col-md-4 no-space"><input ng-change="vm.checkLimit()" ng-model="vm.outrosItens[item.id].largura" placeholder="Largura" class="form-control" type="text" name="{{ 'outros_largura_' + ($index + 1) }}" size="1" maxlength="3" ng-pattern="/^(\d)+$/"></div>
						<div class="col-md-4 no-space"><input ng-change="vm.checkLimit()" ng-model="vm.outrosItens[item.id].comprimento" placeholder="Comprimento" class="form-control" type="text" size="1" name="{{ 'outros_comprimento_' + ($index + 1) }}" maxlength="3" ng-pattern="/^(\d)+$/"></div>
						<div class="col-md-4 no-space"><input ng-change="vm.checkLimit()" ng-model="vm.outrosItens[item.id].altura" placeholder="Altura" class="form-control" type="text" size="1" name="{{ 'outros_altura_' + ($index + 1) }}" id="outros_altura_1" maxlength="3" ng-pattern="/^(\d)+$/"></div>
					</div>
				</div>
				<div class="form-group">
					<button ng-show="$last" type="button" class="pull-right btn btn-info" ng-click="vm.addOutrosItens()"><i class="fa fa-plus"></i></button>
				</div>
			</div>
		</div>
		<!-- /custom form for "other items" -->

		<?php endif; ?>

	</div>
	<?php endforeach ?>
</div>
<!-- /listing & quantities section -->

<br />
<br />
<br />

<!-- kits -->
<h3 class="section-title">Não tem caixas?</h3>
<p>Escolha um dos kits de embalamento para facilitar a acomodação dos seus bens no transporte.</p>

<?php
$sql = "SELECT *, name_" . core::lang() . " AS name
        FROM kits
        WHERE is_active = 1
        ORDER BY sort_order ASC";
$kits = get_sql($sql, 'array');
?>

<div class="row">
	<?php foreach ($kits as $kit):
        $sql = "SELECT b.*, kb.quantity
                FROM boxes b
                INNER JOIN kits_boxes kb ON kb.box_id = b.id
                WHERE kb.kit_id = " . $kit['id'];
        $boxes = get_sql($sql, 'array'); ?>
        <?php if (!$kit["price"]):
            $sql = "SELECT SUM(b.price*kb.quantity)
                            FROM kits_boxes kb
                            INNER JOIN boxes b ON kb.box_id = b.id
                            WHERE kb.kit_id = '" . $kit['id'] . "'";
            $price = current(current(get_sql($sql, 'array')));

            if ($price):
                $kit['price'] = $price;
            endif;
        endif; ?>
	<div class="col-md-3">
        <?php
        $html = '<div style="color: #565a5c"><p class="mb-0">Este kit inclui:</p><table style="margin-top: -15px">';
        foreach ($boxes as $box):
            $html .= '<tr><td style="width: 1px;white-space: nowrap;">' . $box['quantity'] . 'x <img src="https://www.citymover.pt/images/boxes/' . $box['image'] . '" width="40"></td><td>' . $box['name_' . core::lang()] . '</td></p>';
        endforeach;
        $html .= '</table></div>';
        ?>
		<div class="kit" ng-class="{'active': vm.selectedKit.id == <?php echo $kit['id']; ?>}"
             ng-click='vm.selectKit(<?php echo json_encode($kit); ?>)'
             title="<?php echo $kit["name_" . core::lang()]; ?>"
             data-toggle="popover"
             data-trigger="hover"
             data-placement="top"
             data-html="true"
             data-content="<?php echo htmlentities($html); ?>">
			<h4><?php echo $kit["name_" . core::lang()] ?></h4>
			<span><?php echo $kit["description_" . core::lang()] ?></span>

                <span class="kit-price color-red"><?php echo $kit["price"] ?>&euro;</span>
		</div>
	</div>
	<?php endforeach ?>
</div>

<!-- /kits -->

<!-- packed boxes -->
    <div class="row">
        <div class="col-md-8">
            <div class="BoxesClient mt-40">
                <h3 class="section-title">Caixas já embaladas</h3>

                <div id="MoreBoxes">
                    <div class="row pb-20" ng-repeat="caixa in vm.caixasEmbaladas">
                        <div class="col-md-12">
                            <div class="form-group">
                                <button ng-show="!$first" type="button" class="pull-right btn btn-danger" ng-click="vm.removeCaixasEmbaladas(caixa.id)"><i class="fa fa-times"></i></button>
                            </div>
                        </div>
                        <div class="col-md-7">
                            <div class="form-group">
                                <label>Nome</label>
                                <input placeholder="Descrição" type="text" class="form-control" name="{{ 'boxes_nome_' + ($index + 1) }}">
                            </div>
                        </div>
                        <div class="col-md-5">
                            <div class="form-group">
                                <label>Quantidade</label>
                                <div class="qty-changer qty-changer-left">
                                    <a href="javascript:void(0)" class="btn btn-info btn-plus" ng-click="vm.caixasEmbaladas[caixa.id].quantity = vm.caixasEmbaladas[caixa.id].quantity >= 1 ? (vm.caixasEmbaladas[caixa.id].quantity-1) : vm.caixasEmbaladas[caixa.id].quantity"><i class="fa fa-minus"></i></a>
                                    <input ng-change="vm.checkLimit()" ng-model="vm.caixasEmbaladas[caixa.id].quantity" type="number" class="form-control Quant" value="0" size="1" name="{{ 'boxes_quantidade_' + ($index + 1) }}">
                                    <a href="javascript:void(0)" class="btn btn-info btn-minus" ng-click="vm.caixasEmbaladas[caixa.id].quantity = vm.caixasEmbaladas[caixa.id].quantity + 1"><i class="fa fa-plus"></i></a>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <label>Dimensões</label>
                                <div class="row">
                                    <div class="col-md-4 no-space"><input ng-change="vm.checkLimit()" ng-model="vm.caixasEmbaladas[caixa.id].largura" placeholder="Largura" class="input-sm form-control" type="number" name="{{ 'boxes_largura_' + ($index + 1) }}" size="1" maxlength="3" ng-pattern="/^(\d)+$/"></div>
                                    <div class="col-md-4 no-space"><input ng-change="vm.checkLimit()" ng-model="vm.caixasEmbaladas[caixa.id].comprimento" placeholder="Comprimento" class="input-sm form-control" type="number" size="1" name="{{ 'boxes_comprimento_' + ($index + 1) }}" maxlength="3" ng-pattern="/^(\d)+$/"></div>
                                    <div class="col-md-4 no-space"><input ng-change="vm.checkLimit()" ng-model="vm.caixasEmbaladas[caixa.id].altura" placeholder="Altura" class="input-sm form-control" type="number" size="1" name="{{ 'boxes_altura_' + ($index + 1) }}" maxlength="3" ng-pattern="/^(\d)+$/"></div>
                                </div>
                            </div>

                            <div class="form-group">
                                <button ng-show="$last" type="button" class="pull-right btn btn-info" ng-click="vm.addCaixasEmbaladas()"><i class="fa fa-plus"></i></button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /packed boxes -->

            <!-- customer photos -->
            <div>
                <ul class="image-preview-list row d-none">
                    <li class="col-xs-12 col-md-3">
                        <button type="button" class="d-none btn-remove btn-danger"><i class="fa fa-times"></i></button>
                        <input class="file-trigger" type="file" name="photos[]" accept="image/*;capture=camera">
                        <img class="img-fluid img-preview" src="">
                        <textarea placeholder="Descreva esta imagem" class="hidden form-control" name="photo_descriptions[]"></textarea>
                    </li>
                </ul>

                <p class="text-center">
                    <button type="button" class="btn btn-primary btn-add-image"><i class="fa fa-camera"></i> Adicionar imagem / Tirar foto</button>
                </p>
            </div>
            <!-- /customer photos -->
        </div>
        <div class="col-md-4 text-right mt-40">

            <h3>Total a pagar na mudança</h3>
            <table class="table pull-right text-right">
                <tr>
                    <th>Volume ({{ vm.getM3() | number: 2 }}m³)</th>
                    <td>{{ vm.getM3() * vm.m3Price | number: 2 }}&euro;</td>
                </tr>
                <tr ng-if="vm.selectedKit">
                    <th>{{ vm.selectedKit.name }}</th>
                    <td>{{ vm.selectedKit.price | number: 2 }}&euro;</td>
                </tr>
                <tr>
                    <th class="pt-10">Subtotal</th>
                    <td>{{ vm.getTotal() | number: 2 }}&euro;</td>
                </tr>
                <!--tr>
                    <th>Desconto Worten</th>
                    <td>
                        <span class="badge badge-success fz-16">{{ vm.getTotal() - vm.getTotal(true) | number: 2 }}&euro; ({{ vm.discount }}%)</span>
                    </td>
                </tr-->
                <tr>
                    <th>Total</th>
                    <th class="color-red fz-20">{{ vm.getTotal(true) | number: 2 }}&euro;</th>
                </tr>
            </table>
        </div>
    </div>

<div class="row">
	<div class="col-md-12">
		<hr class="form-closure" />
	</div>
	<div class="col-md-6 text-left"><a class="btn btn-info" href="<?php echo base_url("?mod=form&step=1") ?>">Voltar</a></div>
	<div class="col-md-6 text-right" ng-show="vm.getM3() <= vm.m3Limit"><button type="submit" class="btn btn-forward">Avançar <i class="fa fa-chevron-right"></i></button></div>
	<div class="col-md-6 text-right" ng-show="vm.getM3() > vm.m3Limit"><a href="https://www.citymover.pt" class="btn btn-success bg-citymover">Continuar na CityMover <i class="fa fa-chevron-right"></i></a></div>
</div>

</div>

<br />
<br />
<br />
    <input type="hidden" name="kit_id" value="{{ vm.selectedKit ? vm.selectedKit.id : '' }}">
    <input type="hidden" name="kit_price" value="{{ vm.selectedKit ? vm.selectedKit.price : '' }}">
    <input type="hidden" name="kit_name" value="{{ vm.selectedKit ? vm.selectedKit.name  : '' }}">
    <input type="hidden" name="total_step2" value="{{ vm.getTotal() }}">
    <input type="hidden" name="total_with_discount_step2" value="{{ vm.getTotal(true) }}">
    <input type="hidden" name="desconto_step2" value="{{ vm.getTotal() - vm.getTotal(true) }}">
    <input type="hidden" name="discount_percent" value="{{ vm.discount }}">
    <input type="hidden" name="price_m3" value="{{ vm.price_m3 }}">
</form>
