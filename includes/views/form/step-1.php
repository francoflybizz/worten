<?php

$PREDIO_ELEVADOR = $_SESSION['type'] == 'office'?PREDIO__ELEVADOR:PREDIO_ELEVADOR;

$sql = "SELECT * FROM zones WHERE maxorders > 0";
$zones = get_sql($sql, 'array');
?>
<textarea name="zones" class="d-none"><?php echo json_encode($zones); ?></textarea>

<div class='container mt-40' ng-controller="Step1Ctrl as vm">

	<h2>A sua mudança</h2>
	<p>Preencha o formulário abaixo com os detalhes da sua mudança (origem e destino). No próximo passo terá a possibilidade de escolher os itens a transportar.</p>
	<br />

	<div class="row">

	<div class="col-md-12">
		<form method='post' enctype='multipart/form-data' action='<?php echo MUDANCA ?>'>
		<input type='hidden' name='step' value='2' />

		<div class="">
			<div class="form-group">
				<label><?php echo LOCALIDADE_ORIGEM ?></label>
				<input class="form-control" type="text" name="originCity" value="<?php echo $_SESSION['dep_city'] ?>" />
				<div class="OrderError" id="err_originCity">Por favor, indique a localidade de origem</div>
				<!--input type='hidden' name='originCity' id='psd_originCity' value="<?php echo $_SESSION['dep_city'] ?>" /-->
			</div>
		</div>

		<div class="">
			<div class="form-group">
				<label><?php echo CODIGO_POSTAL ?></label>
				<input ui-mask="9999-999"  ui-mask-placeholder ui-mask-placeholder-char="_" ng-model="vm.dep_zip" class="form-control" name="dep_zip" id="dep_zip" value="<?php echo $_SESSION['dep_zip'] ?>" /><a href="//www.ctt.pt/feapl_2/app/open/postalCodeSearch/postalCodeSearch.jspx" target="_blank"><?php echo CTT_VERIFICAR ?></a>
				<div class="OrderError" id="err_dep_zip"><?php echo CODIGO_POSTAL_ORIGEM_ERRO ?></div>
				<div id="dep_helper"></div>
			</div>
		</div>

		<div class="">
			<div class="form-group">
				<label><?php echo MORADA_ORIGEM ?></label>
				<textarea class="form-control" name='address' cols='57' rows='4'><?php echo $_SESSION['address'] ?></textarea>
				<div class='OrderError' id='err_address' style='display:none;'><?php echo MORADA_ORIGEM_ERRO ?></div>
			</div>
		</div>

		<div class="">
			<div class="form-group">
				<label><?php echo PREDIO_ELEVADOR ?></label>
				<label class="radio-inline"><input class='Input' type='radio' name='dep_lift' value='yes' <?php echo ($_SESSION['dep_lift']=='yes'?"checked":"") ?> /> <?php echo SIM ?> </label>
				<label class="radio-inline"><input class='Input' type='radio' name='dep_lift' value='no' <?php echo empty($_SESSION['dep_lift']) || $_SESSION['dep_lift']=='no'?"checked":"" ?> /> <?php echo NAO ?> </label>
				<div class='OrderError' id='err_dep_lift' style='display:none;'><?php echo ELEVADOR_ORIGEM_ERRO ?></div>
			</div>
		</div>

		<div class="">
			<div class="form-group">
				<label><?php echo LOCALIDADE_DESTINO ?></label>
				<input class="form-control" type="text" name="destinationCity" value="<?php echo $_SESSION['dest_city'] ?>" />
				<!--input type='hidden' name='destinationCity' id='psd_destinationCity' value="<?php echo $_SESSION['dest_city'] ?>" /-->
				<div class="OrderError" id="err_destCity">Por favor, indique a localidade de destino</div>
			</div>
		</div>

		<div class="">
			<div class="form-group">
				<label><?php echo CODIGO_POSTAL ?></label>
				<input ui-mask="9999-999"  ui-mask-placeholder ui-mask-placeholder-char="_" ng-model="vm.dest_zip" class="form-control" type="text" name="dest_zip" id="dest_zip" value="<?php echo $_SESSION['dest_zip'] ?>" /><a href="//www.ctt.pt/feapl_2/app/open/postalCodeSearch/postalCodeSearch.jspx" target="_blank"><?php echo CTT_VERIFICAR ?></a>
				<div class="OrderError" id="err_dest_zip"><?php echo CODIGO_POSTAL_DESTINO_ERRO ?></div>
					<div id="dest_helper"></div>
			</div>
		</div>

		<div class="">
			<div class="form-group">
				<label><?php echo MORADA_DESTINO ?></label>
				<textarea class="form-control" name='address_dest' cols='57' rows='4'><?php echo $_SESSION['address_dest'] ?></textarea>
					<div class='OrderError' id='err_address_dest' style='display:none;'><?php echo MORADA_DESTINO_ERRO ?></div>
			</div>
		</div>

		<div class="">
			<div class="form-group">
				<label><?php echo PREDIO_ELEVADOR ?></label>
				<label class="radio-inline"><input class='Input' type='radio' name='dest_lift' value='yes' <?php echo ($_SESSION['dest_lift']=='yes'?"checked":"") ?> /> <?php echo SIM ?></label>
				<label class="radio-inline"><input c class='Input' type='radio' name='dest_lift' value='no' <?php echo empty($_SESSION['dest_lift']) || $_SESSION['dest_lift']=='no'?"checked":"" ?> /><?php echo NAO ?></label>
				<div class='OrderError' id='err_dest_lift' style='display:none;'><?php echo ELEVADOR_DESTINO_ERRO ?></div>
			</div>
		</div>

		<div class="">
			<div class="form-group">
				<label><?php echo DATA_MUDANCA ?></label>
				<input class="form-control datepicker" type="text" name="data_entrega" value="<?php echo $_SESSION['data_entrega'] ?>" />
				<input type='hidden' name='day' id='psd_day' size='2' maxlength='2' value="<?php echo substr($_SESSION['data_entrega'],0,2) ?>" />
				<input type='hidden' name='month' id='psd_month' size='2' maxlength='2' value="<?php echo substr($_SESSION['data_entrega'],3,2) ?>" />
				<input type='hidden' name='year' id='psd_year' size='2' maxlength='2' value="<?php echo substr($_SESSION['data_entrega'],6,4) ?>" />
			</div>
		</div>

		<?php if ($_SESSION['type'] == 'office'): ?>

			<div class="">
				<div class="form-group">
					<label><?php echo EMPRESA ?></label>
					<input class="form-control" size='60' type='hidden' name='type' id='type' value='<?php echo $_SESSION['type'] ?>' />
					<input class="form-control" size='60' type='text' name='company' id='psd_company' value='<?php echo $_SESSION['company'] ?>' />
					<div class='OrderError' id='err_company' style='display:none;'><?php echo EMPRESA_ERRO ?></div>
				</div>
			</div>

		<?php endif; ?>

		<?php $NOME = $_SESSION['type'] == 'office' ? NOME_CONTACTO:NOME; ?>

		<div class="">
			<div class="form-group">
				<label><?php echo $NOME ?></label>
				<input class="form-control" size='60' type='text' name='name' id='psd_name' value="<?php echo $_SESSION['name'] ?>" onkeypress='return isAlphaKey(event)' />
				<div class='OrderError' id='err_name' style='display:none;'><?php echo NOME_ERRO ?></div>
			</div>
		</div>

		<div class="">
			<div class="form-group">
				<label><?php echo TELEFONE ?></label>
				<input class="form-control" size='25' type='text' name='phone' id='psd_phone' value="<?php echo $_SESSION['phone'] ?>" onkeypress='return isNumber(event,this);'>
				<div class='OrderError' id='err_phone' style='display:none;'><?php echo TELEFONE_ERRO ?></div>
			</div>
		</div>

		<div class="">
			<div class="form-group">
				<label><?php echo TELEMOVEL ?></label>
				<input class="form-control" size='25' type='text' name='mobile' id='psd_mobile' value='<?php echo $_SESSION['mobile'] ?>' onkeypress='return isNumber(event,this);'>
			</div>
		</div>

		<div class="">
			<div class="form-group">
				<label>Email</label>
				<input class="form-control" size='60' type='text' name='email' id='psd_email' value='<?php echo $_SESSION['email'] ?>' />
				<div class='OrderError' id='err_email' style='display:none;'><?php echo EMAIL_ERRO ?></div>
			</div>
		</div>

		<div class="">
			<div class="form-group">
				<label><?php echo NUMERO_CONTRIBUINTE ?></label>
				<input class="form-control" size='25' type='text' name='taxnumber' id='psd_taxnumber' value='<?php echo $_SESSION['taxnumber'] ?>' onkeypress='return isNumber(event,this);'>
				<div class='OrderError' id='err_taxnumber' style='display:none;'><?php echo NUMERO_CONTRIBUINTE_ERRO ?></div>
			</div>
		</div>

		<div class="">
			<hr class="form-closure" />
			<div class="row">
				<div class="col-md-6 text-left"><input class='FormBtmPrev btn btn-default' type='button' name='go_back' value='<?php echo PAGINA_INICIAL ?>' onclick='window.location="<?php echo base_url(); ?>";' /> &nbsp; </div>
				<div class="col-md-6 text-right" ng-show="!vm.invalidZone"><button type="submit" class='FormBtmNext btn btn-success' type='button' name='step_1_submit' onclick='Validar(this.form, "1");return false;'><?php echo PROXIMO_PASSO ?> <i class="fa fa-chevron-right"></i></button></div>
				<div class="col-md-6 text-right" ng-show="vm.invalidZone"><a href="https://www.citymover.pt" class="btn btn-success bg-citymover">Continuar na CityMover <i class="fa fa-chevron-right"></i></a></div>
			</div>
		</div>

		<br>
		<br>

	</form>

	</div>


	</div>

</div>
