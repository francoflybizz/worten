<?

define('NOME_SITE', 'Moving, Storage, Freight forwarding, Logistics - cityMover');

# Erros
define("ERRO_1","A fatal error occured. Please return to Step 1 and try again.");

# Links steps .htaccess
# PARA TRADUÇÃO EM INGLÊS, VERIFICAR EM HTACCESS
define("OS_SEUS_DADOS","your-data");
define("MUDANCA","moving");
define("SEGURO_E_ACESSIBILIDADES","insurance-and-accessibility");
define("CONFIRMACAO","confirmation");
define("PAGAMENTO","payment");
define("CONFERIR_OBJETOS","check-over");
define("APOIO_CLIENTE","Client Support");

# Datas
define("JANEIRO","January");
define("FEVEREIRO","February");
define("MARCO","March");
define("ABRIL","April");
define("MAIL","May");
define("JUNHO","June");
define("JULHO","July");
define("AGOSTO","August");
define("SETEMBRO","September");
define("OUTUBRO","October");
define("NOVEMBRO","November");
define("DEZEMBRO","December");

define("DOMINGO","Sunday");
define("SEGUNDA","Monday");
define("TERCA","Tuesday");
define("QUARTA","Wednesday");
define("QUINTA","Thursday");
define("SEXTA","Friday");
define("SABADO","Saturday");

define("EM"," in ");
define("E"," and ");
define("NA"," on ");
define("IMPRIMIR","Print");
define("SAIR","Exit");
define("PAGINA_INICIAL","Homepage");
define("NECESSITA_AJUDA","<img src='images/icon_faqs.png' align='absmiddle' /> FAQ");
define("DUVIDAS","If you have problems or doubts, please contact us: <img src='images/icon_phone.png' align='absmiddle' alt='Phone' /> +351 21 478 94 00&nbsp;&nbsp;&nbsp;<a href='mailto:info@citymover.pt'><img src='images/icon_email.png' align='absmiddle' alt='Email' /></a>&nbsp;");

# Banner
define("TITULO_1","what to move");
define("TITULO_2","when to move");
define("TITULO_3","competitive rates");
define("TITULO_4","the boxes & materials you need");
define("TITULO_5","where to recycle");
define("TITULO_6","Please fill our online form and schedule your move");

# Index
define("TIPO_MUDANCA","Type of move");
define("HABITACAO","Home");
define("EMPRESA","Company");
define("EMPRESA","Company");
define("CODIGO_POSTAL_ORIGEM_ERRO","Fill in a valid origin zip code");
define("CODIGO_POSTAL_DESTINO_ERRO","Fill in a valid destination zip code");
define("CTT_VERIFICAR","Verify zip code on CTT website");
define("DATA_MUDANCA_INDEX_ERRO","Fill in the move date");
define("VER_CALENDARIO","View calendar");
define("AVANCAR","proceed >>");
define("CODIGO_POSTAL_INCORRECTO","Invalid zip code");

# Header
define("SEUS_DADOS","Your info");
define("CONFIRMACAO_","Confirm");
define("PAGAMENTO_","Payment");
define("O_SEU_ORCAMENTO","Your quote");
define("MENSAGEM_FINAL","Final message");

# Passo 1
define("LOCALIDADE_ORIGEM","Moving from");
define("MORADA_ORIGEM","From address");
define("CODIGO_POSTAL","Zip code");
define("LOCALIDADE_DESTINO","Moving to");
define("MORADA_DESTINO","To address");
define("DATA_MUDANCA","Move date");
define("EMPRESA","Company");
define("NOME","Name");
define("NOME_CONTACTO","Contact name");
define("TELEFONE","Phone");
define("TELEMOVEL","Cell phone");
define("TELEMOVEL_","Cell phone");
define("NUMERO_CONTRIBUINTE","VAT Number");
define("PREDIO_ELEVADOR","Building with elevator");
define("PREDIO__ELEVADOR","Building with elevator");

define("LOCALIDADE_ORIGEM_ERRO","Please fill in a valid departure town");
define("MORADA_ORIGEM_ERRO","Please fill in a valid departure address");
define("CODIGO_POSTAL_ERRO","Please fill in a valid zip code");
define("LOCALIDADE_DESTINO_ERRO","Please fill in a valid destination town");
define("MORADA_DESTINO_ERRO","Please fill in a valid destination address");
define("DATA_MUDANCA_ERRO","Moves must be scheduled at least ten days in advance.");
define("EMPRESA_ERRO","Please fill in the company name");
define("NOME_ERRO","Please fill in your name");
define("TELEFONE_ERRO","Fill in at least one contact number");
define("EMAIL_ERRO","Please fill in a valid email address");
define("NUMERO_CONTRIBUINTE_ERRO","Please make sure you filled in a valid VAT Number");
define("ELEVADOR_ORIGEM_ERRO","Please inform us if the departure building has an elevator.");
define("ELEVADOR_DESTINO_ERRO","Please inform us if the destination building has an elevator.");

# Passo 2
define("MENSAGEM_P2_4_HABITACAO","cityMover boxes to buy");
define("MENSAGEM_P2_4_EMPRESA","cityMover containers to rent");
define("CAIXAS_EMBALADAS","Boxes already packed");
define("NUMERO_CAIXAS","Number");
define("VOLUME","Volume");
define("IMAGEM","Image");
define("NOME","Name");
define("PRECO","Price");
define("QUANTIDADE","Quantity");
define("LARGURA","Width");
define("COMPRIMENTO","Length");
define("ALTURA","Height");
define("NOVO_ITEM","New Box");

# Passo 3
define("OUTROS","Insurance & Accessibility");
define("NECESSITA_SEGURO","Do you need insurance?");
define("SIM","Yes");
define("NAO","No");
define("CONFERIR_OBJETOS_SELECCIONADOS","To pick items to insure click here");
define("ITENS_SEGURAR","Items to insure");
define("VALOR_ITEMS_EUROS","Items value (Euros)");
define("VALOR_SEGURO_EUROS","Insurance value (Euros");
define("NECESSITA_SEGURO_ERRO","Please choose if you need insurance or not");
define("VALOR_ITEMS_ERRO","Please fill in the value of items to insure");
define("LCA","Width x Length x Height");
define("LCA_ABREVIADO","W. x L. x H.");
define("ITEMS_SEGURAR","Please select below which items don't fit in the elevator");
#define("ITEMS_SEGURAR","Selecione os &iacute;tems abaixo que desejas segurar");
define("VALOR","Value");
define("ELEVADOR","Elevator");
define("ELEVADOR_ERRO","Please select if there are items that don't fit in the elevator");
define("NAO_CABEM_ELEVADOR","Items that don't fit in the elevator");
define("NAO_CABEM_ELEVADOR_ERRO","Please tell us which items don't fit in the elevator");

#define("ESTACIONAMENTO","Parking");
#define("ESTACIONAMENTO_OPCAO_1","Reserve parking(99,80 Euros)");
#define("ESTACIONAMENTO_OPCAO_2","Reserve a policy agent to regulate transit in parking (75 Euros)"); # Traduzir em pdf.php
#define("VALOR_ESTACIONAMENTO_EUROS","Parking cost (Euros)");

define("ESTACIONAMENTO","Parking");
define("ESTACIONAMENTO_2016","Temporary traffic conditionings");
define("ESTACIONAMENTO_OPCAO_1","License for temporary traffic conditioning (99,80 Euros)");
define("ESTACIONAMENTO_OPCAO_2","Traffic regulation support by the Police - minimum 4 hour period (75,00 Euros)"); # Traduzir em pdf.php
define("VALOR_ESTACIONAMENTO_EUROS","Parking cost (Euros)");


# Passo 4
define("DADOS_MUDANCA","Moving info");
define("MUDANCA_HORARIO","(Movings start at 8:30am)");
define("CATEGORIAS","Categories:");
define("ITEMS","Items");
define("CAIXAS","Boxes");
define("COMENTARIOS","Comments");
define("CAIXAS_ADICIONAIS","Do you wish to purchase additional boxes?");
define("TERMOS_CONDICOES_1","I've read and I accept the TOS ");
define("TERMOS_CONDICOES_2","terms and conditions");
define("TERMOS_CONDICOES_ERRO","Please select one of the options");
define("CONFERIR_CAIXAS","To confirm the selected boxes click here");
define("ITEMS_TRANSPORTAR","Items to move");
define("SEM_CAIXAS_SELECIONADAS","No boxes selected");
define("SEM_CAIXAS_EMBALADAS","No boxes packed");
define("AGENTE_PSP","Traffic regulation support by Police officer - minimum 4 hour period");
define("DESCONTO","Discount");
define("COMISSAO","Commission");
define("V6_COMISSAO","Commission");


# Passo 5
define("REFERENCIA","References");
define("ORCAMENTO","Quote");
define("MUDANCA_","Move");
define("MATERIAIS_DE_EMBALAGEM_","Packing materials");
define("SEGURO","Insurance");
define("VERSAO_IMPRIMIR","Print version");
define("NAO_ORCAMENTO","Reject this quote - exit");
define("SIM_ORCAMENTO","Accept this quote");
define("FORMA_PAGAMENTO","Please select a payment option:");
define("TRANSFERENCIA_BANCARIA","Bank transfer");
define("CHEQUE_NUMERARIO","Check or Cash");
define("PARA","to");
define("CONFIRMAR","Confirm");
define("FECHAR_FORMULARIO","Close form");
define("TOTAL_SINAL_RESERVA_ONLINE","Online Deposit");

# Botões
define("PROXIMO_PASSO","Next step");
define("PASSO_ANTERIOR","Previous step");

define("MAPADOSITE","Sitemap");
define("PAGINAS","Pages");
define("PASSO","Step");

# PDF
define("DADOS_PESSOAIS","Personal info");
define("VALOR_SEGURO","Values for insurance");

# Questionário
define("PERGUNTA_ERRO","Please select one of the options below");
define("COMENTARIO_ERRO","Please fill in the text field above or choose another option");
define("ENVIAR","&nbsp;&nbsp;&nbsp;Send");
define("COMENTARIOS","Comments");
define("CODIGO_VERIFICACAO","Verification code");
define("CAPCHA_ERRO_1","Error while saving the message, please try again"); //
define("CAPCHA_ERRO_2","Invalid verification code");

define("DIAS","Days");
define("POSSUI_CADASTRADO","Are you a registered user?");
define("UTILIZADOR","User");
define("PALAVRA_PASSE","Password");

# Serviços de baixo
define("MUDANCAS","moves");
define("MUDANCAS_BODY","Creative moving solutions for people and companies within the Lisbon metropolitan area.");
define("ARMAZENAGEM","storage");
define("ARMAZENAGEM_BODY","Storage solutions for furniture, materials and computer equipment in individual, pratical safe boxes.");
define("TECNOLOGIA","technology");
define("TECNOLOGIA_BODY","Specialized solutions for moving servers, infrastructures and all electronic equipment.");
define("LOGISTICA","Logistics");
define("LOGISTICA_BODY","Transport, stowage and assembly solutions from the plant to construction site including waste pickup and recycling.");

#rotator
define("R_FIRST", "Schedule your move<br />Request your free quote!");
define("R_SECOND", "Fill in your move details<br />in the form below!");

#testemunhos
define("L_TESTIMONIALS", "Testemunhos");

#press
define("L_PRESS", "Press");

#kwicks
define("K_MUDANCAS", "House & Office Moves");
define("TRANSITARIO", "Freight Forwarder");
define("K_ARMAZENAGEM", "Storage");
define("K_OUTROS", "Other Services");

#home
define("MUDANCAS_ESCRITORIO","Office and industrial moves");
define("MUDANCAS_HABITACAO","Home moves");
define("GESTAO_MUDANCAS","Moving management");
define("ALUGUER_MOB","Office furniture rental");
define("ALUGUER_CONTENTORES","We rent containers for your move");
define("REC_ARQUIVO","File recycling");
define("REC_MOBILIARIO","Furniture recycling");
define("DONATIVO","We help you donate the items you no longer need");

define("ARMAZENAMENTO_MOB","Furniture storage");
define("BOXES_INDIVIDUAIS","Individual boxes with access control");
define("CODIGOS_BARRAS","Location and warehouse management via barcode");
define("ARMAZENAGEM_ARQUIVOS","File storage");

define("GESTAO_DOC","Document Management");
define("TECNOLOGIA2","Technology");
define("LOGISTICA2","Logistics");
define("MUD_INTER","International Moves");

define("GESTAO_DOC_URL","./document-management");
define("TECNOLOGIA2_URL","./technology");
define("LOGISTICA2_URL","./logistics");
define("MUD_INTER_URL","./international-moving-services");
define("ARM_ALFANDE_EXPORT_URL","./storage");
define("ARM_ALFANDE_EXPORT","Bonded warehouse for import/export");
define("EMISSAO_DME","DME Issuing");
define("VIA_MARITIMA","Sea transport");
define("VIA_AEREA","Air transport");
define("VIA_TERRESTRE","Road transport (Europe)");
define("CONTENTORES_GRUPAGEM","Full containers and groupage services");
define("TRANSITARIO_URL","./freight-forwarder");
define("ARMAZENAGEM_URL","./storage");


/* .pdf_v5 */
define("V6_DIAS", "Days");
define("V6_VALOR", "Value");
define("V6_VALOR_EFEITO", "Values for");
define("V6_DE_SEGURO", "insurance");
define("V6_TOTAL_EXCLUINDO_MUD", "Total (move not included)");
define("V6_TOTAL_EXCLUINDO_MUD_IVA", "Total (move not included) + VAT");
define("V6_FACTURA_MIN_HORA", "Billed from start to end of the move plus the trip cost. Minimal billing time is 2 hours.");
define("V6_FACTURA_NAO_MEIAS_HORAS", "We don't charge half hours");
define("V6_FACTURA_MIN_HORA_2", "We don't charge half hours");
define("V6_ACRESCE_IVA", "VAT is not included");
define("V6_DESCONHECIDO", "Unknown / Direct");
define("V6_TELEMOVEL", "Phone");
define("V6_EMAIL", "Email");
define("V6_CONTRIBUINTE", "VAT Number");


define("INQ_VIATURAS", "Vehicles");
define("INQ_ESTADO_CONSEVACAO", "Conservation");
define("INQ_DISPONIBILIZADO", "Loading and unloading equipment used by cityMover");
define("INQ_IMAGEM_EXTERIOR", "Exterior image (presentation)");
define("INQ_LIMPEZA", "Cleanliness");
define("INQ_OPERADORES", "Operators");
define("INQ_APRESENTACAO", "Presentation");
define("INQ_PROFISSIONALISMO", "Professionalism");
define("INQ_CONHECIMENTOS_TECNICOS", "Technical knowledge");
define("INQ_CUMPRIMENTO_HORARIOS", "Compliance with agreed times & dates");
define("INQ_ACTUACAO", "Response to unpredicted situations");
define("INQ_COMPRIMENTO", "Compliance with safety standards");
define("INQ_SERVICO_COMERCIAL", "Commercial service");
define("INQ_LOGISTICA_", "Logistics / Shipping");

define("INQ_ESCRITORIO", "Office moves");
define("INQ_HABITACAO", "House Moves");
define("INQ_LOGISTICA", "Logistics");
define("INQ_ARMAZENAMENTO", "Storage");
define("INQ_OUTRO", "Other");
define("INQ_COMENTARIOS", "Comments");

define("INQ_MAU", "Bad");
define("INQ_INSUFICIENTE", "Insufficient");
define("INQ_SUFICIENTE", "Sufficient");
define("INQ_BOM", "Good");
define("INQ_MUITO_BOM", "Very Good");

define("LICENCAS_E_ESTACIONAMENTO", "Licenses and Parking");
define("SERVICOS_DE_POLICIAMENTO", "Policing Services");

?>
