<?
# Erros
define("ERRO_1","Ocorreu um erro. Por favor volte ao Passo 1 e tente novamente.");

# Links steps .htaccess
# PARA TRADUÇÃO EM INGLÊS, VERIFICAR EM HTACCESS
define("OS_SEUS_DADOS","os-seus-dados");
define("MUDANCA","mudanca");
define("SEGURO_E_ACESSIBILIDADES","seguro-e-acessibilidades");
define("CONFIRMACAO","confirmacao");
define("PAGAMENTO","pagamento");
define("CONFERIR_OBJETOS","conferir-objetos");
define("APOIO_CLIENTE","Apoio ao Cliente");

# Datas
define("JANEIRO","Janeiro");
define("FEVEREIRO","Fevereiro");
define("MARCO","Mar&ccedil;o");
define("ABRIL","Abril");
define("MAIL","Maio");
define("JUNHO","Junho");
define("JULHO","Julho");
define("AGOSTO","Agosto");
define("SETEMBRO","Setembro");
define("OUTUBRO","Outubro");
define("NOVEMBRO","Novembro");
define("DEZEMBRO","Dezembro");

define("DOMINGO","Domingo");
define("SEGUNDA","Segunda-feira");
define("TERCA","Ter&ccedil;a-feira");
define("QUARTA","Quarta-feira");
define("QUINTA","Quinta-feira");
define("SEXTA","Sexta-feira");
define("SABADO","S&aacute;bado");

define("EM"," em ");
define("E"," e ");
define("NA"," na ");
define("IMPRIMIR","Imprimir");
define("SAIR","Sair");
define("PAGINA_INICIAL","Página inicial");
define("NECESSITA_AJUDA","<img src='images/icon_faqs.png' align='absmiddle' /> FAQ");
define("DUVIDAS","Caso tenha d&uacute;vidas, n&atilde;o hesite em contactar-nos: <img src='images/icon_phone.png' align='absmiddle' /> 21 478 94 00&nbsp;&nbsp;&nbsp;<a href='mailto:info@citymover.pt?subject=Questão no preenchimento do formulário cityMover'><img src='images/icon_email.png' align='absmiddle' /></a>&nbsp;");

# Banner
define("TITULO_1","o que mudar");
define("TITULO_2","quando mudar");
define("TITULO_3","o pre&ccedil;o mais competitivo");
define("TITULO_4","as caixas e materiais que necessita");
define("TITULO_5","onde reciclar");
define("TITULO_6","Preencha o formul&aacute;rio online e marque j&aacute; a sua mudan&ccedil;a");

# Index
define("TIPO_MUDANCA","Tipo de mudan&ccedil;a");
define("HABITACAO","Habita&ccedil;&atilde;o");
define("EMPRESA","Empresa");
define("EMPRESA","Empresa");
define("CODIGO_POSTAL_ORIGEM_ERRO","Preencha um c&oacute;digo postal de origem v&aacute;lido");
define("CODIGO_POSTAL_DESTINO_ERRO","Preencha um c&oacute;digo postal de destino v&aacute;lida");
define("CTT_VERIFICAR","Verificar via CTT");
define("DATA_MUDANCA_INDEX_ERRO","Preencha a data que deseja para a mudan&ccedil;a");
define("VER_CALENDARIO","Ver calend&aacute;rio");
define("AVANCAR","avan&ccedil;ar &raquo;");
define("CODIGO_POSTAL_INCORRECTO","C&oacute;digo Postal incorrecto");

# Header
define("SEUS_DADOS","Os seus dados");
define("CONFIRMACAO_","Confirma&ccedil;&atilde;o");
define("PAGAMENTO_","Pagamento");
define("O_SEU_ORCAMENTO","O seu orçamento");
define("FORM_O_SEU_ORCAMENTO", "o-seu-orcamento");
define("MENSAGEM_FINAL","Mensagem final");

# Passo 1
define("LOCALIDADE_ORIGEM","Localidade de origem");
define("MORADA_ORIGEM","Morada de origem");
define("CODIGO_POSTAL","C&oacute;digo postal");
define("LOCALIDADE_DESTINO","Localidade de destino");
define("MORADA_DESTINO","Morada de destino");
define("DATA_MUDANCA","Data da mudan&ccedil;a");
define("EMPRESA","Empresa");
define("NOME","Nome");
define("NOME_CONTACTO","Nome de contacto");
define("TELEFONE","Telefone");
define("TELEMOVEL","Telem&oacute;vel");
define("TELEMOVEL_","Telemóvel");
define("NUMERO_CONTRIBUINTE","N&ordm; contribuinte");
define("PREDIO_ELEVADOR","Habita&ccedil;&atilde;o com elevador");
define("PREDIO__ELEVADOR","Pr&eacute;dio com elevador");

define("LOCALIDADE_ORIGEM_ERRO","Preencha uma localidade de origem v&aacute;lida");
define("MORADA_ORIGEM_ERRO","Por favor preencha a morada de origem");
define("CODIGO_POSTAL_ERRO","Por favor preencha o c&oacute;digo postal");
define("LOCALIDADE_DESTINO_ERRO","Preencha a localidade de destino");
define("MORADA_DESTINO_ERRO","Por favor preencha a morada de destino");
define("DATA_MUDANCA_ERRO","A data deve ter pelo menos uma semana de anteced&ecirc;ncia");
define("EMPRESA_ERRO","Por favor preencha o nome da empresa");
define("NOME_ERRO","Por favor preencha o seu nome");
define("TELEFONE_ERRO","Preencha pelo menos um n&uacute;mero de contacto");
define("EMAIL_ERRO","Por favor preencha correctamente o seu email");
define("NUMERO_CONTRIBUINTE_ERRO","Por favor preencha o seu n&ordm; contribuinte");
define("ELEVADOR_ORIGEM_ERRO","Por favor informe se h&aacute; elevador na habita&ccedil;&atilde;o de origem");
define("ELEVADOR_DESTINO_ERRO","Por favor informe se h&aacute; elevador no habita&ccedil;&atilde;o de destino");

# Passo 2
define("MENSAGEM_P2_4_HABITACAO","Caixas cityMover a adquirir");
define("MENSAGEM_P2_4_EMPRESA","Contentores cityMover a alugar");
define("CAIXAS_EMBALADAS","Caixas j&aacute; embaladas");
define("NUMERO_CAIXAS","N&uacute;mero de caixas");
define("VOLUME","Volume");
define("IMAGEM","Imagem");
define("NOME","Nome");
define("PRECO","Pre&ccedil;o");
define("QUANTIDADE","Quantidade");
define("LARGURA","Largura");
define("COMPRIMENTO","Comprimento");
define("ALTURA","Altura");
define("NOVO_ITEM","Novo &iacute;tem");

# Passo 3
define("OUTROS","Seguro e Acessibilidades");
define("NECESSITA_SEGURO","Necessita de seguro?");
define("SIM","Sim");
define("NAO","N&atilde;o");
define("CONFERIR_OBJETOS_SELECCIONADOS","Para conferir os objectos seleccionados, clique aqui");
define("ITENS_SEGURAR","Itens a segurar");
define("VALOR_ITEMS_EUROS","Valor dos &iacute;tens em Euros");
define("VALOR_SEGURO_EUROS","Valor do seguro em Euros");
define("NECESSITA_SEGURO_ERRO","Por favor indique se necessita de seguro");
define("VALOR_ITEMS_ERRO","Por favor insira o valor dos bens a segurar");
define("LCA","Largura x Comprimento x Altura");
define("LCA_ABREVIADO","Larg. x Comp. x Alt.");
define("ITEMS_SEGURAR","Seleccione abaixo os itens que não cabem no elevador");
#define("ITEMS_SEGURAR","Selecione os &iacute;tems abaixo que desejas segurar");
define("VALOR","Valor");
define("ELEVADOR","Elevador");
define("ELEVADOR_ERRO","Por favor indique se h&aacute; items");
define("NAO_CABEM_ELEVADOR","Items que n&atilde;o cabem no elevador");
define("NAO_CABEM_ELEVADOR_ERRO","Indique quais os items que n&atilde;o cabem no elevador");
define("ESTACIONAMENTO","Estacionamento");
define("ESTACIONAMENTO_2016","Condicionamentos temporários de trânsito (por troço de via e/ou cruzamento)");
define("ESTACIONAMENTO_OPCAO_1","Licen&ccedil;a para Condicionamentos tempor&aacute;rios de tr&acirc;nsito (por tro&ccedil;o de via e/ou cruzamento) (99,80 Euros)");
define("ESTACIONAMENTO_OPCAO_2","Apoio &agrave; regula&ccedil;&atilde;o do transito pela PSP &ndash; per&iacute;odos m&iacute;nimos de 4 horas (75 Euros)"); # Traduzir em pdf.php
define("VALOR_ESTACIONAMENTO_EUROS","Valor do estacionamento em Euros");

# Passo 4
define("DADOS_MUDANCA","Dados da mudan&ccedil;a");
define("MUDANCA_HORARIO","(As mudan&ccedil;as t&ecirc;m lugar a partir das 08:30)");
define("CATEGORIAS","Categorias:");
define("ITEMS","&Iacute;tens");
define("CAIXAS","Caixas");
define("COMENTARIOS","Coment&aacute;rios");
define("CAIXAS_ADICIONAIS","Deseja adquirir caixas adicionais?");
define("TERMOS_CONDICOES_1","Declaro que li e que aceito os ");
define("TERMOS_CONDICOES_2","termos e condi&ccedil;&otilde;es");
define("TERMOS_CONDICOES_ERRO","Por favor seleccione uma das op&ccedil;&otilde;es");
define("CONFERIR_CAIXAS","Para conferir as caixas seleccionadas, clique aqui");
define("ITEMS_TRANSPORTAR","&Iacute;tens a transportar");
define("SEM_CAIXAS_SELECIONADAS","N&atilde;o h&aacute; há caixas seleccionadas");
define("SEM_CAIXAS_EMBALADAS","N&atilde;o h&aacute; caixas embaladas");
define("AGENTE_PSP","Apoio &agrave; regula&ccedil;&atilde;o do tr&acirc;nsito pela PSP &ndash; per&iacute;odos m&iacute;nimos de 4 horas");
define("DESCONTO","Desconto");
define("COMISSAO","Comiss&atilde;o");
define("V6_COMISSAO","Comissão");


# Passo 5
define("REFERENCIA","Refer&ecirc;ncia");
define("ORCAMENTO","Or&ccedil;amento");
define("MUDANCA_","Mudan&ccedil;a");
define("MATERIAIS_DE_EMBALAGEM_","Materiais de Embalagem");
define("SEGURO","Seguro");
define("VERSAO_IMPRIMIR","Vers&atilde;o para imprimir");
define("NAO_ORCAMENTO","N&atilde;o aceito este or&ccedil;amento - sair");
define("SIM_ORCAMENTO","Aceito este or&ccedil;amento");
define("FORMA_PAGAMENTO","Escolha uma forma de pagamento:");
define("TRANSFERENCIA_BANCARIA","Transfer&ecirc;ncia Banc&aacute;ria");
define("CHEQUE_NUMERARIO","Cheque ou numer&aacute;rio");
define("PARA","para");
define("CONFIRMAR","Confirmar");
define("FINALIZAR","Finalizar");
define("FECHAR_FORMULARIO","Fechar janela");
define("TOTAL_SINAL_RESERVA_ONLINE","Sinal para Reserva Online");

# Botões
define("PROXIMO_PASSO","Pr&oacute;ximo passo");
define("PASSO_ANTERIOR","Passo anterior");

define("MAPADOSITE","Mapa do Site");
define("PAGINAS","P&aacute;ginas");
define("PASSO","Passo");

# PDF
define("DADOS_PESSOAIS","Dados Pessoais");
define("VALOR_SEGURO","Valores para efeito de seguro");

# Questionário
define("PERGUNTA_ERRO","Por favor, marque uma das alternativas abaixo");
define("COMENTARIO_ERRO","Por favor, informe na caixa de texto acima ou seleccione outra alternativa");
define("ENVIAR","&nbsp;&nbsp;&nbsp;Enviar");
define("COMENTARIOS","Coment&aacute;rios");
define("CODIGO_VERIFICACAO","C&oacute;digo de verifica&ccedil;&atilde;o");
define("CAPCHA_ERRO_1","Ocorreu um erro ao gravar os dados, por favor tente novamente"); // Error while saving the message, please, try again
define("CAPCHA_ERRO_2","C&oacute;digo de verifica&ccedil;&atilde;o inv&aacute;lido");

define("DIAS","Dias");
define("POSSUI_CADASTRADO","J&aacute; &eacute; cliente registado?");
define("UTILIZADOR","Utilizador");
define("PALAVRA_PASSE","Palavra-passe");

# Serviços de baixo
define("MUDANCAS","mudan&ccedil;as");
define("MUDANCAS_BODY","Solu&ccedil;&otilde;es criativas de mudan&ccedil;as para empresas e particulares, em toda a Grande Lisboa.");
define("ARMAZENAGEM","armazenagem");
define("ARMAZENAGEM_BODY","Solu&ccedil;&otilde;es para materiais, mob&iacute;lias e equipamento inform&aacute;tico, em boxes individuais pr&aacute;ticas e seguras.");
define("TECNOLOGIA","tecnologia");
define("TECNOLOGIA_BODY","Solu&ccedil;&otilde;es especializadas em mudan&ccedil;as de servidores, infra-estruturas, e todo o equipamento inform&aacute;tico.");
define("LOGISTICA","log&iacute;stica");
define("LOGISTICA_BODY","Solu&ccedil;&otilde;es de transporte, arruma&ccedil;&atilde;o e montagem desde a f&aacute;brica at&eacute; &agrave; obra incluindo recolha e reciclagem.");

#rotator
define("R_FIRST", "Agende a sua mudan&ccedil;a<br />Or&ccedil;amento gr&aacute;tis!");
define("R_SECOND", "Preencha abaixo os dados<br />da sua mudan&ccedil;a!");

#testemunhos
define("L_TESTIMONIALS", "Testemunhos");

#press
define("L_PRESS", "Imprensa");

#kwicks
define("K_MUDANCAS", "Mudan&ccedil;as");
define("K_TRANSITARIO", "Transitário");
define("K_ARMAZENAGEM", "Armazenagem");
define("K_OUTROS", "Outros Servi&ccedil;os");

#home
define("MUDANCAS_ESCRITORIO","Mudan&ccedil;as de escrit&oacute;rios e industriais");
define("MUDANCAS_HABITACAO","Mudan&ccedil;as de habita&ccedil;&atilde;o");
define("GESTAO_MUDANCAS","Gest&atilde;o da mudan&ccedil;a");
define("ALUGUER_MOB","Alugamos mobili&aacute;rio de escrit&oacute;rio");
define("ALUGUER_CONTENTORES","Alugamos caixas e contentores para os seus bens");
define("REC_ARQUIVO","Reciclagem de arquivos");
define("REC_MOBILIARIO","Reciclagem de mobili&aacute;rio");
define("DONATIVO","Auxiliamos no donativo dos seus bens");

define("ARM_ALFANDE_EXPORT","Armaz&eacute;m alfandegado para exporta&ccedil;&atilde;o");
define("VIA_MARITIMA","Via mar&iacute;tima");
define("VIA_AEREA","Via a&eacute;rea");
define("VIA_TERRESTRE","Via terrestre (Europa)");
define("CONTENTORES_GRUPAGEM","Contentores completos e servi&ccedil;os de grupagem, com maior incid&ecirc;ncia nos PALOPs");
define("EMISSAO_DME","Emiss&atilde;o de DME");

define("ARMAZENAMENTO_MOB","Armazenagem de mobili&aacute;rio");
define("BOXES_INDIVIDUAIS","Boxes individuais com acesso exclusivo");
define("CODIGOS_BARRAS","Localiza&ccedil;&atilde;o em armaz&eacute;m por c&oacute;digo de barras");
define("ARMAZENAGEM_ARQUIVOS","Armazenagem de arquivos");

define("MUD_INTER","Mudan&ccedil;as Internacionais");
define("TECNOLOGIA2","Tecnologia");
define("LOGISTICA2","Log&iacute;stica");
define("GESTAO_DOC","Gest&atilde;o Documental");

define("GESTAO_DOC_URL","./gestao-documental");
define("TECNOLOGIA2_URL","./mudancas-tecnologia-fp23.html");
define("LOGISTICA2_URL","./page-logistica-fp24.html");
define("MUD_INTER_URL","./mudancas-internacionais");
define("TRANSITARIO_URL","./transitario");
define("ARMAZENAGEM_URL","./armazenagem-fp22.html");


/* .pdf_v5 */
define("V6_DIAS", "Dias");
define("V6_VALOR", "Valor");
define("V6_VALOR_EFEITO", "Valores para o efeito");
define("V6_DE_SEGURO", "de seguro");
define("V6_TOTAL_EXCLUINDO_MUD", "Total (Excluindo mudança)");
define("V6_TOTAL_EXCLUINDO_MUD_E", "Total (Excluindo mudança)");
define("V6_TOTAL_EXCLUINDO_MUD_IVA", "Total (Excluindo mudança) + IVA");
define("V6_TOTAL_EXCLUINDO_MUD_IVA_E", "Total (Excluindo mudança) + IVA");
//define("V6_FACTURA_MIN_HORA", "O serviço é facturado por hora, isto é, não são facturados períodos de duração inferior a uma hora.");
define("V6_FACTURA_MIN_HORA", "Faturado do início ao final da mudança acrescido de uma hora de deslocação (mínimo de faturação 2 horas).");
define("V6_FACTURA_NAO_MEIAS_HORAS", "Não se faturam períodos de meias horas.");
define("V6_FACTURA_MIN_HORA_2", "O período mínimo de faturação é de 1 hora, não se faturam 1/2 horas");
define("V6_ACRESCE_IVA", "Acresce o IVA à taxa em vigor");
define("V6_DESCONHECIDO", "Desconhecido / Directo");
define("V6_TELEMOVEL", "Telemóvel");
define("V6_EMAIL", "Email");
define("V6_CONTRIBUINTE", "N. contribuinte");


define("INQ_VIATURAS", "Viaturas");
define("INQ_ESTADO_CONSEVACAO", "Estado de conservação");
define("INQ_DISPONIBILIZADO", "Equipamento de carga e descarga disponibilizado pela cityMover");
define("INQ_IMAGEM_EXTERIOR", "Imagem exterior (apresentação)");
define("INQ_LIMPEZA", "Limpeza");
define("INQ_OPERADORES", "Operadores");
define("INQ_APRESENTACAO", "Apresentação");
define("INQ_PROFISSIONALISMO", "Profissionalismo");
define("INQ_CONHECIMENTOS_TECNICOS", "Conhecimentos técnicos");
define("INQ_CUMPRIMENTO_HORARIOS", "Cumprimento dos horários / datas acordadas");
define("INQ_ACTUACAO", "Actuação em situações imprevistas");
define("INQ_COMPRIMENTO", "Cumprimento das normas de segurança na execução do serviço");
define("INQ_SERVICO_COMERCIAL", "Serviço Comercial");
define("INQ_LOGISTICA_", "Logística / Entrega mobiliário novo");

define("INQ_ESCRITORIO", "Mudanças de escritório");
define("INQ_HABITACAO", "Mudanças de habitação");
define("INQ_LOGISTICA", "Logística");
define("INQ_ARMAZENAMENTO", "Armazenagem");
define("INQ_OUTRO", "Outro");

define("INQ_MAU", "Mau");
define("INQ_INSUFICIENTE", "Insuficiente");
define("INQ_SUFICIENTE", "Suficiente");
define("INQ_BOM", "Bom");
define("INQ_MUITO_BOM", "Muito Bom");


?>