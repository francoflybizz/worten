<?php

define('NOME_SITE', 'Mudanças, Armazenagem, Transitário, Logística - cityMover');

define('SOLUCOES_CRIATIVAS_DE_MUDANCAS', 'Soluções criativas de mudanças');
define('AGENDE_SUA_MUDANCA', 'Agende a sua mudança!<br />Orçamento grátis!');
define('A_SUA_MUDANCA', 'A sua mudança');
define('MUDANCAS_DENTRO_E_FORA_DE_PORTUGAL', 'Mudanças dentro e fora de Portugal');
define('HABITACAO', 'Habitação');
define('ESCRITORIO', 'Escritório');
define('LOCALIDADE_ORIGEM', 'Localidade de origem');
define('LOCALIDADE_DESTINO', 'Localidade de destino');
define('DATA_MUDANCA', 'Data da mudança');
define('AVANCAR', 'Avançar');

define('HOME_DESTAQUE_1_TITULO', 'Somos uma empresa de mudanças');
define('HOME_DESTAQUE_1_DESCR', 'Mudamos particulares e empresas, habitações e escritórios, data centers, artigos frágeis, obras de arte, e todo o tipo de bens.<br><br>A <span class="citymover">cityMover</span> é especializada em mudanças rápidas, profissionais, e amigas do ambiente. Peça-nos um orçamento online gratuito.');
define('HOME_DESTAQUE_2_TITULO', '...Transitário, e muito mais');
define('HOME_DESTAQUE_2_DESCR', 'Fornecemos um serviço completo: Armazenagem, Transitário, Gestão Documental, Tecnologia, Logística, e Mudanças Internacionais.<br><br>Somos também uma empresa solidária, colaborando com instituições como a BUS e a Raríssimas no apoio a causas sociais.');
define('HOME_DESTAQUE_3_TITULO', 'Seguros, alvarás e certificações');
define('HOME_DESTAQUE_3_DESCR', 'Além de todos os seguros legais para mudanças e transporte de bens, fornecemos seguros para os seus bens mais valiosos. Dispomos ainda de:');
define('HOME_DESTAQUE_3_LIST', serialize( array(
	'Alvará de Transportes Nacionais e Internacionais' => '#',
	'Alvará de Transitário' => '#',
	'Certificação ISO 9001:2008' => '#'
) ) );

define('MUDANCAS', 'Mudanças');
define('MUDANCAS_HOME_VALORES', serialize( array(
	'Mudanças de escritórios e industriais',
	'Mudanças de habitação',
	'Gestão da mudança'
) ) );

define('TRANSITARIO', 'Transitário');
define('TRANSITARIO_HOME_VALORES', serialize( array(
	'Via Marítima',
	'Via Aérea',
	'Via Terrestre (Europa)',
	'Contentores completos',
	'Serviços de grupagem, com enfoque nos PALOP'
) ) );

define('ARMAZENAGEM', 'Armazenagem');
define('ARMAZENAGEM_HOME_VALORES', serialize( array(
	'armazém de exportação',
	'Emissão de DME',
	'Armazenamento de mobiliário',
	'Boxes individuais com acesso exclusivo',
	'Localização e gestão por código de barras'
) ) );

define('GESTAO_DOCUMENTAL', 'Gestão <br>Documental');
define('GESTAO_DOCUMENTAL_HOME_VALORES', serialize( array(
	'Localização rápida da sua documentação',
	'Procura de documentos de forma imediata',
	'Indexação via código de barras',
	'Total confidencialidade',
	'Segurança e controlo de acessos'
) ) );

define('TECNOLOGIA', 'Tecnologia');
define('TECNOLOGIA_HOME_VALORES', serialize( array(
	'Mudança de servidores',
    'Mudança de computadores',
    'Ligação e instalação de equipamentos',
    'Gestão técnica do projecto',
    'Reciclagem de equipamentos informáticos'
) ) );

define('LOGISTICA', 'Logística');
define('LOGISTICA_HOME_VALORES', serialize( array(
	'Transporte desde a fábrica',
    'Descarga em obra',
    'Montagem',
    'Armazenagem em trânsito',
    'Entrega posterior em obra',
    'Recolha e reciclagem de resíduos',
    'Logística reversa'
) ) );

define('MUDANCAS_INTERNACIONAIS', 'Mudanças <br>Internacionais');
define('MUDANCAS_INTERNACIONAIS_HOME_VALORES', serialize( array(
	'Armazenagem',
    'Despacho alfandegário',
    'Seguro',
    'Transporte de viaturas',
    'Serviços de importação e exportação',
    'Liftvans de todos os tamanhos',
    'Orçamentos gratuitos'
) ) );

define('VER_SERVICO', 'Ver serviço');
define('PROCURA_SOLUCAO_A_MEDIDA', 'Procura uma solução à medida? Contate a nossa equipa comercial');
define('FALE_CONOSCO', 'Fale Connosco');

define('MUDANCAS_EM', 'Mudanças em');
define('MUDANCAS_NA', 'Mudanças na');
define('MUDANCAS_NO', 'Mudanças no');
define('PARCEIRO', 'Parceiro');
define('CERTIFICACAO', 'Certificação');
define('FOOTER_TEXT', 'Mudanças, Armazenagem, Tecnologia e Logística. Todos os direitos reservados.');

define('HOME_DESCONTO_1_TITULO', 'A cityMover efetua a logística do Querido Mudei a Casa');
define('HOME_DESCONTO_1_DESCR', 'Mais que um programa de TV, o \'Querido\' é hoje uma rede de franchising que faz obras em habitações e empresas.');
define('HOME_DESCONTO_2_TITULO', '10% desconto em mudanças para clientes Remax');
define('HOME_DESCONTO_2_DESCR', 'Se é cliente Remax e vai mudar de casa ou de escritório, ligue-nos para o <b><a href="tel:00351214789400">21 478 94 00</a></b>.');
define('HOME_DESCONTO_3_TITULO', '10% de desconto para sócios do Automóvel Clube de Portugal');
define('HOME_DESCONTO_3_DESCR', 'E 20% de desconto ao dia 20 de cada mês! Ligue-nos para o <b><a href="tel:00351214789400">21 478 94 00</a></b>.');
define('SABER_MAIS', 'Saber mais');
define('BLOG_OFICIAL', 'Blog oficial');

define('PASSO1', 'Passo 1');
define('PASSO1_TEXT', 'Os seus dados');

define('PASSO2', 'Passo 2');
define('PASSO2_TEXT', 'Mudança');

define('PASSO3', 'Passo 3');
define('PASSO3_TEXT', 'Fotos');

define('PASSO4', 'Passo 3');
define('PASSO4_TEXT', 'Seguro e Acessibilidade');

define('PASSO5', 'Passo 4');
define('PASSO5_TEXT', 'Confirmação');

define('PASSO6', 'Passo 5');
define('PASSO6_TEXT', 'Pagamento');

define('ORIGEM', 'Origem');
define('DESTINO', 'Destino');
define('CODIGO_POSTAL', 'Código Postal');
define('MORADA_ORIGEM', 'Morada de origem');
define('MORADA_DESTIDO', 'Morada de destino');
define('PREDIO_COM_ELEVADOR', 'Prédio com elevador?');
define('SIM', 'Sim');
define('NAO', 'Não');
define('DATA_MUDANCA', 'Data da mudança');
define('NOME_DO_CONTACTO', 'Nome do contato');
define('EMPRESA', 'Empresa');
define('NUMERO_DE_CONTRIBUINTE', 'Nº Contribuinte');
define('EMAIL', 'Email');
define('TELEFONE', 'Telefone');
define('TELEMOVEL', 'Telemóvel');
define('AVANCAR', 'Avançar');
define('PRECISA_DE_AJUDA', 'Precisa de ajuda?');
define('APOIO_AO_CLIENTE', 'Apoio ao cliente');
define('SEG_A_SEX', 'Seg a Sex');
define('INFORMACOES', 'Informações');
define('PAGINA_INICIAL', 'Página inicial');
define('CODIGO_VERIFICACAO', 'Código de verificação');


# Passo 1
define("LOCALIDADE_ORIGEM","Localidade de origem");
define("MORADA_ORIGEM","Morada de origem");
define("CODIGO_POSTAL","Código postal");
define("LOCALIDADE_DESTINO","Localidade de destino");
define("MORADA_DESTINO","Morada de destino");
define("DATA_MUDANCA","Data da mudança");
define("EMPRESA","Empresa");
define("NOME","Nome");
define("NOME_CONTACTO","Nome de contato");
define("TELEFONE","Telefone");
define("TELEMOVEL","Telemóvel");
define("TELEMOVEL_","Telemóvel");
define("NUMERO_CONTRIBUINTE","Nº Contribuinte");
define("PREDIO_ELEVADOR","Habitação com elevador");
define("PREDIO__ELEVADOR","Prédio com elevador");

define("LOCALIDADE_ORIGEM_ERRO","Preencha uma localidade de origem válida");
define("MORADA_ORIGEM_ERRO","Preencha a morada de origem");
define("CODIGO_POSTAL_ERRO","Preencha o código postal");
define("LOCALIDADE_DESTINO_ERRO","Preencha a localidade de destino");
define("MORADA_DESTINO_ERRO","Preencha a morada de destino");
define("DATA_MUDANCA_ERRO","A data deve ter pelo menos dez dias de antecedência");
define("EMPRESA_ERRO","Preencha o nome da empresa");
define("NOME_ERRO","Preencha o seu nome");
define("TELEFONE_ERRO","Preencha pelo menos um número de contato");
define("EMAIL_ERRO","Por favor verifique o seu email");
define("NUMERO_CONTRIBUINTE_ERRO","Preencha o nº contribuinte");
define("ELEVADOR_ORIGEM_ERRO","Por favor indique se há elevador na morada de origem");
define("ELEVADOR_DESTINO_ERRO","Por favor indique se há elevador na morada de destino");


# Passo 2
define("MENSAGEM_P2_4_HABITACAO","Caixas cityMover a adquirir");
define("MENSAGEM_P2_4_EMPRESA","Contentores cityMover a alugar");
define("CAIXAS_EMBALADAS","Caixas já embaladas");
define("NUMERO_CAIXAS","Número de caixas");
define("VOLUME","Volume");
define("IMAGEM","Imagem");
define("NOME","Nome");
define("PRECO","Preço");
define("QUANTIDADE","Quantidade");
define("LARGURA","Largura");
define("COMPRIMENTO","Comprimento");
define("ALTURA","Altura");
define("NOVO_ITEM","Novo item");


# Passo 3
define("OUTROS","Seguro e Acessos");
define("NECESSITA_SEGURO","Necessita de seguro?");
define("SIM","Sim");
define("NAO","Não");
define("CONFERIR_OBJETOS_SELECCIONADOS","Para conferir os objectos seleccionados, clique aqui");
define("ITENS_SEGURAR","Itens a segurar");
define("VALOR_ITEMS_EUROS","Valor dos itens em Euros");
define("VALOR_SEGURO_EUROS","Valor do seguro em Euros");
define("NECESSITA_SEGURO_ERRO","Por favor indique se necessita de seguro");
define("VALOR_ITEMS_ERRO","Por favor insira o valor dos bens a segurar");
define("LCA","Largura x Comprimento x Altura");
define("LCA_ABREVIADO","Larg. x Comp. x Alt.");
define("LCA_SIGLAS","L x C x A");
define("ITEMS_SEGURAR","Seleccione abaixo os itens que não cabem no elevador");
#define("ITEMS_SEGURAR","Selecione abaixo os itens que deseja segurar");
define("VALOR","Valor");
define("ELEVADOR","Elevador");
define("ELEVADOR_ERRO","Por favor indique se há itens que não cabem");
define("NAO_CABEM_ELEVADOR","Itens que não cabem no elevador");
define("NAO_CABEM_ELEVADOR_ERRO","Indique quais os items que não cabem no elevador");
define("ESTACIONAMENTO","Estacionamento");
define("ESTACIONAMENTO_OPCAO_1","Reservar estacionamento (40 Euros)");
define("ESTACIONAMENTO_OPCAO_2","Reservar agente da PSP para regulação de trânsito no estacionamento e parqueamento do camião da mudança (60 Euros)"); # Traduzir em pdf.php
define("VALOR_ESTACIONAMENTO_EUROS","Valor do estacionamento em Euros");


# Passo 4
define("DADOS_MUDANCA","Dados da mudança");
define("MUDANCA_HORARIO","(As mudanças têm lugar a partir das 08:30)");
define("CATEGORIAS","Categorias:");
define("ITEMS","Itens");
define("CAIXAS","Caixas");
define("COMENTARIOS","Comentários");
define("CAIXAS_ADICIONAIS","Deseja adquirir caixas adicionais?");
define("TERMOS_CONDICOES_1","Declaro que li e que aceito os ");
define("TERMOS_CONDICOES_2","termos e condições");
define("TERMOS_CONDICOES_ERRO","Por favor seleccione uma das opções");
define("CONFERIR_CAIXAS","Para conferir as caixas seleccionadas, clique aqui");
define("ITEMS_TRANSPORTAR","Itens a transportar");
define("SEM_CAIXAS_SELECIONADAS","Não há caixas seleccionadas");
define("SEM_CAIXAS_EMBALADAS","Não há caixas embaladas");
define("AGENTE_PSP","Agente da PSP");
define("DESCONTO","Desconto");
define("COMISSAO","Comissão");
define("V6_COMISSAO","Comissão");


# Passo 5
define("REFERENCIA","Referência");
define("ORCAMENTO","Orçamento");
define("MUDANCA_","Mudança");
define("MATERIAIS_DE_EMBALAGEM_","Materiais de embalagem");
define("SEGURO","Seguro");
define("VERSAO_IMPRIMIR","Versão para imprimir");
define("NAO_ORCAMENTO","Não aceito este orçamento - sair");
define("SIM_ORCAMENTO","Aceito este orçamento");
define("FORMA_PAGAMENTO","Escolha uma forma de pagamento:");
define("TRANSFERENCIA_BANCARIA","Transferência Bancária");
define("CHEQUE_NUMERARIO","Cheque ou numerário");
define("PARA","para");
define("CONFIRMAR","Confirmar");
define("FINALIZAR","Finalizar");
define("FECHAR_FORMULARIO","Fechar janela");
define("TOTAL_SINAL_RESERVA_ONLINE","Sinal para Reserva Online");

# Botões
define("PROXIMO_PASSO","Próximo passo");
define("PASSO_ANTERIOR","Passo anterior");

define("MAPADOSITE","Mapa do Site");
define("PAGINAS","Páginas");
define("PASSO","Passo");


/* .pdf_v5 */
define("V6_DIAS", "Dias");
define("V6_VALOR", "Valor");
define("V6_VALOR_EFEITO", "Valores para efeito");
define("V6_DE_SEGURO", "de seguro");
define("V6_TOTAL_EXCLUINDO_MUD", "Total (excluindo mudança)");
define("V6_TOTAL_EXCLUINDO_MUD_E", "Total (excluindo mudança)");
define("V6_TOTAL_EXCLUINDO_MUD_IVA", "Total (excluindo mudança) + IVA");
define("V6_TOTAL_EXCLUINDO_MUD_IVA_E", "Total (excluindo mudança) + IVA");
//define("V6_FACTURA_MIN_HORA", "O serviço é facturado por hora, isto é, não são facturados períodos de duração inferior a uma hora.");
define("V6_FACTURA_MIN_HORA", "Faturado do início ao fim da mudança acrescido de uma hora de deslocação (mínimo de faturação 2 horas).");
define("V6_FACTURA_NAO_MEIAS_HORAS", "Não se faturam períodos de meias horas.");
define("V6_FACTURA_MIN_HORA_2", "O período mínimo de faturação é de 1 hora, não se faturam 1/2 horas");
define("V6_ACRESCE_IVA", "Acresce IVA à taxa em vigor");
define("V6_DESCONHECIDO", "Desconhecido / Directo");
define("V6_TELEMOVEL", "Telemóvel");
define("V6_EMAIL", "Email");
define("V6_CONTRIBUINTE", "Nº contribuinte");

define("PERGUNTA_ERRO","Por favor escolha uma das alternativas abaixo");
define("COMENTARIO_ERRO","Por favor preencha a caixa de texto acima ou seleccione outra alternativa");
define("ENVIAR","   Enviar");
define("COMENTARIOS","Comentários");
define("CODIGO_VERIFICACAO","Código de verificação");
define("CAPCHA_ERRO_1","Ocorreu um erro ao gravar os dados, por favor tente novamente"); // Error while saving the message, please, try again
define("CAPCHA_ERRO_2","Código de verificação inválido");

define("DIAS","Dias");
define("POSSUI_CADASTRADO","Já é cliente registado?");
define("UTILIZADOR","Utilizador");
define("PALAVRA_PASSE","Palavra-passe");

define("LICENCAS_E_ESTACIONAMENTO", "Licenças e Estacionamento");
define("SERVICOS_DE_POLICIAMENTO", "Serviços de Policiamento");

?>
