<?php

/**
* Tools
*/
class Tools{
	
	public static $base_path;
	function __construct(){
	}

	function word_limiter($str, $limit = 100, $end_char = '&#8230;')
	{
		if (trim($str) == '')
		{
			return $str;
		}

		preg_match('/^\s*+(?:\S++\s*+){1,'.(int) $limit.'}/', $str, $matches);

		if (strlen($str) == strlen($matches[0]))
		{
			$end_char = '';
		}

		return rtrim($matches[0]).$end_char;
	}

	//esta funcao está apenas a fazer o simples upload, não faz resize.
	function upload_image_from_post( $config, $override_files_var = false ){

		//$config["destination_folder"] = "sliders/example";
		//$config["new_name"] = "sliders/example";
		//$config["post_var"] = "image";
		//$config["max_width"] = 960;
		//$config["max_height"] = 365;
		//$config["resize_mode"] = ; [reduce] -> reduz o tamanho ate caber no rectangulo. [exact] -> reduz e corta ate caber no rectangulo.
		if ( $override_files_var === FALSE ) {
			$file_var = $_FILES;
		}else{
			$file_var = $override_files_var;
		}


		$aux_foler = $config["destination_folder"].'/';
		$config["destination_folder"] = base_path("client_files/".$config["destination_folder"].'/');

		//Get extension
		$aux = explode(".", $file_var[$config["post_var"]]["name"]);
		// $ext = $aux[ ( count($aux)-1 ) ];
		// $ext = strtolower($ext);
		$ext = pathinfo($file_var[$config["post_var"]]["name"], PATHINFO_EXTENSION);

		if ( file_exists($config["destination_folder"].$config["new_name"].".".$ext ) ) {
			$i = 1;
			while (file_exists( $config["destination_folder"].$config["new_name"]."(".$i.").".$ext )) {
				$i++;
			}
			$config["new_name"] = $config["new_name"]."(".$i.")";

		}else{
			$file_name = $config["new_name"].".".$ext;
		}

		$copy = copy($file_var[$config["post_var"]]["tmp_name"], $config["destination_folder"].$config["new_name"].'.'.$ext);

		//falhou a criação da pasta
		if(!$copy){
			$create_dir = mkdir($config["destination_folder"], $mode = 0755);
		}

		if ( $copy || $create_dir) {
			$img = $aux_foler.$config["new_name"].'.'.$ext;
			switch ( $config["resize_mode"] ) {
				case 'exact':
					tools::make_thumb( base_path("client_files/".$img), $config["max_width"], $config["max_height"] );
					break;
				case 'reduce':
					tools::fit_rectangle( base_path("client_files/".$img), $config["max_width"], $config["max_height"] );
					break;
				
				default:
					die( "Erro em tools:: Tipo de redimensionamento ".$config["resize_mode"]." não definido." );
					break;
			}
			return $aux_foler.$config["new_name"].'.'.$ext;
		}else{
			return FALSE;
		}

	}


	function make_thumb($src, $desired_width, $desired_height) {
		$file_type  = exif_imagetype($src);

		$sizes = tools::get_display_size_thumb( $src, $desired_width, $desired_height );

		$dest = $src;

		switch ($file_type) {
			case 2:	//IMAGETYPE_JPEG
				$source_image = imagecreatefromjpeg($src);
				break;
			case 3:	//IMAGETYPE_PNG
				$source_image = imagecreatefrompng($src);
				break;
			default:
				$source_image = imagecreatefromjpeg($src);
				break;
		}


		$width = imagesx($source_image);
		$height = imagesy($source_image);

		$virtual_image = imagecreatetruecolor($desired_width, $desired_height);
		
		/* copy source image at a resized size */
		imagecopyresampled($virtual_image, $source_image, 0, 0, $sizes["x"], $sizes["y"], $desired_width, $desired_height, round($sizes["r"]*$desired_width), round($sizes["r"]*$desired_height) );
		
		switch ($file_type) {
			case 2:	//IMAGETYPE_JPEG
				imagejpeg($virtual_image, $dest, 100);
				break;
			case 3:	//IMAGETYPE_PNG
				/*
					bool imagepng ( resource $image [, string $filename [, int $quality [, int $filters ]]] )

					quality
					Compression level: from 0 (no compression) to 9.

				*/
				imagepng($virtual_image, $dest, 0, PNG_NO_FILTER);	
				break;
			default:
				imagejpeg($virtual_image, $dest, 100);
				break;
		}

	}

	function fit_rectangle($src, $desired_width, $desired_height) {
		$file_type  = exif_imagetype($src);

		$sizes = tools::get_display_size( $src, $desired_width, $desired_height );

		$dest = $src;

		/* read the source image */
		switch ($file_type) {
			case 2:	//IMAGETYPE_JPEG
				$source_image = imagecreatefromjpeg($src);
				break;
			case 3:	//IMAGETYPE_PNG
				$source_image = imagecreatefrompng($src);
				break;
			default:
				$source_image = imagecreatefromjpeg($src);
				break;
		}
		$width = imagesx($source_image);
		$height = imagesy($source_image);

		$virtual_image = imagecreatetruecolor($sizes["width"], $sizes["height"]);



		//echo '<hr />';
		//echo 'Dest Width:';
		//echo $width;
		//echo '<hr />';
		//echo 'Dest Heigh:';
		//echo $height;
		//echo '<hr />';
		//echo 'Src x:';
		//echo 0;
		//echo '<hr />';
		//echo 'Src y:';
		//echo 0;
		//echo '<hr />';
		//echo 'Des width:';
		//echo $sizes["width"];
		//echo '<hr />';
		//echo 'Des height:';
		//echo $sizes["height"];



		/* copy source image at a resized size */
		imagecopyresampled($virtual_image, $source_image, 0, 0, 0, 0, $sizes["width"], $sizes["height"], $width, $height );
		
		/* create the physical thumbnail image to its destination */
		switch ($file_type) {
			case 2:	//IMAGETYPE_JPEG
				imagejpeg($virtual_image, $dest, 100);
				break;
			case 3:	//IMAGETYPE_PNG
				/*
					bool imagepng ( resource $image [, string $filename [, int $quality [, int $filters ]]] )

					quality
					Compression level: from 0 (no compression) to 9.

				*/
				imagepng($virtual_image, $dest, 0);	
				break;
			default:
				imagejpeg($virtual_image, $dest, 100);
				break;
		}
		//die("");

	}

	public function get_display_size_thumb($src, $max_width, $max_height ) {
		
		$pro = getimagesize( $src );

		//var_dump($pro);
		


		$return["original_width"] = $img_width = $pro[0];
		$return["original_height"] =  $img_height = $pro[1];

		$pre_img_width = $max_width;
		$pre_img_height = $img_width / $max_width * $img_height;



		$r_w = $img_width / $max_width;
		$r_h = $img_height / $max_height;

		if ( $r_w > $r_h ) {
			$r = $r_h;
		}else{
			$r = $r_w;
		}

		$x = ($img_width - $r * $max_width)/2;
		$y = ($img_height - $r * $max_height)/2;

		$return["x"] = abs( round( $x ) );
		$return["y"] = abs( round( $y ) );
		$return["r"] = $r;

		//die("fasf");

		return $return;


	}

	function url_title( $str ){

			$str = strtolower($str);

			//transforma os c/ e s/
			$str = str_replace("c/", "com-", $str);
			$str = str_replace("s/", "sem-", $str);

		  $a = array('À', 'Á', 'Â', 'Ã', 'Ä', 'Å', 'Æ', 'Ç', 'È', 'É', 'Ê', 'Ë', 'Ì', 'Í', 'Î', 'Ï', 'Ð', 'Ñ', 'Ò', 'Ó', 'Ô', 'Õ', 'Ö', 'Ø', 'Ù', 'Ú', 'Û', 'Ü', 'Ý', 'ß', 'à', 'á', 'â', 'ã', 'ä', 'å', 'æ', 'ç', 'è', 'é', 'ê', 'ë', 'ì', 'í', 'î', 'ï', 'ñ', 'ò', 'ó', 'ô', 'õ', 'ö', 'ø', 'ù', 'ú', 'û', 'ü', 'ý', 'ÿ', 'A', 'a', 'A', 'a', 'A', 'a', 'C', 'c', 'C', 'c', 'C', 'c', 'C', 'c', 'D', 'd', 'Ð', 'd', 'E', 'e', 'E', 'e', 'E', 'e', 'E', 'e', 'E', 'e', 'G', 'g', 'G', 'g', 'G', 'g', 'G', 'g', 'H', 'h', 'H', 'h', 'I', 'i', 'I', 'i', 'I', 'i', 'I', 'i', 'I', 'i', '?', '?', 'J', 'j', 'K', 'k', 'L', 'l', 'L', 'l', 'L', 'l', '?', '?', 'L', 'l', 'N', 'n', 'N', 'n', 'N', 'n', '?', 'O', 'o', 'O', 'o', 'O', 'o', 'Œ', 'œ', 'R', 'r', 'R', 'r', 'R', 'r', 'S', 's', 'S', 's', 'S', 's', 'Š', 'š', 'T', 't', 'T', 't', 'T', 't', 'U', 'u', 'U', 'u', 'U', 'u', 'U', 'u', 'U', 'u', 'U', 'u', 'W', 'w', 'Y', 'y', 'Ÿ', 'Z', 'z', 'Z', 'z', 'Ž', 'ž', '?', 'ƒ', 'O', 'o', 'U', 'u', 'A', 'a', 'I', 'i', 'O', 'o', 'U', 'u', 'U', 'u', 'U', 'u', 'U', 'u', 'U', 'u', '?', '?', '?', '?', '?', '?'); 
		  $b = array('A', 'A', 'A', 'A', 'A', 'A', 'AE', 'C', 'E', 'E', 'E', 'E', 'I', 'I', 'I', 'I', 'D', 'N', 'O', 'O', 'O', 'O', 'O', 'O', 'U', 'U', 'U', 'U', 'Y', 's', 'a', 'a', 'a', 'a', 'a', 'a', 'ae', 'c', 'e', 'e', 'e', 'e', 'i', 'i', 'i', 'i', 'n', 'o', 'o', 'o', 'o', 'o', 'o', 'u', 'u', 'u', 'u', 'y', 'y', 'A', 'a', 'A', 'a', 'A', 'a', 'C', 'c', 'C', 'c', 'C', 'c', 'C', 'c', 'D', 'd', 'D', 'd', 'E', 'e', 'E', 'e', 'E', 'e', 'E', 'e', 'E', 'e', 'G', 'g', 'G', 'g', 'G', 'g', 'G', 'g', 'H', 'h', 'H', 'h', 'I', 'i', 'I', 'i', 'I', 'i', 'I', 'i', 'I', 'i', 'IJ', 'ij', 'J', 'j', 'K', 'k', 'L', 'l', 'L', 'l', 'L', 'l', 'L', 'l', 'l', 'l', 'N', 'n', 'N', 'n', 'N', 'n', 'n', 'O', 'o', 'O', 'o', 'O', 'o', 'OE', 'oe', 'R', 'r', 'R', 'r', 'R', 'r', 'S', 's', 'S', 's', 'S', 's', 'S', 's', 'T', 't', 'T', 't', 'T', 't', 'U', 'u', 'U', 'u', 'U', 'u', 'U', 'u', 'U', 'u', 'U', 'u', 'W', 'w', 'Y', 'y', 'Y', 'Z', 'z', 'Z', 'z', 'Z', 'z', 's', 'f', 'O', 'o', 'U', 'u', 'A', 'a', 'I', 'i', 'O', 'o', 'U', 'u', 'U', 'u', 'U', 'u', 'U', 'u', 'U', 'u', 'A', 'a', 'AE', 'ae', 'O', 'o'); 
		  $str = str_replace($a, $b, $str);

		  return strtolower(preg_replace(array('/[^a-zA-Z0-9 -]/', '/[ -]+/', '/^-|-$/'),  array('', '-', ''),  $str ));;

	}

	function time_ago ($oldTime, $newTime = "") {
		if(empty($newTime))
			$newTime = date("Y-m-d H:m:s");

		$timeCalc = strtotime($newTime) - strtotime($oldTime);
		if ($timeCalc > (60*60*24)) {$timeCalc = round($timeCalc/60/60/24) . " dia(s) atr&aacute;s";}
		else if ($timeCalc > (60*60)) {$timeCalc = round($timeCalc/60/60) . " hora(s) atr&aacute;s";}
		else if ($timeCalc > 60) {$timeCalc = round($timeCalc/60) . " minuto(s) atr&aacute;s";}
		else if ($timeCalc > 0) {$timeCalc .= " seconds ago";}
		return $timeCalc;
	}

	function is_email($email){
		if (!filter_var($email, FILTER_VALIDATE_EMAIL) === false) {
		  return true;
		} else {
		  return false;
		}
	}

	function save_email_to_file($email){
		if(!isset($email))
			return false;

		$handle = fopen(base_path("emails.txt"), "a+");
		$write = fwrite($handle, $email . "\n");
		$close = fclose($handle);
		return $close;
	}

	//determinar se o url deve ser para lançar para fora do site, ou utilizar o caminho base do site
	function in_out_url($url){
		$count = 0;
		str_replace("http://", "", $url, $count);
		$out_url = ( $count > 0 ) ?  $url: base_url(Core::lang() . "/" . $url);
		return $out_url;
	}


	//Esta função recebe a imagem, o tamanho máximo e devolve o a width e a height para mostrar
	public function get_display_size($src, $max_width, $max_height ) {
		
		//$src = str_replace( base_url(), '', $src);
		$src = base_path($src);
		$pro = getimagesize( $src );

		$return["original_width"] = $img_width = $pro[0];
		$return["original_height"] =  $img_height = $pro[1];

		if( $img_width > $max_width ) {
			$final_height = ( $max_width * $img_height ) / $img_width;
			$final_width = $max_width;
		}else{
			$final_height = $img_height;
			$final_width = $img_width;
		}

		if( $final_height > $max_height ) {
			$final_width = ( $img_width * $max_height ) / $img_height;
			$final_height = $max_height;
		}

		$return['width'] = $final_width;
		$return['margin_left'] = ($max_width - $return['width'])/2;
		if( $return['margin_left'] < 0 ){
			$return['margin_left'] = 0;
		}else{
			$return['margin_left'] = round($return['margin_left']);
		}

		$return['height'] = $final_height;
		$return['margin_top'] = ( $max_height - $return['height'] ) / 2;
		if( $return['margin_top'] < 0 ){
			$return['margin_top'] = 0;
		}else{
			$return['margin_top'] = round($return['margin_top']);
		}

		$return["width"] = round( $return["width"] );
		$return["height"] = round( $return["height"] );
		$return["margin_top"] = round( $return["margin_top"] );
		
		return $return;

	}

	public function get_setting( $key ) {
		$query = "select * from settings where call_word = '".$key."' and lang = '".core::lang()."'";
		$res = mysql_query($query) or die_sql( $query );
		$row = mysql_fetch_array($res);
		return $row["value"];
	}
	
	//adiciona mensagens a apresentar ao user em session
	public function notify_add($message, $type = "info"){

		$_SESSION["notify"]["messages"][] = array("message" => $message, "type" => $type, "already_displayed" => "false");
	}

	//lista as mensagens a apresentar ao user para serem usadas via script
	public function notify_list(){

		if(!empty($_SESSION["notify"]["messages"])){


			echo "<ul id=\"notify-messages\">";
			foreach ($_SESSION["notify"]["messages"] as $message) {
				echo "<li data-type=\"".$message["type"]."\">" . $message["message"] . "</li>";
			}
			echo "</ul>";
		}

		//limpar o queue de notifications
		self::notify_empty();

	}

	public function notify_empty(){
		$_SESSION["notify"]["messages"] = array();
	}
	

	function Title($mod,$id) {
		$sql = "SELECT `key` chave,`value` FROM meta WHERE `key` IN ('index_title','index_keywords','index_description') AND lang = '{$_SESSION['lang']}'";
		$query = mysql_query($sql);

		for ($i=0;$i<3;$i++) {
			$dados = mysql_fetch_object($query);

			if ($dados->chave == "index_title") $title = $dados->value;
			elseif ($dados->chave == "index_keywords") $keywords = $dados->value;
			elseif ($dados->chave == "index_description") $description = $dados->value;
		}

		if ($mod=="freepage") {
			$dados = mysql_fetch_object(mysql_query("SELECT html_title, html_description, html_keywords FROM pages WHERE id = {$id}"));
			$title = $dados->html_title.' - '.$title;
			$description = $dados->html_description;
			$keywords = $dados->html_keywords;
		}
		elseif ($mod=="research") {
			$title = APOIO_CLIENTE.' - '.$title;
		}
		elseif ($mod=="sitemap") { 
			$title = MAPADOSITE.' - '.$title;
		}

		$keywords = $keywords==""?"cityMover, mudanças":$keywords;	
		$description = $description==""?"cityMover.":$description;	

		$dados = array("title"=>$title,"description"=>$description,"keywords"=>$keywords);

		return $dados;
	}

	function pageInfo() {
		if (isset($_SERVER['HTTP_REFERER'])) $referer = $_SERVER['HTTP_REFERER'];
		else $referer = "Desconhecido / Directo";

		$content = "
			<div style='margin:15px auto 25px auto; width:700px;'>
				<table width='100%' cellpadding='3' cellspacing='0' style='font-size:11px; font-color:#525252; border: 1px solid #f1f1f1;'>
					<tr>
						<td style='background:#fff;'>REFERER: ".$referer."</td>
					</tr>
					<tr>
						<td style='background:#fff;'>IP Adress: ".getenv("REMOTE_ADDR")."
					</tr>
				</table>
			</div>";

		return $content;
	}

	function filtro($string) {
		if (preg_match( "/bcc:|cc:|multipart|\[url|Content-Type:/i", implode($string))) return true;
		if (preg_match_all("/<a|http:/i", implode($string), $out) > 3) return true;

		$spamwords = "/(http|href|4u|adipex|advicer|baccarrat|blackjack|bllogspot|booker|byob|car-rental-e-site|car-rentals-e-site|carisoprodol|casino|casinos|chatroom|cialis|coolcoolhu|coolhu|credit-card-debt|credit-report-4u|cwas|cyclen|cyclobenzaprine|dating-e-site|day-trading|debt-consolidation|debt-consolidation-consultant|discreetordering|duty-free|dutyfree|equityloans|fioricet|flowers-leading-site|freenet-shopping|freenet|gambling-|hair-loss|health-insurancedeals-4u|homeequityloans|homefinance|holdem|holdempoker|holdemsoftware|holdemtexasturbowilson|hotel-dealse-site|hotele-site|hotelse-site|incest|insurance-quotesdeals-4u|insurancedeals-4u|jrcreations|levitra|macinstruct|mortgage-4-u|mortgagequotes|online-gambling|onlinegambling-4u|ottawavalleyag|ownsthis|palm-texas-holdem-game|paxil|penis|pharmacy|phentermine|poker-chip|poze|pussy|rental-car-e-site|ringtones|roulette|shemale|shoes|slot-machine|texas-holdem|thorcarlson|top-site|top-e-site|tramadol|trim-spa|ultram|valeofglamorganconservatives|viagra|vioxx|xanax|zolus)/i";  
		if (preg_match($spamwords, implode($string))) return true;
		//if (strpos($string,"[") >= 0 || strpos($string,"]") >= 0) return true;

		return false;
	}

	# Função Anti SQL Injection
	# http://www.maisumblog.com/2009/02/06/como-evitar-ataques-de-sql-injection-no-php-e-mysql/
	# Leonardo Cesar Teixeira 
	static function ASI($str) { 
	  if (!is_numeric($str)) {
		  $str= get_magic_quotes_gpc() ? stripslashes($str) : $str;
		  $str= function_exists("mysql_real_escape_string") ? mysql_real_escape_string($str) : mysql_escape_string($str);
	  }

	  return $str;
	}
	

	function geraCodigo() {
		$CaracteresAceitos = 'abcdxywzABCDZYWZ0123456789';
		$max = strlen($CaracteresAceitos)-1;
		$codigo = null;

		for($i=0; $i < 25; $i++) {
			$codigo .= $CaracteresAceitos{mt_rand(0, $max)};
		}
		return $codigo;
	}

	function items() {

		$sql = "(SELECT o.id_items id, c.name_pt categoria, i.name_pt item, o.total
				FROM orders_items o LEFT JOIN items i
				  ON i.id = o.id_items LEFT JOIN categories c
				  ON c.id = i.cat
				WHERE id_orders = {$_SESSION['id_order']}
				  AND c.name_pt IS NOT NULL
				ORDER BY categoria, item, o.total)
				UNION
				(SELECT CONCAT('o',id) id, 'Outros' categoria, outros item, total
				FROM orders_items
				WHERE id_orders = {$_SESSION['id_order']}
				  AND outros <> '')";
		$query = mysql_query($sql);
		$n_items = mysql_num_rows($query);

		$body = "
				<h4 class='insurance-table-title'>".ITEMS." a segurar</h4>
			
				<table class='insurance-table table' width='100%' cellpadding='3' cellspacing='0'>";

				$cor = "#fff";
				$header = false;
				for ($i=0;$i<$n_items;$i++) {
					$r = mysql_fetch_object($query);

					# SESSION ÍTENS
					$valor = '';
					if (is_array($_SESSION['items_insurance_value'])) {
						foreach ($_SESSION['items_insurance_value'] as $item) {
							if(!preg_match('[a-zA-Z0-9]', $item['id'])) {
								if ($r->id==$item['id']) {
									$valor = $item['value'];
									break;
								}
							}
							if (strpos($item['id'],"-") >= 0) { // Para outros ítens
								if (tools::makeFriendlyUrl($r->item,'variaveis') == end(explode('-', $item['id']))) {
									$valor = $item['value'];
									break;
								}
							}

						}
					}
					else $valor = '';
					
					if ($r->categoria != $categoria_ant) {
						$body .= "
							<tr>
								<th>".$r->categoria."</th>";

						if (!$header) {
							$body .= "
									<th class='text-center' align='center'>".QUANTIDADE."</th>
									<th align='right' class='text-right'>".VALOR." &euro;&nbsp;&nbsp;&nbsp;&nbsp;</th>";
							$header = true;
						}
						else {
							$body .= "
									<td>&nbsp;</td><td>&nbsp;</td>";
						}
					}

					$body .= "
						</tr>
						<tr style='background:{$cor};'>
							<td>
								<span id='ItemLink".$r->id."'>".stripslashes($r->item)."</span>
							</td>
							<td align='center'>".$r->total."</td>";
								
							$id_ = substr($r->id,0,1)=="o"?"outros_".$r->id."-".tools::makeFriendlyUrl($r->item,'variaveis'):"items_value_".$r->id;

					$body .= "
							<td align='right'><input class='form-control' type='number' name='".$id_."' id='".$id_."' size='5' maxlength='7' value='{$valor}' onBlur=\"TotalInsurance();\">
							</td>
						</tr>";

					if ($cor=="#fff") $cor = "#f1f1f1";
					else $cor = "#fff";
					$categoria_ant = $r->categoria;
				}

			$body .= "
				</table>
		";

		return $body;
	}


	function boxes() {

		$lang = core::lang();
		$sql = "SELECT name_{$lang} name, o.total, b.id id
				FROM orders_boxes o LEFT JOIN boxes b ON b.id = o.id_boxes
				WHERE id_orders = {$_SESSION['id_order']}
				ORDER BY name_pt, o.total";
		
		$query = mysql_query($sql);
		$n_boxes = mysql_num_rows($query);

	
		if ($n_boxes > 0) {
			$body = "			
					<h4 class='insurance-table-title'>".CAIXAS." a segurar</h4>
					<table class='insurance-table table' width='100%' cellpadding='3' cellspacing='0'>
						<tr>
							<th>&nbsp;</th>
							<th class='text-center' align='center'>".QUANTIDADE."</th>
							<th align='right' class='text-right'>".VALOR." &euro;</th>
						</tr>";
						$cor = "#fff";
						for ($i=0;$i<$n_boxes;$i++) {
							$r = mysql_fetch_object($query);

							# SESSION BOXES
							$valor = '';
							if (is_array($_SESSION['boxes_insurance_value'])) {
								foreach ($_SESSION['boxes_insurance_value'] as $item) {
									if ($r->id==$item['id']) {
										$valor = $item['value'];
										break;
									}
								}
							}
							else $valor = '';

							$body .= "
								<tr style='background:{$cor};'>
									<td>".stripslashes($r->name)."</td>
									<td align='center'>{$r->total}</td>
									<td align='right'><input class='form-control' type='number' name='boxes_value_".$r->id."' id='boxes_value_".$r->id."' size='5' maxlength='7' value='{$valor}' onBlur=\"TotalInsurance();\"></td>
								</tr>";
							
							if ($cor=="#fff") $cor = "#f1f1f1";
							else $cor = "#fff";

						}					

				$body .= "
					</table>
			";
		}


		$sql = "SELECT * FROM orders_packed
				WHERE id_orders = {$_SESSION['id_order']}
				ORDER BY nome, total";

		$query = mysql_query($sql);
		$n_packed = mysql_num_rows($query);


		if ($n_packed > 0) {
			$body .= "
				<h4 class='insurance-table-title'>".CAIXAS_EMBALADAS." a segurar</h4>
				<div>
					<table class='insurance-table' width='100%' cellpadding='3' cellspacing='0'>
						<tr>
							<td>&nbsp;</td>
							<td align='center'>".QUANTIDADE."</td>
							<td align='right'>".VALOR." &euro;&nbsp;&nbsp;&nbsp;&nbsp;</td>
						</tr>";

						$cor = "#fff";
						for ($i=0;$i<$n_packed;$i++) {
							$s = mysql_fetch_object($query);

							# SESSION PACKED
							$valor = '';
							if (is_array($_SESSION['packed_insurance_value'])) {
								foreach ($_SESSION['packed_insurance_value'] as $item) {
									if (tools::makeFriendlyUrl($s->nome,'variaveis')==$item['nome']) {
										$valor = $item['value'];
										break;
									}
								}
							}
							else $valor = '';
							
							$body .= "
								<tr style='background:{$cor};'>
									<td>".stripslashes($s->nome)." ".$s->largura."x".$s->comprimento."x".$s->largura."</td>
									<td align='center'>{$s->total}</td>
									<td align='right'>";

							$body .= '<input type="hidden" name="packed_original_'.tools::makeFriendlyUrl($s->nome,"variaveis").'_value" id="packed_original_'.tools::makeFriendlyUrl($s->nome,"variaveis").'_value" value="'.stripslashes($s->nome).'">';

							$body .= "
										<input class='form-control' type='number' name='packed_value_".tools::makeFriendlyUrl($s->nome,'variaveis')."' id='packed_value_".tools::makeFriendlyUrl($s->nome,'variaveis')."' size='5' maxlength='7' value='{$valor}' onBlur=\"TotalInsurance();\">
									</td>
								</tr>";

							if ($cor=="#fff") $cor = "#f1f1f1";
							else $cor = "#fff";
						}
				$body .= "
					</table>
				</div>
				<br />";	
		}

		return $body;
	}

	function Message($id) {

		$lang = !empty($_SESSION["lang_fe"]) ? $_SESSION["lang_fe"] : $_SESSION["lang"];
		$lang = empty($lang) ? "pt" : $lang; //make sure something is set

		$sql = "SELECT text_{$lang} message FROM messages WHERE id = {$id}";
		
		$query = mysql_query($sql);
		$dados = mysql_fetch_array($query, MYSQL_ASSOC);
		
		return $dados['message'];
		
	}

	function uploadFiles($dir,$sizeMb, $ficheiro, $extensoes="", $novoNome="") {
		// Standard: $_FILES['ficheiro']

		// Pasta onde o ficheiro vai ser salvo
		$pasta = $dir;
		 
		// Tamanho máximo do ficheiro (em Bytes)
		$tamanho = 1024 * 1024 * $sizeMb; // se 2, então 2Mb
		 
		// Array com as extensões permitidas
		$extensoes = $extensoes == ''?array('jpeg', 'jpg', 'gif', 'png', 'gif', 'doc', 'docx', 'xls', 'xlsx', 'ppt', 'pptx', 'pdf', 'zip', 'rar', 'swf'):array($extensoes);

		// Renomeia o ficheiro? (Se true, o ficheiro será salvo em um nome único)
		$_renomeia = false;
		  
		// Verifica se houve algum erro com o upload. Se sim, exibe a mensagem do erro
		if ($ficheiro['error'] != 0) {
		die("N&atilde;o foi poss&iacute;vel fazer o upload, erro:<br />" . $_UP['erros'][$_FILES['ficheiro']['error']]);
		exit; // Para a execução do script
		}
		 
		// Caso script chegue a esse ponto, não houve erro com o upload e o PHP pode continuar
		 
		// Faz a verificação da extensão do ficheiro
		$extensao = strtolower(end(explode('.', $ficheiro['name'])));

		$OK = false;
		foreach ($extensoes as $key=>$value) {
			if ($value == $extensao) $OK = true;
		}

		if ($OK == false) {
			if (count($extensoes) == 1) $msg = "Por favor, envie ficheiros somente com a exten&ccedil;&atilde;o <b>{$extensoes[0]}</b>";
			else $msg = "Por favor, envie ficheiros com uma das seguintes extens&otilde;es: jpeg, jpg, gif, png, gif, doc, docx, xls, xlsx, ppt, pptx, pdf, zip, rar, swf";
		}
		
		elseif ($tamanho < $ficheiro['size']) {
			$msg = "Envie ficheiros de at&eacute; ".$siteMb;
		} 
		else {
			if ($novoNome != "") $uploadfile = $dir.$novoNome.".".strtolower(end(explode('.', $ficheiro['name'])));
			else $uploadfile = $dir.$ficheiro['name'];

			if (move_uploaded_file($ficheiro['tmp_name'], $uploadfile)) $msg = "OK";
		}

	return $msg;
	}


	function TamanhoArquivo($cFile)	{
		if ( file($cFile) ){
			$nSize = filesize($cFile);
			if ($nSize<1024) { return strval($nSize).' bytes'; }
			if ($nSize<pow(1024,2)) { return number_format($nSize/1024,2).' KB'; }
			if ($nSize<pow(1024,3)) { return number_format($nSize/pow(1024,2),2).' MB'; }
		}
		else return 0;
	}

	function shortText($text,$size) {
		if (strlen($text) > $size) {
			for ($a=$size; $a>=1; $a--) {
				if(substr($text,$a,1)==" ") break;
			}
			$text = substr($text,0,$a).' <b>(...)</b>';	
		}

		return stripslashes($text);
	}

	function enviaMail() {
		$sql = "SELECT * FROM orders WHERE id = {$_SESSION['id_order']}";
		$query = mysql_query($sql);
		$dados = mysql_fetch_array($query, MYSQL_ASSOC);

		$level = $dados['level']=='office'?'Empresa':'Habitação';
		$insurance = $dados['insurance']==1?'Sim':'N&atilde;o';

		$body = "<html>
					<body link='#000000' style='margin:0; background:#fff; color:#777777; font-family:Arial, Helvetica, sans-serif; font-size:12px; font-stretch:normal; font-style:normal; font-variant:normal; font-weight:normal; padding:0;'>
								
					<div style='margin:15px auto 25px auto; width:700px;'>
						<table width='100%' cellpadding='5' cellspacing='0'>
							<tr>
								<td style='background:#A3D96A;'><a href='http://www.citymover.pt'><img src='http://www.citymover.pt/images/Logotipo.jpg' border='0' alt='Citymover'></a></td>
							</tr>
						</table>

						<div style='background:#66BD29; margin:0px 0 0 0; padding:5px; color:#fff; font-weight:bold;'>Dados Pessoais - {$level}</div>
						<div style='border:1px solid #f1f1f1; padding:5px;'>
							<table width='100%' cellpadding='3' cellspacing='0' style='font-size:12px;'>
							<tr>
								<td>Nome</td>
								<td>".stripslashes($dados['name'])."</td>
							</tr>";
							
							if ($dados['company']!="") {
								$body .= "
									<tr>
										<td>Empresa</td>
										<td>".stripslashes($dados['company'])."</td>
									</tr>";						
							}

							$body .= "						
							<tr>
								<td>Telefone</td>
								<td>{$dados['phone']}</td>
							</tr>
							<tr>
								<td>Telem&oacute;vel</td>
								<td>{$dados['mobile']}</td>
							</tr>
							<tr>
								<td>Email</td>
								<td>{$dados['email']}</td>
							</tr>
							<tr>
								<td>N. Contribuinte</td>
								<td>{$dados['taxnumber']}</td>
							</tr>
							</table>
						</div>

						<div style='background:#66BD29; margin:10px 0 0 0; padding:5px; color:#fff; font-weight:bold;'>Dados da mudan&ccedil;a</div>
						<div style='border:1px solid #f1f1f1; padding:5px;'>
							<table width='100%' cellpadding='3' cellspacing='0' style='font-size:12px;'>
							<tr>
								<td>Localidade de origem</td>
								<td>".stripslashes($dados['dep_city'])."</td>
							</tr>
							<tr>
								<td>Morada de origem</td>
								<td>".stripslashes($dados['address'])."</td>
							</tr>
							<tr>
								<td>C&oacute;digo Postal</td>
								<td>{$dados['dep_zip']}</td>
							</tr>
							<tr>
								<td>Localidade de destino</td>
								<td>".stripslashes($dados['dest_city'])."</td>
							</tr>
							<tr>
								<td>Morada de destino</td>
								<td>".stripslashes($dados['address_dest'])."</td>
							</tr>
							<tr>
								<td>C&oacute;digo Postal</td>
								<td>{$dados['dest_zip']}</td>
							</tr>
							<tr>
								<td>Data da mudan&ccedil;a</td>
								<td>{$_SESSION['data_entrega']}</td>
							</tr>
							</table>
						</div>

						<div style='background:#66BD29; margin:10px 0 0 0; padding:5px; color:#fff; font-weight:bold;'>&Iacute;tems</div>
						<div style='border:1px solid #f1f1f1; padding:0px;'>
						<table width='100%' cellpadding='3' cellspacing='0' style='font-size:12px;'>";

						$sql = "(SELECT c.name_pt categoria, i.name_pt item, o.total
								FROM orders_items o LEFT JOIN items i
								  ON i.id = o.id_items LEFT JOIN categories c
								  ON c.id = i.cat
								WHERE id_orders = {$_SESSION['id_order']}
								  AND c.name_pt IS NOT NULL
								ORDER BY categoria, item, o.total)
								UNION
								(SELECT 'Outros' categoria, outros item, total
								FROM orders_items
								WHERE id_orders = {$_SESSION['id_order']}
								  AND outros <> '')";
						$query = mysql_query($sql);
						$n_items = mysql_num_rows($query);
						
						$cor = "#fff";
						for ($i=0;$i<$n_items;$i++) {
							$r = mysql_fetch_object($query);

							if ($r->categoria != $categoria_ant) {
								$body .= "
									<tr style='background:#A3D96A; color:#333;'>
										<td>".$r->categoria."</td>
										<td align='right'>".QUANTIDADE."</td>
									</tr>";
							}

							$body .= "
								<tr style='background:{$cor};'>
									<td>".stripslashes($r->item)."</td>
									<td align='right'>{$r->total}</td>
								</tr>";
							
							//$cor_ant = $cor;
							if ($cor=="#fff") $cor = "#f1f1f1";
							else $cor = "#fff";
							$categoria_ant = $r->categoria;

						}
						$body .= "
							</table>
						</div>

						<div style='background:#66BD29; margin:10px 0 0 0; padding:5px; color:#fff; font-weight:bold;'>Caixas</div>
						<div style='border:1px solid #f1f1f1; padding:0px;'>
						<table width='100%' cellpadding='3' cellspacing='0' style='font-size:12px;'>
						<tr style='background:#A3D96A; color:#333;'>
							<td>Nome</td>
							<td align='right'>".QUANTIDADE."</td>
						</tr>";

						$sql = "SELECT name_pt, o.total
								FROM orders_boxes o LEFT JOIN boxes b ON b.id = o.id_boxes
								WHERE id_orders = {$_SESSION['id_order']}
								ORDER BY name_pt, o.total";
						$query = mysql_query($sql);
						$n_items = mysql_num_rows($query);

						if ($n_items>0) {
							for ($i=0;$i<$n_items;$i++) {
								$r = mysql_fetch_object($query);
		
								$body .= "
									<tr style='background:#fff;'>
										<td>".stripslashes($r->name_pt)."</td>
										<td align='right'>{$r->total}</td>
									</tr>";
							}					
						}
						else $body .= "				
									<tr>
										<td colspan='2'><i>".SEM_CAIXAS_SELECIONADAS."</i></td>
									</tr>";

					$body .= "
							</table>
						</div>

						<div style='background:#66BD29; margin:10px 0 0 0; padding:5px; color:#fff; font-weight:bold;'>Caixas j&aacute; embaladas</div>
						<div style='border:1px solid #f1f1f1; padding:0px;'>
						<table width='100%' cellpadding='3' cellspacing='0' style='font-size:12px;'>
						<tr style='background:#A3D96A; color:#333;'>
							<td>".NOME."</td>
							<td>".LCA."</td>
							<td align='right'>".QUANTIDADE."</td>
						</tr>";

						$sql = "SELECT * FROM orders_packed
								WHERE id_orders = {$_SESSION['id_order']}
								ORDER BY nome, total";
						$query = mysql_query($sql);
						$n_items = mysql_num_rows($query);

						if ($n_items>0) {
							for ($i=0;$i<$n_items;$i++) {
								$r = mysql_fetch_object($query);

								$body .= "
									<tr style='background:#fff;'>
										<td>".stripslashes($r->nome)."</td>
										<td>{$r->largura} x {$r->comprimento} x {$r->altura}</td>
										<td align='right'>{$r->total}</td>
									</tr>";
							}					
						}
						else $body .= "
									<tr>
										<td colspan='2'><i>".SEM_CAIXAS_EMBALADAS."</i></td>
									</tr>";

					$body .= "
							</table>
						</div>

						<div style='background:#66BD29; margin:10px 0 0 0; padding:5px; color:#fff; font-weight:bold;'>Seguro</div>
						<div style='border:1px solid #f1f1f1; padding:0px;'>
						<table width='100%' cellpadding='3' cellspacing='0' style='font-size:12px;'>
						<tr style='background:#A3D96A; color:#333;'>
							<td>Necessita de seguro?</td>
							<td>{$insurance}</td>
						</tr>
						";

						if ($dados['insurance'] == 1) {
							$body .= "
							<tr>
								<td class='txt' valign='top' style='background:#F8F8F8;'><strong>A segurar</strong></td>
								<td style='background:#F8F8F8;'>";

									# PASSO 4: SEGUROS: ÍTENS
									$sql = "(SELECT i.id id, c.name_pt categoria, i.name_pt item, o.total, o.`value`
											FROM orders_items o LEFT JOIN items i
											  ON i.id = o.id_items LEFT JOIN categories c
											  ON c.id = i.cat
											WHERE id_orders = {$_SESSION['id_order']}
											  AND c.name_pt IS NOT NULL
											  AND o.`value` IS NOT NULL
											ORDER BY categoria, item, o.total)
											UNION
											(SELECT CONCAT('o',id) id, 'Outros' categoria, outros item, total, `value`
											FROM orders_items
											WHERE id_orders = {$_SESSION['id_order']}
											  AND outros <> ''
											  AND `value` IS NOT NULL
											  )";
									$query = mysql_query($sql);
									$n_items = mysql_num_rows($query);

									if ($n_items > 0) {
										
										$body .= "
											<table width='100%' cellpadding='5' cellspacing='0'>
											<div style='background:#525252; margin:10px 0 0 0; padding:5px; color:#fff; font-weight:bold;'>&Iacute;tens a segurar</div>
											<div style='border:1px solid #f1f1f1; padding:0px;'>
												<table width='100%' cellpadding='3' cellspacing='0' style='font-size:12px;'>";

												$cor = "#fff";
												$header = false;
												$categoria_ant = "";
												for ($j=0;$j<$n_items;$j++) {
													$t = mysql_fetch_object($query);

													if ($t->categoria != $categoria_ant) {
														$body .= "
															<tr style='background:#DADADA; color:#333;'>
																<td>".$t->categoria."</td>";

														if (!$header) {
															$body .= "
																	<td align='center'>".QUANTIDADE."</td>
																	<td align='right'>".VALOR." &euro;</td>";
															$header = true;
														}
														else {
															$body .= "
																	<td>&nbsp;</td><td>&nbsp;</td>";
														}
													}

													$body .= "
														</tr>
														<tr style='background:{$cor};'>
															<td>".stripslashes($t->item)."</td>
															<td align='center'>".$t->total."</td>
															<td align='right'>".number_format($t->value,2,',','.')."</td>
														</tr>";

													if ($cor=="#fff") $cor = "#f1f1f1";
													else $cor = "#fff";
													$categoria_ant = $t->categoria;
												}

										$body .= "</table>";
									}

									# PASSO 4: SEGUROS: CAIXAS
									$sql = "SELECT name_{$_SESSION['lang']} name, o.total, b.id id, o.`value`
											FROM orders_boxes o LEFT JOIN boxes b ON b.id = o.id_boxes
											WHERE id_orders = {$_SESSION['id_order']}
												AND `value` IS NOT NULL
											ORDER BY name_pt, o.total";
									$query = mysql_query($sql);
									$n_items = mysql_num_rows($query);

									if ($n_items > 0) {
										
										$body .= "
											<table width='100%' cellpadding='5' cellspacing='0'>
											<div style='background:#525252; margin:10px 0 0 0; padding:5px; color:#fff; font-weight:bold;'>Caixas a segurar</div>
											<div style='border:1px solid #f1f1f1; padding:0px;'>
												<table width='100%' cellpadding='3' cellspacing='0' style='font-size:12px;'>
													<tr style='background:#DADADA; color:#333;'>
														<td>&nbsp;</td>
														<td align='center'>".QUANTIDADE."</td>
														<td align='right'>".VALOR." &euro;</td>
													</tr>";

												$cor = "#fff";
												for ($j=0;$j<$n_items;$j++) {
													$t = mysql_fetch_object($query);

													$body .= "
														<tr style='background:{$cor};'>
															<td>".$t->name."</td>
															<td align='center'>{$r->total}</td>
															<td align='right'>".number_format($r->value,2,',','.')."</td>
														</tr>";
													
													if ($cor=="#fff") $cor = "#f1f1f1";
													else $cor = "#fff";

												}					

										$body .= "</table>";
									}

									# PASSO 4: SEGUROS: CAIXAS EMBALADAS
									$sql = "SELECT * FROM orders_packed
											WHERE id_orders = {$_SESSION['id_order']}
												AND `value` IS NOT NULL
											ORDER BY nome, total";
									$query = mysql_query($sql);
									$n_items = mysql_num_rows($query);

									if ($n_items > 0) {
										
										$body .= "
											<table width='100%' cellpadding='5' cellspacing='0'>
											<div style='background:#525252; margin:10px 0 0 0; padding:5px; color:#fff; font-weight:bold;'>Caixas j&aacute; embaladas, a segurar</div>
											<div style='border:1px solid #f1f1f1; padding:0px;'>
												<table width='100%' cellpadding='3' cellspacing='0' style='font-size:12px;'>
													<tr style='background:#DADADA; color:#333;'>
														<td>&nbsp;</td>
														<td align='center'>".QUANTIDADE."</td>
														<td align='right'>".VALOR." &euro;</td>
													</tr>";

												$cor = "#fff";
												for ($j=0;$j<$n_items;$j++) {
													$u = mysql_fetch_object($query);

													$body .= "
														<tr style='background:{$cor};'>
															<td>".$u->nome."</td>
															<td align='center'>{$r->total}</td>
															<td align='right'>".number_format($r->value,2,',','.')."</td>
														</tr>";
													
													if ($cor=="#fff") $cor = "#f1f1f1";
													else $cor = "#fff";

												}					

										$body .= "</table>";
									}					

							$body .= "
								</td>
							</tr>
							";
						}

						$body .= "
							</table>
						</div>";

						if ($dados['lift_items'] != "") {
							$body .= "
								<div style='background:#66BD29; margin:10px 0 0 0; padding:5px; color:#fff; font-weight:bold;'>".NAO_CABEM_ELEVADOR."</div>
								<div style='border:1px solid #f1f1f1; padding:0px;'>
								<table width='100%' cellpadding='3' cellspacing='0' style='font-size:12px;'>
								<tr style='background:#fff;'>
									<td align='left'>".nl2br(stripslashes($dados['lift_items']))."</td>
								</tr>
								</table>
								</div>";
						}


						if ($dados['dep_emel'] == 1 || $dados['dest_emel'] == 1) {
							$body .= "
								<div style='background:#66BD29; margin:10px 0 0 0; padding:5px; color:#fff; font-weight:bold;'>".ESTACIONAMENTO."</div>
								<div style='border:1px solid #f1f1f1; padding:0px;'>
								<table width='100%' cellpadding='3' cellspacing='0' style='font-size:12px;'>
									<tr>
										<td>";
						
							if ($dados['dep_emel'] == 1 && $dados['dest_emel'] == 0) $body .= "<tr><td width='85%' align='left'>".ESTACIONAMENTO_OPCAO_1.EM."<b>".strtolower(MORADA_ORIGEM)."</b></td>";
							elseif ($dados['dep_emel'] == 0 && $dados['dest_emel'] == 1) $body .= "<tr><td width='85%' align='left'>".ESTACIONAMENTO_OPCAO_1.EM."<b>".strtolower(MORADA_DESTINO)."</b></td>";
							elseif ($dados['dep_emel'] == 1 && $dados['dest_emel'] == 1) $body .= "<tr><td width='85%' align='left'>".ESTACIONAMENTO_OPCAO_1.EM."<b>".strtolower(MORADA_ORIGEM)."</b>".E."<b>".strtolower(MORADA_DESTINO)."</b></td>";

							$body .= "
										<td align='right'>".number_format($_SESSION['total_parking'],2,',','.')."</td>
									</tr>
								</table>
								</div>";
						}

						if ($dados['dep_psp'] == 1 || $dados['dest_psp'] == 1) {
							$body .= "
								<div style='background:#66BD29; margin:10px 0 0 0; padding:5px; color:#fff; font-weight:bold;'>".AGENTE_PSP."</div>
								<div style='border:1px solid #f1f1f1; padding:0px;'>
								<table width='100%' cellpadding='3' cellspacing='0' style='font-size:12px;'>
									<tr>
										<td>";

							if ($dados['dep_psp'] == 1 && $dados['dest_psp'] == 0) $body .= "<tr><td width='85%' align='left'>".ESTACIONAMENTO_OPCAO_2.EM."<b>".strtolower(MORADA_ORIGEM)."</b></td>";
							elseif ($dados['dep_psp'] == 0 && $dados['dest_psp'] == 1) $body .= "<tr><td width='85%' align='left'>".ESTACIONAMENTO_OPCAO_2.EM."<b>".strtolower(MORADA_DESTINO)."</b></td>";
							elseif ($dados['dep_psp'] == 1 && $dados['dest_psp'] == 1) $body .= "<tr><td width='85%' align='left'>".ESTACIONAMENTO_OPCAO_2.EM."<b>".strtolower(MORADA_ORIGEM)."</b>".E."<b>".strtolower(MORADA_DESTINO)."</b></td>";

							$body .= "
										<td align='right'>".number_format($_SESSION['total_psp'],2,',','.')."<br></td>
									</tr>
								</table>
								</div>";
						}

						$body .= "
						<div style='background:#66BD29; margin:10px 0 0 0; padding:5px; color:#fff; font-weight:bold;'>".ORCAMENTO."</div>
						<div style='border:1px solid #f1f1f1; padding:5px;'>
						<table width='100%' cellpadding='3' cellspacing='0' style='font-size:12px;'>
							<tr>
								<td width='65%' style='background:#F0F0F0;'>".MUDANCA_."</td>
								<td align='right' style='background:#F0F0F0;'>".$_SESSION['total_mudanca']." &euro;</td>
							</tr>
							<tr>
								<td class='OrcTd2'>".MATERIAIS_DE_EMBALAGEM_."</td>
								<td align='right' style='background:#F0F0F0;'>".$dados['total_boxes_sm3']." &euro;</td>
							</tr>
							<tr>
								<td style='background:#F0F0F0;'>".SEGURO."</td>
								<td align='right' class='OrcTd2'>".$_SESSION['insurance_final']." &euro;</td>
							</tr>
							<tr>
								<td  class='OrcTd2'>".ESTACIONAMENTO."</td>
								<td align='right' style='background:#F0F0F0;'>".$_SESSION['total_parking']." &euro;</td>
							</tr>
							<tr>
								<td style='background:#F0F0F0;'>".AGENTE_PSP."</td>
								<td align='right' class='OrcTd2'>".$_SESSION['total_psp']." &euro;</td>
							</tr>
							<tr>
								<td  class='OrcTd2'><b>Total</b></td>
								<td style='background:#F0F0F0;' align='right'><b>".($_SESSION['total'])." &euro;</b></td>
							</tr>
							<tr>
								<td style='background:#F0F0F0;'>IVA {$_SESSION['percentual_iva']}%</td>
								<td  class='OrcTd1' align='right'>".$_SESSION['iva']." &euro;</td>
							</tr>
							<tr>
								<td  style='background:#E6E6E6'><strong>Total + IVA</strong></td>
								<td style='background:#E6E6E6' align='right'><strong>".$_SESSION['total_iva']." &euro;</strong></td>
							</tr>
							<tr>
								<td style='background:#F2F2F2'>".TOTAL_SINAL_RESERVA_ONLINE." {$_SESSION['percentual_reserva_online']}%</td>
								<td style='background:#F2F2F2' align='right'>".$_SESSION['total_reserva_online']." &euro;</td>
							</tr>
						</table>
					</div>".tools::pageInfo()."
					</div>
				</body>
				</html>
				";

		$from = "info@citymover.pt";
		$headers  = "MIME-Version: 1.0\n";
		$headers .= "Content-type: text/html; charset=UTF-8\n";
		$headers .= "From: $from";

		if($_SERVER["REMOTE_ADDR"] == "95.95.76.64" || $_SERVER["HTTP_HOST"] == "localhost"){
			mail("franco.silva@bright.pt","cityMover.pt: Tentativa de orçamento {$_SESSION['id_order']} | ".$level,$body,$headers);
		}
		else{
			mail("logistica@citymover.pt,info@citymover.pt","cityMover.pt: Tentativa de orçamento {$_SESSION['id_order']} | ".$level,$body,$headers);
			mail("hugo.silva@bright.pt","cityMover.pt: Tentativa de orçamento {$_SESSION['id_order']} | ".$level,$body,$headers);	
		}

		
	}

	static function setTitle($title) {
		settings::set("site_title",settings::get("site_title").$title);
	}

	static function getPost($s) {
				if (array_key_exists($s, $_POST)) return htmlspecialchars($_POST[$s]);
				else return false;
	}
	
	static function getGet($s) {
				if (array_key_exists($s, $_GET)) return htmlspecialchars($_GET[$s]);
				else return false;
	}
	
	static function getParam($s) {
			$a = $_GET['rewrite']; 
			$a = substr($a,0,-5); 
			$params = explode("-",$a);
			$key = array_search($s,$params);
			if ($key !== FALSE) {
				if (array_key_exists($key+1, $params)) return htmlspecialchars($params[$key+1]);
				else return false;
			}
			else return false;
	}
	static function refresh($url = false) {
		    $url = $url ? "\"$url\"" : "window.location";
			echo "<script>window.location=$url</script>";
			die();
	}
	static function mc() {
		return time();
	}
	
	static function loggedIn() {
		return UserLogin::logged();
	}
	
	static function getUsername() {
		UserLogin::logged();
		return Settings::get('username');
	}
	
	static function getUserId() {
		UserLogin::logged();
		return Settings::get('id');
	}
	
	
	static function checkAndFixUrl($url) {
		if (preg_match("/^http:\\/\\/(.*)$/", $url)) return $url;
		else return "http://".$url;
	}
	
	static function makeFriendlyUrl($str,$tipo='') {
		$str = strtolower($str);
		if (strlen($str) > 40) $str = substr($str, 0, 40);
		
		/* save portuguese chars */
	
		$str = str_replace("Á","a",$str);
		$str = str_replace("Â","a",$str);
		$str = str_replace("Ã","a",$str);
		$str = str_replace("À","a",$str);
		
		$str = str_replace("â","a",$str);
		$str = str_replace("ã","a",$str);
		$str = str_replace("á","a",$str);
		$str = str_replace("à","a",$str);
		
		$str = str_replace("Í","i",$str);
		$str = str_replace("Ì","i",$str);
		$str = str_replace("Î","i",$str);
		
		$str = str_replace("í","i",$str);
		$str = str_replace("î","i",$str);
		$str = str_replace("í","i",$str);
		
		$str = str_replace("É","e",$str);
		$str = str_replace("Ê","e",$str);
		$str = str_replace("È","e",$str);
		
		$str = str_replace("é","e",$str);
		$str = str_replace("è","e",$str);
		$str = str_replace("ê","e",$str);
		
		$str = str_replace("Õ","o",$str);
		$str = str_replace("Ô","o",$str);
		$str = str_replace("Ó","o",$str);
		$str = str_replace("Ò","o",$str);
		
		$str = str_replace("ó","o",$str);
		$str = str_replace("õ","o",$str);
		$str = str_replace("ô","o",$str);
		$str = str_replace("ò","o",$str);
		
		$str = str_replace("Û","u",$str);
		$str = str_replace("Ú","u",$str);
		$str = str_replace("Ù","u",$str);
		
		$str = str_replace("ú","u",$str);
		$str = str_replace("û","u",$str);
		$str = str_replace("ù","u",$str);
		
		$str = str_replace("ç","c",$str);
		$str = str_replace("Ç","c",$str);
		
		$str = str_replace("Ñ","n",$str);
		$str = str_replace("ñ","n",$str);
		
		$c = strlen($str);

		if ($tipo != "variaveis")
			for ($i=0;$i<$c;$i++) {
					if (!ctype_alnum($str[$i]) && $str[$i] != "-") {
						$str[$i] = "-";
					}
			}
		else {
			$str = str_replace(" ","",$str);
			$str = str_replace("'","",$str);
		} 
		
		while (strpos($str, "--")!==false) $str = str_replace("--",$tipo,$str);

		return $str;
	}
	
	/* ar niekada nebuna, kad reik surusiuot dvimaty masyva pagal rakta antram lygy? buna, buna... */
	
	static function psdSort($array, $key, $dir) {
		$c = count($array);
		for($x = 0; $x < $c; $x++) {
		  for($y = 0; $y < $c; $y++) {
			if ($dir == "ASC") {
			    if($array[$x][$key] < $array[$y][$key]) {
			      $hold = $array[$x];
			      $array[$x] = $array[$y];
			      $array[$y] = $hold;
			    }
			}
			if ($dir == "DESC") {
			    if($array[$x][$key] > $array[$y][$key]) {
			      $hold = $array[$x];
			      $array[$x] = $array[$y];
			      $array[$y] = $hold;
			    }
			}
		  }
		}
		return $array;
	}

	function resizeImg($source, $dir, $maxW, $maxH, $forceName=false) {
		//'&#169; rallynews.lt'
			
			if ($imgParams = getimagesize($source)) {
			$pat = "/^([a-zA-Z]+)\/([a-zA-Z-]+)$/";
			preg_match($pat, $imgParams['mime'], $m);

			switch (strtolower($m[2])) {
					default: return false;
					case "pjpeg":
					case "jpeg": $ftype = $type = "jpeg"; break;
					case "gif": $ftype = $type =  "gif"; break;
					case "x-png":
					case "png": $ftype = $type = "png"; break;
					
			}
			eval ("\$src = @imagecreatefrom$type(\$source);");


			$w1 = $imgParams[0];
			$h1 = $imgParams[1];
			if ($maxW < $w1) {
					$f = $w1/$maxW;
					$w1 = $maxW;
					$h1 = round($h1/$f);
			}
			if ($maxH < $h1) {
					$f = $h1/$maxH;
					$h1 = $maxH;
					$w1 = round($w1/$f);
			}
			
			
			$dest1 = imagecreatetruecolor($w1, $h1);
			imagecopyresampled($dest1, $src, 0, 0, 0, 0, $w1, $h1, $imgParams[0], $imgParams[1]);		
				
			$name = tools::mc() . rand(1,666) .".". $ftype;
			if ($forceName) $name = $forceName;
			$fullThumbName = $dir.$name;
		
			eval ("image$type(\$dest1,\$fullThumbName);");
			imagedestroy($dest1);
			imagedestroy($src);
		}
		
		if (isset($name)) return $name;
		else return false;

	}
	
	function includeEditor($title, $value, $toolbar="Default", $height=500) {
		include_once("../js/fckeditor/fckeditor.php");
		$sBasePath = "../js/fckeditor/";
		$oFCKeditor = new FCKeditor($title);
		$oFCKeditor->BasePath = $sBasePath;
		$oFCKeditor->Config['SkinPath'] = '../../../js/fckeditor/editor/skins/silver/' ;
		$oFCKeditor->Value = $value;
		$oFCKeditor->Height = $height;
		$oFCKeditor->Config['EnterMode'] = 'br';
		$oFCKeditor->ToolbarSet = $toolbar;

		$t = $oFCKeditor->CreateHtml();

		return $t;
	}

	static function isEmail ($email) {
		if(eregi("^[_\.0-9a-zA-Z-]+@([0-9a-zA-Z][0-9a-zA-Z-]+\.)+[a-zA-Z]{2,6}$", $email)) return true;
		else return false;
	}
	
	static function showPages($url, $cur, $tot, $pp = 0, $tp=5) {
		/*
		$cur - current page
		$tot - Total Pages
		$pp - Per page
		*/


	   if (!$url || !$cur || !$tot || $tp == 0) return false;
	   $links = '';
	   if ($pp <= 0) return false;
	   $pages = $total = ceil($tot / $pp);
	   if ($cur > 1) $links = "<a href='{$url}1'>&laquo;</a> ";
	   if ($cur > 1) $links .= "<a href='{$url}".($cur-1)."'><</a> ";
	   if ($pages > $tp) $pages = $tp;
	   $pm = floor($tp/2);
	   if ($cur - $pm > 0) { $start = $cur - $pm; $end = $start + $tp; }
	   else { $start = 1; $end = 1+$tp; }
	   if ($end > $total) { $end = $total; $start = $end - $tp; }
	   if ($start < 1) $start =1;
	   for ($i=$start;$i<=$end;$i++) {
	   	   $links .= " <a href='$url".$i."'".($cur==$i?' class="active"':'').">";
	
		   $links .= $i;
		
		   $links .= "</a> ";
		}
	  if ($cur < $total) $links .= "<a href='$url".($cur+1)."'>></a> ";
	  if ($cur < $total) $links .= "<a href='$url".($end)."'>&raquo;</a> ";

	  return $links;
	 }
	 

	
	static function getUserIdFromEmail($email){
		$sql = "SELECT `id` FROM `users` WHERE `email` ='$email'";
		$query = mysql_query($sql);
		if(mysql_num_rows($query)>0) return mysql_result($query,0,0); else return false;
	}
	static function getUserIdFromNick($nick){
		$sql = "SELECT `id` FROM `users` WHERE `username` ='$nick'";
		$query = mysql_query($sql);
		if(mysql_num_rows($query)>0) return mysql_result($query,0,0); else return false;
	}
		
	static function getNickFromUserId($id){
		$sql = "SELECT `username` FROM `users` WHERE `id` ='$id'";
		$query = mysql_query($sql);
		if(mysql_num_rows($query)>0) return mysql_result($query,0,0); else return false;
	}

	static function getNameFromUserId($id){
		$sql = "SELECT `name_surname` FROM `users` WHERE `id` ='$id'";
		$query = mysql_query($sql);
		if(mysql_num_rows($query)>0) return mysql_result($query,0,0); else return false;
	}
	
	static function getCurrentUserLevel() {
		$level = 1;
		$s = settings::get('user');
		if (!$s) return $level;
		$level = $s->level;
		return $level;
	}
	
	static function showError($err) {
		$t = "<p class='error'>{$err}</p>";
		
		return $t;
	}
	
	static function showOk($ok) {
		$t = "<p class='ok'>{$ok}</p>";
		
		return $t;
	}
	
	static function showNotice($str, $position="") {
		$t = '<p class="notice" style="'.$position.'">'.$str.'</p>';
		return $t;
	}
	
	

	
	static function userIsTrusted() {
		if (Settings::get("user_trusted")) return true;
		else return false;
	}
	
	static function before($mc) 
	{
		$now = tools::mc();
		if ($now>$mc) $diff = $now - $mc;
		else $diff = $mc - $now;

		$d = '';

		if ($diff < 60) {
			$d = $diff . DATE_SEC;
		}
		elseif ($diff < 3600) 
		{
			//iki valandos
			$d = round($diff/60) . DATE_MIN;
		}
		elseif ($diff < 3600*24)
		{
			//iki paros
			$d = round($diff/3600) . DATE_HOUR;
		}
		elseif ($diff < 3600*24*30) 
		{
			//iki menesio
			$d = round($diff/(3600*24)) . DATE_DAY;
		}
		elseif ($diff < 3600*24*365)
		{
			//iki metu
			$d = round($diff/(3600*24*30)) . DATE_MONTH;
		}
		else {
			//virs metu
			$d = round($diff/(3600*24*365)) . DATE_YEAR;
		}

		return $d;
	}

	static function getStringCut( $string, $limit )
	{
		$exp 	= explode( " ", $string );
		$string = "";

		if( count( $exp ) < $limit )
			$limit = count( $exp );

		for( $i = 0; $i < $limit; $i++ ) 
			$string .= $exp[ $i ] . " ";
		
		return trim( $string );
	}
	
	static function getUniqueFilename($dir, $name) {
		if (!file_exists($dir.$name)) return $name;
		$t = explode('.', $name);
		if (count($t)<=1) return false;
		$xt = $t[count($t)-1];
		$file = substr($name, 0, strlen($name)-strlen($xt)-1);
		
		for ($i=0;$i<100;$i++) {
			$f = $file.$i.'.'.$xt;
			if (!file_exists($dir.$f)) return $f;
		}
		return false;
	}

	function save($step) {
		
		
		if ($step==1) {
			$originCity = addslashes($_SESSION['dep_city']);
			$destinationCity = addslashes($_SESSION['dest_city']);
			$company = addslashes($_SESSION['company']);
			$name = addslashes($_SESSION['name']);
			$address = addslashes($_SESSION['address']);
			$address_dest = addslashes($_SESSION['address_dest']);
			$date_delivery = substr($_SESSION['data_entrega'],6,4)."-".substr($_SESSION['data_entrega'],3,2)."-".substr($_SESSION['data_entrega'],0,2);
			$dep_lift = $_SESSION['dep_lift']=='yes'?1:0;
			$dest_lift = $_SESSION['dest_lift']=='yes'?1:0;



			if ($_SESSION['id_order'] > 0) {
				$sql = "UPDATE `orders` SET
							`company` = '{$company}',
							`address` = '{$address}',
							`address_dest` = '{$address_dest}',
							`name` = '{$name}',
							`phone` = '{$_SESSION['phone']}',
							`mobile` = '{$_SESSION['mobile']}',
							`email` = '{$_SESSION['email']}',
							`taxnumber` = '{$_SESSION['taxnumber']}',
							`dep_city` = '{$originCity}',
							`dep_zip` = '{$_SESSION['dep_zip']}',
							`dest_city` = '{$destinationCity}',
							`dest_zip` = '{$_SESSION['dest_zip']}',
							`dep_lift` = {$dep_lift},
							`dest_lift` = {$dest_lift},
							`zone` = {$_SESSION['zone']},
							`date_delivery` = '{$date_delivery}',
							`id_user` = '{$_SESSION['id_user']}',
							`kit_id` = '{$_SESSION['kit_id']}',
							`kit_price` = '{$_SESSION['kit_price']}',
							`kit_name` = '{$_SESSION['kit_name']}',
							`discount_percent` = '{$_SESSION['discount_percent']}',
							`level` = '{$_SESSION['type']}'
						WHERE `id` = {$_SESSION['id_order']} 
						";
//							AND `status` = '6'
			}
			else {
			
				$query = "select * from prices where id = 1";
				$res = mysql_query($query);
				$row = mysql_fetch_array($res);
				$price_m3 = $row[ $_SESSION['type'] ];

				
				$sql = "INSERT INTO `orders` SET
							`company` = '{$company}',
							`address` = '{$address}',
							`address_dest` = '{$address_dest}',
							`name` = '{$name}',
							`phone` = '{$_SESSION['phone']}',
							`mobile` = '{$_SESSION['mobile']}',
							`email` = '{$_SESSION['email']}',
							`taxnumber` = '{$_SESSION['taxnumber']}',
							`dep_city` = '{$originCity}',
							`dep_zip` = '{$_SESSION['dep_zip']}',
							`dest_city` = '{$destinationCity}',
							`dest_zip` = '{$_SESSION['dest_zip']}',
							`dep_lift` = {$dep_lift},
							`dest_lift` = {$dest_lift},
							`date_delivery` = '{$date_delivery}',
							`insurance` = '-1',
							`level` = '{$_SESSION['type']}',
							`zone` = {$_SESSION['zone']},
							`ip` = '".getenv("REMOTE_ADDR")."',
							`status` = '6',
							`lang` = '{$_SESSION['lang']}',
							`research_code` = '".tools::geraCodigo()."',
							`id_user` = '{$_SESSION['id_user']}',
							`kit_id` = '{$_SESSION['kit_id']}',
							`kit_price` = '{$_SESSION['kit_price']}',
							`kit_name` = '{$_SESSION['kit_name']}',
							`discount_percent` = '{$_SESSION['discount_percent']}',
							`price_m3` = '{$price_m3}',
							`date` = now()						
							";
			}

			mysql_query($sql);

			if ($_SESSION['id_order'] == "" || $_SESSION['id_order'] == 0) {
				$ultimo = mysql_fetch_object(mysql_query("SELECT LAST_INSERT_ID() id_order"));
				$_SESSION['id_order'] = $ultimo->id_order;
			}

			if ($_SESSION['id_order'] == "" || $_SESSION['id_order'] == 0) { 
				header("Location: index.php?mod=form&step=2");
				exit;
			}

			# Adiciona os ítens
			# Ítens
			if (is_array($_SESSION['items'])) {
				mysql_query("DELETE FROM orders_items WHERE id_orders = {$_SESSION['id_order']}");
				foreach ($_SESSION['items'] as $item) {
					mysql_query("INSERT INTO orders_items SET 
									id_orders = {$_SESSION['id_order']}, 
									id_items = {$item['id']}, 
									total = {$item['total']}
								");			
				}
			}

			# Outros ítens
			if (is_array($_SESSION['items_outros'])) {
				foreach ($_SESSION['items_outros'] as $item2) {
					mysql_query("INSERT INTO orders_items SET 
									id_orders = {$_SESSION['id_order']}, 
									outros = '".addslashes($item2['name'])."',
									largura = {$item2['largura']},
									comprimento = {$item2['comprimento']},
									altura = {$item2['altura']},
									total = {$item2['total']}
								");					
				}			
			}

			# Boxes
			if (is_array($_SESSION['boxes'])) {
				mysql_query("DELETE FROM orders_boxes WHERE id_orders = {$_SESSION['id_order']}");
				foreach ($_SESSION['boxes'] as $item) {
					mysql_query("INSERT INTO orders_boxes SET 
									id_orders = {$_SESSION['id_order']}, 
									id_boxes = {$item['id']},
									total = {$item['total']}
								");
				}
			}

			# Boxes
			if (is_array($_SESSION['days'])) {
				foreach ($_SESSION['days'] as $item) {
					mysql_query("UPDATE orders_boxes SET days = {$item['total']}
								 WHERE id_orders = {$_SESSION['id_order']}
									AND id_boxes = {$item['id']} 
								");
				}			
			}

			# Packed
			if (is_array($_SESSION['packed'])) {
				mysql_query("DELETE FROM orders_packed WHERE id_orders = {$_SESSION['id_order']}");
				foreach ($_SESSION['packed'] as $item) {
					mysql_query("INSERT INTO orders_packed SET 
									id_orders = {$_SESSION['id_order']}, 
									nome = '".addslashes($item['nome'])."', 
									largura = {$item['largura']}, 
									comprimento = {$item['comprimento']}, 
									altura = {$item['altura']}, 
									total = {$item['total']}
								");
				}			
			}
		}
		elseif ($step==2) {
			$insurance = $_SESSION['insurance_']=='yes'?1:0;
			$value = "";

			# SEGURO: Adiciona os ítens
			if ($insurance==1) {
				$insurance_value = $_SESSION['insurance_value'];

				# SEGURO: Adiciona os ítens. Vai o id referente a tabela [items]
				if (is_array($_SESSION['items_insurance_value'])) {
					foreach ($_SESSION['items_insurance_value'] as $item) {
						$query = mysql_query("SELECT id, id_items FROM orders_items WHERE id_orders = {$_SESSION['id_order']} AND id_items = '{$item['id']}'");

						if (mysql_num_rows($query) > 0) {
							mysql_query("UPDATE orders_items SET `value` = '{$item['value']}'
										 WHERE id_orders = {$_SESSION['id_order']}
											AND id_items = {$item['id']}
										");						
						}
						else {
							mysql_query("UPDATE orders_items SET `value` = '{$item['value']}'
										 WHERE id_orders = {$_SESSION['id_order']} 
											AND id = ".substr($item['id'],1,strpos($item['id'],"-")-1)
										);
						}
					}
				}

				# SEGURO: Adiciona as caixas
				if (is_array($_SESSION['boxes_insurance_value'])) {
					foreach ($_SESSION['boxes_insurance_value'] as $item) {
						mysql_query("UPDATE orders_boxes SET `value` = '{$item['value']}'
									 WHERE id_orders = {$_SESSION['id_order']} 
										AND id_boxes = {$item['id']}
									");
					}
				}

				# SEGURO: Adiciona as caixas já embaladas
				if (is_array($_SESSION['packed_insurance_value'])) {
					foreach ($_SESSION['packed_insurance_value'] as $item) {
						mysql_query("UPDATE orders_packed SET `value` = '{$item['value']}'
									 WHERE id_orders = {$_SESSION['id_order']} 
										AND nome = '".addslashes($item['original'])."'
									");
					}
				}
			}
			else {
				mysql_query("UPDATE orders_items SET value = '' WHERE id_orders = {$_SESSION['id_order']}");
				mysql_query("UPDATE orders_boxes SET value = '' WHERE id_orders = {$_SESSION['id_order']}");
				mysql_query("UPDATE orders_packed SET value = '' WHERE id_orders = {$_SESSION['id_order']}");
			}





			$totais_de_area = tools::calculate_areas( $_SESSION["id_order"], $_SESSION["type"] );












			# ELEVADOR E ESTACIONAMENTO
			$dep_emel = $_SESSION['dep_emel']==''?0:1;
			$dep_psp = $_SESSION['dep_psp']==''?0:1;
			$dest_emel = $_SESSION['dest_emel']==''?0:1;
			$dest_psp = $_SESSION['dest_psp']==''?0:1;
			$lift_items = $_SESSION['lift']=='yes'?addslashes($_SESSION['lift_items']):"";

			$sql = "UPDATE orders SET 
						insurance = {$insurance},
						insurance_value = '{$insurance_value}',
						lift_items = '{$lift_items}',
						dep_emel = {$dep_emel},
						dep_psp = {$dep_psp},
						dest_emel = {$dest_emel},
						total_area = ".$totais_de_area["total_area"].",
						preco_mudanca_hora = ".$totais_de_area["preco_mudanca_hora"].",
						preco_mudanca_hora_outro = ".$totais_de_area["preco_mudanca_hora_outro"].",
						dest_psp = {$dest_psp}
					WHERE id = {$_SESSION['id_order']}";

			
			mysql_query($sql);
		}
		elseif ($step==3) {
			$sql = "UPDATE orders SET comments = '".addslashes($_SESSION['comments'])."'
					WHERE `id` = {$_SESSION['id_order']}";
			mysql_query($sql);
		}
	}



	function calculate_areas( $order_id, $level ){
		//Recalcular o preço mudança hora
		//Recalcular o preço por hora

		$query_m3 = "select orders_boxes.*, boxes.m3 from orders_boxes left join boxes on orders_boxes.id_boxes = boxes.id where orders_boxes.id_orders = '".$order_id."'";
		$res_m3 = mysql_query($query_m3);
		while( $row_m3 = mysql_fetch_object($res_m3) ) {
			$dados["total_area"] += ( $row_m3->m3 * $row_m3->total );
			$teste1 = $dados["area_total"];
		}
		
		$query_m3 = "select * from orders_items left join items on orders_items.id_items = items.id where orders_items.id_orders = '".$order_id."'";
		$res_m3 = mysql_query($query_m3);
		while( $row_m3 = mysql_fetch_object($res_m3) ) {
			$dados["total_area"] += ( $row_m3->m3 * $row_m3->total );
			$teste2 = $dados["area_total"];
		}
		
		$query_m3_packed = "select * from orders_items where id_orders = '".$order_id."' and id_items is null";
		$res_m3_packed = mysql_query( $query_m3_packed );
		while ( $row_m3_packed = mysql_fetch_object($res_m3_packed) ) {
			$dados["total_area"] += ( ($row_m3_packed->largura / 100)*($row_m3_packed->comprimento / 100)*($row_m3_packed->altura / 100)*$row_m3_packed->total   );
			$teste3 = $dados["area_total"];
		}


		$query_m3_packed = "select * from orders_packed where id_orders = '".$order_id."'";
		$res_m3_packed = mysql_query( $query_m3_packed );
		while ( $row_m3_packed = mysql_fetch_object($res_m3_packed) ) {
			$dados["total_area"] += ( ($row_m3_packed->largura / 100)*($row_m3_packed->comprimento / 100)*($row_m3_packed->altura / 100)*$row_m3_packed->total   );
			$teste3 = $dados["area_total"];
		}

		$query_volume = "SELECT ".$level." as value from prices where id = 8";
		$res_volume = mysql_query($query_volume);
		$row_volume_small = mysql_fetch_object($res_volume);

		$query_volume = "SELECT ".$level." as value from prices where id = 10";
		$res_volume = mysql_query($query_volume);
		$row_volume_large = mysql_fetch_object($res_volume);


		if ( !isset($dados["total_area"])) {
			$dados["total_area"] = 0;
		}

		if ( $dados["total_area"] < $row_volume_small->value ) {	//Carrinha pequena
			$query_price = "SELECT ".$level." as value from prices where id = 6";
			$query_outro_price = "SELECT ".$level." as value from prices where id = 7";
		}elseif( $dados["total_area"] < $row_volume_large->value ) {	//Carrinha Grande
			$query_price = "SELECT ".$level." as value from prices where id = 7";
			$query_outro_price = "SELECT ".$level." as value from prices where id = 6";
		}else{
			$query_price = "SELECT ".$level." as value from prices where id = 11";
			$query_outro_price = "SELECT ".$level." as value from prices where id = 7";
		}
		$res_price = mysql_query($query_price);
		$row_price = mysql_fetch_object($res_price);
		$dados["preco_mudanca_hora"] = $row_price->value;

		$res_price = mysql_query($query_outro_price);
		$row_price = mysql_fetch_object($res_price);
		$dados["preco_mudanca_hora_outro"] = $row_price->value;

		return $dados;

	}

}

?>