<?php

class Database
{

    public $host;
    public $user;
    public $database;
    public $password;
    public $mysqli;
    public static $instance;
    static $force_online = true;

    function __construct()
    {
        if(self::$force_online == false) {
            //definir dados da ligação
            switch ($_SERVER["HTTP_HOST"]) {
                case 'localhost':

                    $this->host = "localhost";
                    $this->user = "root";
                    $this->password = "";
                    $this->database = "SUBSTITUIR";
                    break;

                case 'sushi.local':

                    $this->host = "localhost";
                    $this->user = "root";
                    $this->password = "";
                    $this->database = "sushibox";
                    break;

                default:

                    break;
            }
        } else {
            $this->host = "citymover.pt";
            $this->user = "citymove_worten";
            $this->password = "wooodhu8b78f";
            $this->database = "citymove_worten";
        }

        //go
        $connection = $this->connect();
        $this->mysqli = $connection;

        return $connection;
    }

    //alterar para mysql_connect
    function connect() {
        if (class_exists('medoo')) {
            $database = new medoo(array(
                'database_type' => 'mysql',
                'database_name' => $this->database,
                'server'        => $this->host,
                'username'      => $this->user,
                'password'      => $this->password,
                'charset'       => 'utf8'
            ));

            self::$instance = $database; //create a static instance of the medoo orm
        }

        $connection = mysql_connect($this->host, $this->user, $this->password) or die( mysql_error() );
        mysql_select_db($this->database);
        mysql_query("SET NAMES UTF8"); //fix encoding
        mysql_query("SET SESSION interactive_timeout = (28800*3)");
        mysql_query("SET SESSION wait_timeout = (28800*3)");

        return ($connection !== false) ? $connection:false;
    }
}
