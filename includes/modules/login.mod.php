<?php

/**
* Login
*/
class Login
{
	
	function __construct()
	{
		$this->load(); //loads current settings using session
		if(!empty($_POST))
			$this->validate();
	}

	function load(){
		$this->authenticated = $_SESSION["authenticated"];
	}

	function validate(){

		if($_POST["user"] == "worten" && $_POST["password"] == "worten18"){
			$this->set_authenticated(true);
			tools::notify_add("Olá, Worten!", "alert");
			redirect(base_url("?mod=form"));
		}
		else{
			tools::notify_add("Por favor confirme os seus dados", "error");
			$this->set_authenticated(false);

			if (current_url() != base_url()) {
                redirect(base_url());
            }
		}

	}

	function set_authenticated($bool){

		if($bool === true)
			$this->authenticated = $_SESSION["authenticated"] = true;
		else
			$this->authenticated = $_SESSION["authenticated"] = false;
	}
}

?>