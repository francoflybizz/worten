<?php

class Dashboard
{
	
	function __construct()
	{
	    $this->assets = array(
	        'css' => array(
	            base_url('css/dashboard.css')
            ),
	        'js' => array(
                'https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.1/Chart.min.js',
                base_url('js/dashboard.js')
            )
        );

	    if ($_GET['view']) {
	        $this->view = $_GET['view'];
        }
	}

    public static function statusToLabel($status)
    {
        $statusArr = array(
            0 => '<span class="badge badge-warning">Pendente</span>',
            1 => '<span class="badge badge-primary">Em aprovação</span>',
            2 => '<span class="badge badge-info">Em andamento</span>',
            3 => '<span class="badge badge-success">Concluído</span>',
            4 => '<span class="badge badge-danger">Não aprovado</span>'
        );

        return $statusArr[$status];
	}
}
