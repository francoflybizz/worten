<?php 

/**
* Form
*/
class Form
{
	var $out = '';
	var $tools;
	var $saved = 0;
	var $discount = 0;
	var $step = 1;
	var $maxStep = 6;

	function __construct()
	{
		$step = !empty($_GET["step"]) ? $_GET["step"] : 1;
		$this->load_step($step);

		$this->assets = array(
			'js' => array(
		        base_url('js/angular-form.js') . '?' . md5(filemtime(base_path('js/angular-form.js')))
            )		    
        );

	}

	function load_step($step){

		//logic info - copy pasted from citymover
		if ($step==1) $this->showStep1();
		elseif ($step==2) $this->showStep2();
		elseif ($step==3) $this->showStep3();
		elseif ($step==4) $this->showStep4();
		elseif ($step==5) $this->showStep5();
		elseif ($step==6) $this->showStep6();

		//view info
		$this->view = "form/step-" . $step;
	}

	function show() {

		$this->tools = new tools();
		//$this->type = $_POST['type']?$_POST['type']:"home";

		if (!$_POST["step"]) $this->step = $step = $_GET["step"];
		else $this->step = $step = $_POST["step"];

		if ($_GET['lang']) $_SESSION['lang'] = $_GET['lang']?$_GET['lang']:"pt";
		$this->lang = $_SESSION['lang']=="en"?$_SESSION['lang']:"pt";
		$query = "update visits set went_to_form = '".$step."' where id = '".$_SESSION["visited_id"]."' and went_to_form < '".$step."'";
		//echo $query;
		mysql_query($query) or die(mysql_error());

		
		return $this->out;
	}

	function showHeader() {

		$textMenu[1] = PASSO." 1 <span>".SEUS_DADOS."</span>";
		$textMenu[2] = PASSO." 2 <span>".MUDANCA_."</span>";
		$textMenu[3] = PASSO." 3 <span>".OUTROS."</span>";
		$textMenu[4] = PASSO." 4 <span>".CONFIRMACAO_."</span>";
		$textMenu[5] = PASSO." 5 <span>".O_SEU_ORCAMENTO."</span>";

		$this->out .= "
		<html>
		<head>
			<meta name='viewport' content='width=device-width, initial-scale=1'>
			<link rel='stylesheet' href='https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css'>
			<link href='http://fonts.googleapis.com/css?family=Roboto:400,400italic,700,700italic,900' rel='stylesheet' type='text/css'>
			<link rel='stylesheet' type='text/css' href='" . base_url("js/noty/animate.css") . "' />
			<link rel='stylesheet' type='text/css' href='" . base_url("assets/css/bootstrap.min.css") . "' />
			<link rel='stylesheet' type='text/css' href='" . base_url("assets/css/citymover.old.css") . "' />
			<link rel='stylesheet' type='text/css' href='" . base_url("assets/css/responsive.css") . "' />
		</head>
		<body>
			<div class='form-container'>

				<div class='container'>

					<div class='row'>

						<div class='col-xs-12 col-sm-12 col-md-4 logo-wrap'>
							<img src='images/Logotipo_form.jpg' class='Logotipo img-responsive' />
						</div>

						<div class='col-xs-12 col-sm-12 col-md-8 steps-container'>
							<ul class='stepy-titles'>";

								for($i=1; $i < 6; $i++) {
									$classActive = "";
									if($i == $this->step) {
										$classLink="#";
										$classActive="current-step";
									}
									elseif($i > $this->step) {
										$classLink="#";
										$classActive="StepNext";
									}
									else {
										if ($i == 1) $classLink = OS_SEUS_DADOS;
										elseif ($i == 2) $classLink = MUDANCA;
										elseif ($i == 3) $classLink = SEGURO_E_ACESSIBILIDADES;
										elseif ($i == 4) $classLink = CONFIRMACAO;
										elseif ($i == 5) $classLink = PAGAMENTO;

										$classActive="StepPrev";
									}
									$this->out .= "<li class='stepy-titles ".$classActive."'><a href='".$classLink."' class='".$classActive."'>".$textMenu[$i]."</a></li>";
								}

								$this->out .= "
							</ul>
						</div>
					</div>
				</div>
				<div class='clearfix'></div>

			</div>";

			$display_on = "DisplayOff";
			if ($_SESSION['error_login'] != "") $display_on = "DisplayOn";

			$this->out .= '
			<div class="HelpBar">
				<div class="HelpBar_inner clearfix">
					<div class="HelpBarBox '.$display_on.'">
						<form method="post" onsubmit="return xek_fields();">
							<input type="hidden" name="registernews" value="1" />
							<table cellspacing="0" cellpadding="0" border="0" align="right" style="margin:10px 0">
								<tr>
									<td class="OrderTxt" style="color:#8F8F8F;">'.UTILIZADOR.':</td>
									<td>
										<input size="25" type="text" name="username" id="username" value="'.$_SESSION["user"].'" tabindex="11"
										style="border:0;margin-bottom:5px; background-color:#eeeeee; height:18px; font-size:12px">
										<div class="OrderError" id="err_user" style="display:none;">'.UTILIZADOR_ERRO.'</div>
									</td>
								</tr>

								<tr>
									<td class="OrderTxt" style="color:#8F8F8F;">'.PALAVRA_PASSE.':</td>
									<td>
										<input size="25" type="password" name="password" id="password" value="" tabindex="11"
										style="border:0;margin-bottom:5px; background-color:#eeeeee; height:18px; font-size:12px">
										<div class="OrderError" id="err_password" style="display:none;">'.PALAVRA_PASSE_ERRO.'</div>
									</td>';

									if ($_SESSION['error_login'] != "") {
										$this->out .= '
										<tr>
											<td colspan="2" class="OrderTxt" style="color:#CC3300; font-size:11px; padding-bottom:5px;">'.$_SESSION['error_login'].'</td>
										</tr>
										';
									}

									$this->out .= '
								</tr>
								<td colspan="2" class="HBColSubmit" align="right">
									<input type="submit" value=" OK " name="login_submit" id="login_submit" style="background-color:#66BD29; padding:1px; border:1px solid #999; cursor:pointer" />
								</td>
							</tr>
						</table>
					</form>
				</div>
			</div>
		</div>';

			# id FAQS
		if ($_SESSION['lang'] == 'pt') {
			$idFaq = $_SESSION['type'] == 'office'?37:7;
		}
		else $idFaq = $_SESSION['type'] == 'office'?37:7;

	}

	function showFooter() {
		$this->out .= '
		<!--div class="FormBottom"></div-->
		<input type="hidden" name="base_url" value="'.base_url().'">
		<script src="' . base_url("assets/js/jquery-1.11.1.min.js") .'"></script>
		<script src="'.base_url("js/noty/packaged/jquery.noty.packaged.min.js").'"></script>

		<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery.transit/0.9.12/jquery.transit.min.js"></script>
		<script src="' . base_url("assets/js/jquery.kwicks-1.5.1.js") .'"></script>
		<script src="' . base_url("assets/js/jquery-onload-functions.js") .'"></script>
		<script src="' . base_url("assets/js/citymover.old.js") .'"></script>
	</body>
	</html>
	';
}

function fatalError() {
	$this->out .= ERRO_1;
}

function showStep1() {

	if ($_POST['dep_city']) {
		$_SESSION['type'] = $_POST['type'];
		$_SESSION['dep_city'] = $_POST['dep_city'];
		$_SESSION['dest_city'] = $_POST['dest_city'];
		$_SESSION['data_entrega'] = $_POST['data_entrega'];
	}

	// lines below deleted on all steps, logic is handled by load_step() function using $this->view
	// $this->showHeader();
	// $this->out .= core::load_view("includes/views/form/step1.php");
	// $this->showFooter();

}


function showStep2() {

		//echo "step2";
		//var_dump( $_SESSION );
	if(empty($_SESSION["data_entrega"]))
		$_SESSION['data_entrega'] = $_POST['data_entrega'];


	$this->data_entrega = $_SESSION["data_entrega"];

	if ($_SESSION['data_entrega']=="") { die("step2"); exit(); header("location:index.php"); }

	if ($_POST) {
		$_SESSION['address'] = $_POST['address'];
		$_SESSION['dep_zip'] = $_POST['dep_zip'];
		$_SESSION['dest_zip'] = $_POST['dest_zip'];
		$_SESSION['address_dest'] = $_POST['address_dest'];
		$_SESSION['company'] = $_POST['company'];
		$_SESSION['name'] = $_POST['name'];
		$_SESSION['phone'] = $_POST['phone'];
		$_SESSION['mobile'] = $_POST['mobile'];
		$_SESSION['email'] = $_POST['email'];
		$_SESSION['taxnumber'] = $_POST['taxnumber'];
		$_SESSION['dep_lift'] = $_POST['dep_lift'];
		$_SESSION['dest_lift'] = $_POST['dest_lift'];
		$_SESSION['lift'] = (!empty($_POST['dep_lift']) || !empty($_POST['dest_lift'])) ? "yes" : false;
		//$_SESSION['lift_items'] = $_POST['lift_items'];
	}

		# Verifica se o código postal está dentro de uma zona autorizada
	$dep_cp4 = intval(substr($_SESSION['dep_zip'],0,4));
	$dest_cp4 = intval(substr($_SESSION['dest_zip'],0,4));

	$sql = "SELECT id zona, maxorders FROM zones
	WHERE ({$dep_cp4} between SUBSTRING(`start`,1,4) and SUBSTRING(`end`,1,4))";

		//echo $sql;

	$query = mysql_query($sql);
	$total = mysql_num_rows($query);
	if ( $total > 0 ) {
		$data = mysql_fetch_array($query, MYSQL_ASSOC);
		$_SESSION['origem_aut'] = 1;
	}
	else $_SESSION['origem_aut'] = 0;

	$sql = "SELECT id zona, maxorders FROM zones
	WHERE ({$dest_cp4} between SUBSTRING(`start`,1,4) and SUBSTRING(`end`,1,4))";
	$query = mysql_query($sql);
	$total = mysql_num_rows($query);
	if ($total > 0) {
		$data = mysql_fetch_array($query, MYSQL_ASSOC);
		$_SESSION['destino_aut'] = 1;
	}
	else $_SESSION['destino_aut'] = 0;

	if ($_SESSION['origem_aut']==1 && $_SESSION['destino_aut'] == 1) {
		$_SESSION['maxorders'] = $data['maxorders'];
		$_SESSION['zone'] = $data['zona'];
	}
	else $_SESSION['zone'] = 0;
}


function showStep3() {

		//upload files
	if(!empty($_FILES["photos"]["name"][0])){

			//garantir que a pasta existe com o ID da order
		$user_upload_folder = base_path("client_files/user_uploads/" . $_SESSION["id_order"]);
		$user_upload_folder_relative_path = "user_uploads/" . $_SESSION["id_order"];
			@mkdir($user_upload_folder); //cria a folder

			//found files
			foreach ($_FILES["photos"]["name"] as $key => $value) {

				if(empty($_FILES["photos"]["name"][ $key ])) continue;

				$override_var["slides"]["name"] = $_FILES["photos"]["name"][ $key ];
				$override_var["slides"]["type"] = $_FILES["photos"]["type"][ $key ];
				$override_var["slides"]["tmp_name"] = $_FILES["photos"]["tmp_name"][ $key ];
				$override_var["slides"]["error"] = $_FILES["photos"]["error"][ $key ];
				$override_var["slides"]["size"] = $_FILES["photos"]["size"][ $key ];

				$config["destination_folder"] = $user_upload_folder_relative_path;
				$config["post_var"] = "slides";
				$config["max_width"] = 1024;
				$config["max_height"] = 800;
				$config["resize_mode"] = "exact";
				$config["new_name"] = tools::url_title( $key );

				$image = tools::upload_image_from_post( $config, $override_var );

				//insert into database
				if($image){

					$image_description = $_POST["photo_descriptions"][$key];
					$sql = "REPLACE INTO order_images SET order_id = {$_SESSION["id_order"]}, image = '{$image}', `description` = '{$image_description}'";
					$query = mysql_query($sql);
				}

			}
		}

		if(!is_string($_SESSION['email'])){
			$_SESSION['email'] = $_SESSION['email']['email'];
		}

		/*if ($_SESSION['step'] > 4) {
			header("location:index.php?mod=form&step=".$_SESSION['step']);
			break;
		}
		else $_SESSION['step'] = 3;*/

		if ($_SESSION['email']=="") header("location:index.php");

		if ($_POST) {
			// Limpa arrays
			$_SESSION['items'] = array();
			$_SESSION['items_outros'] = array();
			$_SESSION['boxes'] = array();
			$_SESSION['days'] = array();
			$_SESSION['packed'] = array();
			$_SESSION['MoreItemsTotal'] = $_POST['MoreItemsTotal'];
			$_SESSION['MoreBoxesTotal'] = $_POST['MoreBoxesTotal'];

            $_SESSION['kit_id']    = $_POST['kit_id'];
            $_SESSION['kit_price'] = $_POST['kit_price'];
            $_SESSION['kit_name'] = $_POST['kit_name'];

            $_SESSION['discount_percent'] = $_POST['discount_percent'];

			foreach ($_POST as $key=>$value) {
				# Items
				if (preg_match("/item_([0-9]+)/", $key, $m)) {
					if ($value > 0) {
						//echo $m[1]." - ".$value."<br>";
						$_SESSION['items'][] = array("id"=>$m[1],"total"=>$value);
					}
				}

				# Outros Items
				if (preg_match("/name_outros_([0-9]+)/", $key, $m)) {
					if ($_POST["outros_".$m[1]] > 0) {
						//echo $_POST[$key]." - ".$_POST["outros_".$m[1]]." - ".$_POST["outros_altura_".$m[1]]."<br>";
						$_SESSION['items_outros'][] = array("name"=>$_POST[$key],"largura"=>$_POST["outros_largura_".$m[1]],"comprimento"=>$_POST["outros_comprimento_".$m[1]],"altura"=>$_POST["outros_altura_".$m[1]],"total"=>$_POST["outros_".$m[1]]);
					}
				}

				# Boxes
				if (preg_match("/psd_box_c_([0-9]+)/", $key, $m)) {
					if ($value > 0) {
						//echo $m[1]." - ".$value."<br>";
						$_SESSION['boxes'][] = array("id"=>$m[1],"total"=>$value);
					}
				}

				# Days
				if (preg_match("/psd_day_c_([0-9]+)/", $key, $m)) {
					if ($value > 0) {
						//echo $m[1]." - ".$value."<br>";
						$_SESSION['days'][] = array("id"=>$m[1],"total"=>$value);
					}
				}

				# Packed
				if (preg_match("/boxes_nome_([0-9]+)/", $key, $m)) {

					if ($_POST["boxes_quantidade_".$m[1]] > 0) {

						//echo $value." - ".$_POST["boxes_largura_".$m[1]]." - ".$_POST["boxes_comprimento_".$m[1]]." - ".$_POST["boxes_altura_".$m[1]]." - ".$_POST["boxes_quantidade_".$m[1]]."<br>";
						$_SESSION['packed'][] = array("nome"=>$value,"largura"=>$_POST["boxes_largura_".$m[1]],"comprimento"=>$_POST["boxes_comprimento_".$m[1]],"altura"=>$_POST["boxes_altura_".$m[1]],"total"=>$_POST["boxes_quantidade_".$m[1]]);
					}

				}
			}
		}

		tools::save(1);
	}


	function showStep4() {

		if ($_SESSION['id_order']=="" || $_SESSION['id_order']==0) header("location:index.php");

		if ($_POST) {
			$_SESSION['insurance_'] = $_POST['insurance'];
			$_SESSION['insurance_value'] = str_replace(".","",$_POST['insurance_value']);
			$_SESSION['insurance_value'] = str_replace(",",".",$_SESSION['insurance_value']);
			$_SESSION['insurance_final'] = str_replace(".","",$_POST['insurance_final']);
			$_SESSION['insurance_final'] = str_replace(",",".",$_SESSION['insurance_final']);
			//$_SESSION['lift'] = $_POST['lift'];
			//$_SESSION['lift_items'] = $_POST['lift_items'];
			$_SESSION['dep_emel'] = $_POST['dep_emel'];
			$_SESSION['dep_psp'] = $_POST['dep_psp'];
			$_SESSION['dest_emel'] = $_POST['dest_emel'];
			$_SESSION['dest_psp'] = $_POST['dest_psp'];

			$_SESSION['items_insurance_value'] = $_SESSION['packed_insurance_value'] = $_SESSION['boxes_insurance_value'] = "";
			foreach ($_POST as $key=>$value) {
				# Seguro: Ítens
				if (preg_match("/items_value_([0-9]+)/", $key, $m) || substr($key,0,7) == "outros_") {
					if ($value > 0) {
						$id = $m[1]==""?end(explode('outros_', $key)):$m[1];
						$_SESSION['items_insurance_value'][] = array("id"=>$id,"value"=>$value);
					}
				}

				# Seguro: Caixas
				if (preg_match("/boxes_value_([0-9]+)/", $key, $m)) {
					if ($value > 0) {
						//echo $m[1]." - ".$value."<br>";
						$_SESSION['boxes_insurance_value'][] = array("id"=>$m[1],"value"=>$value);
					}
				}

				# Seguro: Embalados
				if (preg_match("/packed_value_([a-zA-Z0-9]+)/", $key, $m)) {
					//echo $m[1]." - ".$value."<br>";
					if ($value > 0) {
						$_SESSION['packed_insurance_value'][] = array("nome"=>$m[1],"original"=>$_POST['packed_original_'.$m[1].'_value'],"value"=>$value);
					}
				}
			}
		}
		tools::save(2);

		$this->CalculoFinal();

		# Envia email
		//if ($_SESSION['email']!="") tools::enviaMail();

		//make sure there is a valid session order
		if ($_SESSION['id_order'] > 0) {
			$sql = "SELECT * FROM `orders` WHERE `id` = {$_SESSION['id_order']}";
			$query = mysql_query($sql);
			$data = mysql_fetch_array($query, MYSQL_ASSOC);
			$this->data = $data;
		}
		else header("Location: index.html");

	}


	function showStep5() {

		if ($_SESSION['id_order']=="" || $_SESSION['id_order']==0) header("location:index.php");
		if ($_POST) $_SESSION['comments'] = $_POST['comments'];
		//else header("location:index.php");
		$query = mysql_query("SELECT * FROM `orders` WHERE id = {$_SESSION['id_order']}");
		$total = mysql_fetch_array($query, MYSQL_ASSOC);

		//echo $_SESSION['id_order'];

		tools::save(3);

		//header
		//$url = "pdf_v5_area_send_email.php?id={$_SESSION['id_order']}&lang={$_SESSION['lang']}&output=S&type__={$type__}&payment={$payment}";
		//echo '<script type="text/javascript" src="'.base_url("assets/js/jquery-1.11.1.min.js").'"></script>';
		//echo '<script type="text/javascript">$("#response").load("'.$url.'");</script>';

	}


	function showStep6() {
		if (!$_SESSION['id_order']) header("location:index.php");

		//$this->out .= "step6";

		if ( FALSE ) {
			//$this->out .=  "1";
			$type__ = $_GET['type__'];
			$payment = $_GET['payment'];

			$query = mysql_query("SELECT * FROM `orders` WHERE id = {$_SESSION['id_order']}");
			$dados = mysql_fetch_array($query, MYSQL_ASSOC);

			if ($type__ == 1) {				# TB
				$_SESSION['method_payment'] = 2;
				$status = 1;
			}
			elseif ($type__ == 2) {			# Cheque
				$_SESSION['method_payment'] = 3;
				$status = 1;
			}

			elseif ($payment != "") {		# Paypal
				$_SESSION['method_payment'] = 1;
				if ($payment=='ok') {
					$status = 1;
					$_SESSION['payment'] = "yes";
				}
				else {
					$status = 1;
					$_SESSION['payment'] = "no";
				}
			}

			# Status passa de Pendente, 1, para o método de pagamento,
			# 7, Transferência Bancária
			# 8, Paypal Autorizado
			# 9, Paypal Não Autorizado
			# 10, Cheque
			$sql = "UPDATE `orders` SET `status` = '{$status}', method_payment = {$_SESSION['method_payment']}
			WHERE id = {$_SESSION['id_order']}";
			mysql_query($sql);

		}
		else {

			//$this->out .=  "2";

			$this->showHeader();

			$this->out .= "
			<div class='container'>
				<table class='FormOrc' width='100%' cellspacing='0' cellpadding='5'><tr><td align='center' class='FormOrc'>
					<tr>
						<td>
							<table width='80%' cellspacing='0' cellpadding='5' class='Order'>
								<tr>
									<td align='center'>
										<div class='LinkPrint'>
											<a class='LinkPrint' href='javascript: void(0);' onclick='javascript:window.print();'>".IMPRIMIR."</a>
											<a class='LinkExit' href='javascript: void(0);' onclick='javascript:location.reload(true);'>".SAIR."</a>
										</div>
									</td>
								</tr>
								<tr>
									<td>";

						//$this->out .= var_export($_SESSION["method_payment"], true);

										if ( TRUE ) {
											$message_1 = str_replace("[nome]",stripslashes(utf8_decode($_SESSION['name'])),tools::Message(2));
											$message_2 = str_replace("[total_encomenda]",$_SESSION['total'],$message_1);
											$message_3 = str_replace("[iva]",$_SESSION['iva'],$message_2);
											$message_4 = str_replace("[total_com_iva]",$_SESSION['total_iva'],$message_3);
											$message = str_replace("[total_reserva_online]",$_SESSION['total_reserva_online'],$message_4);

										}
										elseif ($_SESSION['method_payment'] == 1) {
											if ($_SESSION['payment'] == "ok") {
												$sql = "SELECT `body_{$_SESSION['lang']}` `body` FROM `emails` WHERE id = 7";
												$msg = mysql_fetch_array(mysql_query($sql), MYSQL_ASSOC);
												$message = str_replace("[nome]",$_SESSION['name'],$msg['body']);
											}
											else {
												$sql = "SELECT `body_{$_SESSION['lang']}` `body` FROM `emails` WHERE id = 8";
												$msg = mysql_fetch_array(mysql_query($sql), MYSQL_ASSOC);
												$message_1 = str_replace("[nome]",stripslashes(utf8_decode($_SESSION['name'])),$msg['body']);
												$message_2 = str_replace("[total_encomenda]",$_SESSION['total'],$message_1);
												$message_3 = str_replace("[iva]",$_SESSION['iva'],$message_2);
												$message_4 = str_replace("[total_com_iva]",$_SESSION['total_iva'],$message_3);
												$message = str_replace("[total_reserva_online]",$_SESSION['total_reserva_online'],$message_4);
											}
										}
						//$this->out .= 'aaaa'.$sql.'aaaa';

										$this->out .= "<p>$message</p>";
										$this->out .= "
									</td>
								</tr>
								<tr>
									<td align='center'>
										<div class='LinkPrint'>
											<a class='LinkPrint' href='javascript: void(0);' onclick='javascript:window.print();'>".IMPRIMIR."</a>
											<a class='btn btn-default LinkExit' href='javascript: void(0);' onclick='javascript:location.reload(true);'>".SAIR."</a>
										</div>
									</td>
								</tr>
							</table>
						</td>
					</tr>
				</table>
			</div>";
			$this->showFooter();

			//error_reporting( E_ALL );
			$visited_date = ($_SESSION["visited_date"]);
			$visited_id = ($_SESSION["visited_id"]);
			$visited_ip = ($_SESSION["visited_ip"]);

			@session_unset();
			session_destroy();
			@session_start();
			$_SESSION["visited_date"] = $visited_date;
			$_SESSION["visited_id"] = $visited_id;
			$_SESSION["visited_ip"] = $visited_ip;
		}
	}

	function CalculoFinal() {
		$sql = "SELECT * FROM `orders` WHERE id = {$_SESSION['id_order']}";
		$data = mysql_fetch_array(mysql_query($sql), MYSQL_ASSOC);

		$_SESSION['autorized_order'] = 0;

		# Status
		# 2, Aprovado
		# 3, Em progresso
		# 4, Entregue
		$_SESSION['limit_reached'] = 0;

		//echo $_SESSION["maxorders"];
		//echo 'aaa';

		if ($_SESSION['maxorders'] > 0) {
			$sql = "SELECT count(id) total
			FROM orders
			WHERE zone = {$data['zone']}
			AND date_delivery = '{$data['date_delivery']}'
			AND status IN (2,3,4)";
			$query = mysql_query($sql);
			$total_orders = mysql_fetch_array(mysql_query($sql), MYSQL_ASSOC);
			if (intval($total_orders['total']) >= intval($_SESSION['maxorders'])) {
				$_SESSION['limit_reached'] = 1;
			}
		}

		if ($_SESSION['origem_aut'] == 0 || $_SESSION['destino_aut'] == 0 || $_SESSION['limit_reached'] == 1) $_SESSION['autorized_order'] = 1;

		$boxes_price = $transport_price = $total_insurance = 0.00;
		$m3 = 0;

		# ÍTENS
		$sql = "SELECT SUM(m3*o.total) total
		FROM orders_items o INNER JOIN items i
		ON i.id = o.id_items
		WHERE `id_orders` = {$_SESSION['id_order']}";
		$total_items = mysql_fetch_array(mysql_query($sql), MYSQL_ASSOC);
		$m3 = $total_items['total'];

		# OUTROS ÍTENS
		$sql = "SELECT largura, comprimento, altura, total
		FROM orders_items
		WHERE id_orders = {$_SESSION['id_order']}
		AND outros <> ''";
		$query_ = mysql_query($sql);
		$xx_ = mysql_num_rows($query_);
		$outros_price = 0;
		for ($i=0;$i<$xx_;$i++) {
			$s = mysql_fetch_object($query_);
			$outros_price += (intval($s->largura)/100) * (intval($s->comprimento)/100) * (intval($s->altura)/100) * $s->total;
			//echo (intval($s->largura)/100).' * '.(intval($s->comprimento)/100).' * '.(intval($s->altura)/100).' * '.$s->total."<br>";
		}
		$total_outros = round($outros_price,2);

		$m3_outros = $total_outros > 0?$total_outros:0.00;

		# CAIXAS
		$sql = "SELECT b.id, b.price, b.m3, o.total, o.days, (SELECT {$_SESSION['type']} price FROM `prices` WHERE id = 1) preco_m3
		FROM orders_boxes o INNER JOIN boxes b
		ON b.id = o.id_boxes
		WHERE `id_orders` = {$_SESSION['id_order']}";
		$query_ = mysql_query($sql);
		$xx_ = mysql_num_rows($query_);
		$boxes_price = 0;
		$cubicagem = 0;
		$total_boxes = 0;

		for ($i=0;$i<$xx_;$i++) {
			$s = mysql_fetch_object($query_);
			$s_total = ( $s->id == 3 || $s->id == 8 ) ? ($s->total) : $s->total; # Float se 8 e 3

			if ( $s->id >= 9 && $s->id <= 16 ) {
				$total_boxes += ($s_total * $s->price);
			}

			# HABITAÇÃO | 1, Caixa em cartão canelado (40cm x 40cm x 40cm)
			if ($s->id == 1 ) {
				$caixa_1_m3 = ($s_total * $s->price) + (($s_total * $s->m3) * $s->preco_m3);
				//echo "1. m3 = (".$s_total.' * '.$s->price.') + (('.$s_total.' * '.$s->m3.') * '.$s->preco_m3.') = '.$caixa_1_m3.'<br>';
				$caixa_1 = ($s_total * $s->price);
				//echo '('.$s_total.' * '.$s->price.') = '.$caixa_1.'<br><br>';
				$cubicagem = ($caixa_1_m3 - $caixa_1);
			}

			# HABITAÇÃO | 2, Caixa para fatos (110cm x 60cm x 60cm)
			if ($s->id == 2) {
				$caixa_2_m3 = ($s_total * $s->price) + (($s_total * $s->m3) * $s->preco_m3);
				//echo "2. m3 = (".$s_total.' * '.$s->price.') + (('.$s_total.' * '.$s->m3.') * '.$s->preco_m3.') = '.$caixa_2_m3.'<br>';
				$caixa_2 = ($s_total * $s->price);
				//echo '('.$s_total.' * '.$s->price.') = '.$caixa_2.'<br><br>';
				$cubicagem += ($caixa_2_m3 - $caixa_2);
			}

			# EMPRESA | 3, Caixa em plástico cityMover - Para aluguer semanal, preços já com entrega e recolha . Minímo de 10 caixas.
			if ($s->id == 3) {
				$caixa_e_m3 = (($s_total * $s->price  * $s->days) + (($s_total * $s->m3) * $s->preco_m3));
				$caixa_e = ($s_total * $s->price  * $s->days);
				$cubicagem += ($caixa_e_m3 - $caixa_e);
			}

			# EMPRESA | 7, Rolo de protecção para os bens embalados. Um rolo dá +- para um apartamento T2 normal.
			if ($s->id == 7) $caixa_e7 = ($s_total * $s->price);

			# HABITAÇÃO | 4, Fita auto adesiva - 1 rolo para cada 10 caixas
			if ($s->id == 4) { $caixa_3 = ($s_total * $s->price);
				//echo "4. (".$s_total.' * '.$s->price.") = ".$caixa_3."<br>";
			}

			# HABITAÇÃO | 8, Rolo de protecção para os bens embalados. Um rolo dá +- para um apartamento T2 normal
			if ($s->id == 8) {$caixa_4 = ($s_total * $s->price);
				//echo "7. (".$s_total.' * '.$s->price.") / 0.5 = ".$caixa_4."<br>";
			}
		}

		if ($caixa_e != "" || $caixa_e7 != "") $total_boxes += round($caixa_e + $caixa_e7,2);
		else $total_boxes += round($caixa_1 + $caixa_2 + $caixa_3 + $caixa_4,2);

		$boxes_price = $total_boxes > 0?$total_boxes:0.00;
		$total_boxes_cm3 = $total_boxes > 0?($total_boxes + $cubicagem):0.00;

		//echo "sm3 = ".$boxes_price."<br>cm3 = ".$total_boxes_cm3;break;

		# CAIXAS EMBALADAS
		$sql = "SELECT largura, comprimento, altura, total, (SELECT home price FROM `prices` WHERE id = 1) preco_m3
		FROM orders_packed
		WHERE id_orders = {$_SESSION['id_order']}";
		$query = mysql_query($sql);
		$xx = mysql_num_rows($query);
		$packed_price = 0;
		for ($i=0;$i<$xx;$i++) {
			$r = mysql_fetch_object($query);
			$packed_price += (intval($r->largura)/100) * (intval($r->comprimento)/100) * (intval($r->altura)/100) * $r->total;
		}
		$total_packed = round($packed_price * $r->preco_m3,2);
		$packed_price = $total_packed > 0?$total_packed:0.00;

		# METRO CÚBICO
		$sql = "SELECT {$_SESSION['type']} price FROM `prices` WHERE id = 1";
		$price = mysql_fetch_array(mysql_query($sql), MYSQL_ASSOC);
		$m3_price = $price['price']>0?$price['price']:0.00;

		# Cálculo do metro cúbico
		$transport_price = round(($m3 + $m3_outros) * $m3_price,2);
		//echo $transport_price;break;
		# $boxes_price =  Já informado L1655
		# $packed_price =  Já informado L1666

		# PREÇO MÍNIMO (Definido no BO > Definições > Preços)
		$sql = "SELECT {$_SESSION['type']} price FROM `prices` WHERE id = 3";
		$price = mysql_fetch_array(mysql_query($sql), MYSQL_ASSOC);
		$minimum_price = $price['price']>0?$price['price']:0.00;

		# TOTAL MUDANÇA (total itens + outros + cubicagem boxes + itens embalados)
		$total_mudanca = $transport_price + $cubicagem + $packed_price;

		# SEGURO: É CALCULADO NO PASSO 3 L676
		$total_insurance = $_SESSION['insurance_final'];

		# TOTAL ((total itens + outros + cubicagem boxes + itens embalados) + seguro)
		$total = $total_mudanca + $boxes_price + $total_insurance;

		//die($discount." : ".$comission);

		# Estacionamento
		$total_parking = $_SESSION['total_parking'] = "";
		if ($_SESSION['dep_emel']==1) $total_parking += 110.00;
		if ($_SESSION['dest_emel']==1) $total_parking += 110.00;

		# Agente PSP
		if ($_SESSION['dep_psp']==1) $total_psp += 75;
		if ($_SESSION['dest_psp']==1) $total_psp += 75;

		$total += $total_parking + $total_psp;

		if($_SESSION['id_user']){
			$user = $_SESSION['id_user'];
			$sql = "SELECT discount,commision from users where id = {$_SESSION['id_user']}";
			$user = mysql_query($sql);
			$partners = mysql_fetch_array($user,MYSQL_ASSOC);
		}

		# discontos
		$discount = $total*($partners['discount']/100) ;

		#comissao
		$comission = ($total-$discount)*($partners['commision']/100) ;

		$total = $total - $discount - $comission;

		if ($total < $minimum_price) {
			$_SESSION['autorized_order'] = 1;
			$status = 14;
		}
		/*
		echo "TOTAL ITEMS ".$m3."<br>";
		echo "TOTAL OUTROS ".$m3_outros."<br>";

		echo "TOTAL ITEMS+OUTROS*PRECO M3 ".$transport_price."<br>";

		echo "TOTAL CAIXAS ".$boxes_price."<br>";
		echo "TOTAL EMBALADOS ".$packed_price."<br>";
		echo "TOTAL MUDANCA ".$total_mudanca."<br>";
		echo "TOTAL insurance ".$total_insurance."<br>";
		echo "TOTAL".$total."<br>";
		break;*/

		# TOTAL + % IVA (Definido no BO > Definições > Preços)
		$sql = "SELECT {$_SESSION['type']} iva FROM `prices` WHERE id = 4";
		$perc = mysql_fetch_array(mysql_query($sql), MYSQL_ASSOC);
		$iva = $perc['iva']>0?$perc['iva']:0.00;
		$iva = (($total*$iva)/100);

		$total_iva = $total + $iva;

		# % SINAL RESERVA ONLINE (Definido no BO > Definições > Preços)
		$sql = "SELECT {$_SESSION['type']} reserva_online FROM `prices` WHERE id = 5";
		$perc_ = mysql_fetch_array(mysql_query($sql), MYSQL_ASSOC);
		$total_reserva_online = $perc_['reserva_online']>0?$perc_['reserva_online']:0.00;
		$total_reserva_online = (($total_iva*$total_reserva_online)/100);

		# Status passa de Tentativa, 6, para:
		# 1, Pendente = Pagamento ainda não efectuado
		# 11, Atingiu o limite máximo de encomendas para o dia
		# 12, Zona inválida

		if ($_SESSION['autorized_order'] == 0 && $_SESSION['origem_aut'] == 1 && $_SESSION['destino_aut'] == 1) $status = 1;
		else {
			if ($_SESSION['limit_reached'] == 1) $status = 11;
			if ($_SESSION['origem_aut'] == 0 ) $status = strlen($status)>0?$status.",1":1;
			if ($_SESSION['destino_aut'] == 0) $status = strlen($status)>0?$status.",1":1;
		}

		$sql = "UPDATE `orders` SET
		`total_mudanca` = '{$total_mudanca}',
		`total_boxes` = '{$total_boxes_cm3}',
		`total_boxes_sm3` = '{$boxes_price}',
		`total_boxes_m3` = '{$cubicagem}',
		`total_items` = '{$transport_price}',
		`total_packed` = '{$packed_price}',
		`total_insurance` = '{$total_insurance}',
		`total_parking` = '{$total_parking}',
		`total_psp` = '{$total_psp}',
		`iva` = '{$iva}',
		`total_reserva_online` = '{$total_reserva_online}',
		`total` = '{$total}',
		`status` = '{$status}',
		`total_desconto` = '{$discount}',
		`total_comissao` = '{$comission}'
		WHERE id = {$_SESSION['id_order']}";
		mysql_query($sql);

		$_SESSION['transport_price'] = number_format($transport_price,2);
		$_SESSION['boxes_price'] = number_format($boxes_price,2);
		$_SESSION['packed_price'] = number_format($packed_price,2);
		$_SESSION['total_insurance'] = $total_insurance!=""?number_format($total_insurance,2):"0,00";
		$_SESSION['insurance_final'] = $_SESSION['insurance_final']!=""?number_format($_SESSION['insurance_final'],2):"0,00";
		$_SESSION['iva'] = number_format($iva,2);
		$_SESSION['total'] = number_format($total,2);
		$_SESSION['total_sem_iva'] = number_format($total-$iva,2);
		$_SESSION['total_reserva_online'] = number_format($total_reserva_online,2);
		$_SESSION['total_mudanca'] = number_format($total_mudanca,2);
		$_SESSION['total_parking'] = $total_parking!=""?number_format($total_parking,2):"0,00";
		$_SESSION['total_psp'] = $total_psp!=""?number_format($total_psp,2):"0,00";
		$_SESSION['total_iva'] = $total_iva!=""?number_format($total_iva,2):"0,00";
		$_SESSION['percentual_iva'] = number_format($perc['iva'],0);
		$_SESSION['percentual_reserva_online'] = number_format($perc_['reserva_online'],0);
	}
}

?>