<?php

require "database.php";

class core {

	public static $view;
	public $mod;
	public $mod_data; //info da página actual
	public $lang;
	public $meta;
	
	//on load core
	function __construct() {
//        error_reporting(E_ALL);
		error_reporting(E_ALL ^E_STRICT ^E_NOTICE ^E_DEPRECATED ^E_WARNING);

//        ini_set("display_errors", 1);
		$display_errors = (in_array($_SERVER['HTTP_HOST'], array('localhost'))) ? 1 : 1;
		ini_set("display_errors", $display_errors);

        $this->load_database();

		session_start();

		self::load_general_functions(); //base_url, base_path, etc
		$this->load_tools();
		$this->set_lang(); //sets up defines based on lang
		$this->set_mod(); //defines module (including default module)
		$this->load_module(); //loads module and handles view load

		$this->load_dummy_data(); //delete in the end

		$this->get_referer();

		//definir os headers
		header("Content-type: text/html; charset=utf-8");
	}

	function load_dummy_data(){
		$_SESSION['type'] = "home"; //or "office"
		//$_SESSION['id_order'] = 6211;
	}

	function load_tools(){
		require_once(base_path("includes/tools.php"));
	}

	function get_referer(){

		//referer - saves first access
		if(!isset($_SESSION["origin"])){
			$_SESSION["origin"] = ($_SERVER["HTTP_REFERER"]) ? $_SERVER["HTTP_REFERER"] : "Desconhecido / Directo";
		}

	}

	function load_database(){
		return true;
		$database = new Database();
		$this->database = $database;
	}

	function set_mod(){
		$this->mod = (isset($_GET["mod"])) ? $_GET["mod"] : "login";
	}

	public function getClientIP()
	{
		if (getenv('HTTP_CLIENT_IP'))
			$clientIP = getenv('HTTP_CLIENT_IP');
		elseif (getenv('HTTP_X_FORWARDED_FOR'))
			$clientIP = getenv('HTTP_X_FORWARDED_FOR');
		elseif (getenv('HTTP_X_FORWARDED'))
			$clientIP = getenv('HTTP_X_FORWARDED');
		elseif (getenv('HTTP_FORWARDED_FOR'))
			$clientIP = getenv('HTTP_FORWARDED_FOR');
		elseif (getenv('HTTP_FORWARDED'))
			$clientIP = getenv('HTTP_FORWARDED');
		elseif (getenv('REMOTE_ADDR'))
			$clientIP = getenv('REMOTE_ADDR');
		else
			$clientIP = 'UNKNOWN';

		if (strpos($clientIP, ',')) {
			$ip = explode(',', $clientIP);
			$clientIP = $ip[0];
		}

		return $clientIP;
	}

	function set_lang()
	{
		if ($lang = filter_input(INPUT_GET, 'lang')) {
			$_SESSION['lang_fe'] = $lang;
		} elseif (empty($_SESSION['lang_fe'])) {
			$_SESSION['lang_fe'] = substr($_SERVER['HTTP_ACCEPT_LANGUAGE'], 0, 2);
		}

		$allowed = array(
			"en",
			"pt"
			);
		if (!$_SESSION['lang_fe'] || !in_array($_SESSION['lang_fe'], $allowed)) {
			$_SESSION['lang_fe'] = 'en';
		}

		$this->lang = filter_var($_SESSION['lang_fe'], FILTER_SANITIZE_STRING);

        //incluir o ficheiro de linguagem
		include_once("includes/lang/" . $this->lang . ".php");
		include_once("includes/lang/old/" . $this->lang . ".php");
	}

	static function lang(){
		return $_SESSION["lang_fe"];
	}

	function load_module(){

		//carregar o ficheiro 
		$file = base_path("includes/modules/".$this->mod.".mod.php");


		//nem sempre há necessidade de carregar o módulo, pode-se excluir caso haja necessidade
		if(true){
			if(file_exists($file)){

				//incluir o ficheiro onde está definido
				require_once($file);

				//instanciar a classe e passa-la para o core
				$this->mod_data = new $this->mod;
			}
			
		}
		
	}

	//define functions de utilização transversal (root, friendly-url, etc)
	function load_general_functions(){

		function calculate_percentage($original_price, $promo_price){
			$percent_discount = 100 - ($promo_price * 100 / $original_price);
			return number_format($percent_discount,0);
		}



		function current_url_clean(){
			$current_url = current_url();
			$url = substr($current_url, 0, strpos($current_url, "?"));
			return $url;
		}

		function current_url() {
			$pageURL = 'http';
			if ($_SERVER["HTTPS"] == "on") {$pageURL .= "s";}
			$pageURL .= "://";
			if ($_SERVER["SERVER_PORT"] != "80") {
				$pageURL .= $_SERVER["SERVER_NAME"].":".$_SERVER["SERVER_PORT"].$_SERVER["REQUEST_URI"];
			} else {
				$pageURL .= $_SERVER["SERVER_NAME"].$_SERVER["REQUEST_URI"];
			}
			return $pageURL;
		}

		function translate($call_word){

//			if(core::lang() == "en") return $call_word; //bypass translate attempts in english

			$defined = defined($call_word);

            //o define não existe no idioma actual
            /*if(!$defined){
                //retornar o valor em PT
                require_once(base_path("includes/lang/pt.php")); //não redefine os restantes defines porque defines são CONSTANTS, pelo que só define os que estão "em falta neste idioma"
            }*/

            $final_word = constant($call_word);
            if(!empty($final_word))
            	return $final_word;
            else{

            	$content = file_get_contents(base_path("translation.txt"));

                if (false === strpos($content, '"' . $call_word . '"')) { // eveitar duplicados - deixa o processo de traduzir mais rápido
                    //debug - escreve num txt as traduções em falta
                	$handle = fopen(base_path("translation.txt"), "a+");
                	$string = "define(\"" . $call_word . "\", \"MISSING TRANSLATION\");\n";
                	$write  = fwrite($handle, $string);
                	$close  = fclose($handle);
                }

                return $call_word; //returns the input
            }
        }

        function translate_url($call_word){

			if(core::lang() == "en") return $call_word; //bypass translate attempts in english

			$defined = defined($call_word);

			//o define não existe no idioma actual
			if(!$defined){

				//retornar o valor em PT
				require_once(base_path("includes/lang/pt-urls.php")); //não redefine os restantes defines porque defines são CONSTANTS, pelo que só define os que estão "em falta neste idioma"
				$output_url = constant($call_word);

				if(!empty($output_url))
					return $output_url;
				else{

					//debug - escreve num txt as traduções em falta
					$handle = fopen(base_path("translation.txt"), "a+");
					$string = "define(\"" . $call_word . "\", \"MISSING URL TRANSLATION\");\n";
					$write = fwrite($handle, $string);
					$close = fclose($handle);

					return $output_url; //returns the input
				}		
			}

			else{
				$output_url = constant($call_word);
			}

			return $output_url;
			
			
		}

		function var_bump($var){
			echo '<div style="z-index: 10000000; position: fixed; right:10px; top:10px; padding:15px; background: rgba(0,0,0,0.8); color:#fff; width: 400px; font-size: 13px; line-height: 19px; font-family: Consolas; line-height: 15px; border-radius: 3px; height: 300px; overflow-y: scroll;">'; var_dump($var); echo '</div>';
		}

		function redirect($uri = '', $method = 'location', $http_response_code = 302)
		{
			if ( ! preg_match('#^https?://#i', $uri))
			{
				$uri = base_url($uri);
			}

			switch($method)
			{
				case 'refresh'	: header("Refresh:0;url=".$uri);
				break;
				default			: header("Location: ".$uri, TRUE, $http_response_code);
				break;
			}
			exit;
		}

		function base_url($url = false){
			if ($_SERVER["HTTP_HOST"] == "localhost") {
				$host = "http://localhost/worten";
			} elseif ($_SERVER["HTTP_HOST"] == "worten.local") {
				$host = "http://worten.local";
			} else {
				$host = "http://" . $_SERVER["HTTP_HOST"] . "/worten";
			}

			return $host."/".$url;
		}

		function base_path($url = false)
		{
			return BASE_PATH . DIRECTORY_SEPARATOR . $url;
		}

        function get_sql($sql, $array_return = "object"){
            if(!empty($sql)){
                $query = mysql_query($sql) or die(mysql_error());
                if($query){
                    if (true) {
                        if($array_return == "object"){
                            while ($row = mysql_fetch_object($query)) {
                                $output[] = $row;
                            }
                        }
                        else{
                            while ($row = mysql_fetch_assoc($query)) {
                                $output[] = $row;
                            }
                        }
                    }
                    return $output;
                }
                return false;
            }
            return false;
        }

	}

		function load_view($file, $data = false){

			if(file_exists($file)){
				if($data){
					//criar um object temporario
					$temp_object = new stdClass();
					foreach ($data as $key => $value) {
						$temp_object->$key = $value;
					}
					//passar o core na própria data - deve haver melhor forma de fazer isto. não é grave porque php passa por referência
					//$temp_object->core = $this; não necessário para este caso
					//limpar $data colocando os valores do object temporario
					unset($data);
					$data = $temp_object;
				}

				ob_start();
				include($file);
				$output = ob_get_clean();
				return $output;
			}

			else echo "File <b>".$file."</b> not found";

		}

	}


	?>