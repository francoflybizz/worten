$(document).ready(function(){

    $('[data-toggle="popover"]').popover();

    $('.datepicker').datepicker({
        language: 'pt',
        startDate: "+7d"
    });



	//notificacoes
	$("#notify-messages li").each(function(){

		console.log($(this).html());
		console.log($(this).data("type"));

		new Noty({
			text: $(this).html(),
			type: $(this).data("type"),
			theme: "sunset",
			timeout : 2500
		}).show();
	});


	//dynamic division tables
	$(".house-division").click(function(){

		//trigger the click on the div
		$(".house-division").removeClass("active");
		$(this).addClass("active");

		//what is the target?
		var target = $(this).data("target");

		//make sure other targets are not visible
		$(".house-division-items>div").addClass("d-none");

		//make sure target is visible
		$("#" + target).removeClass("d-none")

	});

	//click handler on kits
	//$(".kit").click(function(){
	//	$(".kit").removeClass("active");
	//	$(this).addClass("active");
	//});

    $(".btn-add-image").off().click(function(){
        $("ul.image-preview-list li").last()
			.find("input[type=file]")
			.click(); //abre o popup e cria um handler NESTE input file

        $("input.file-trigger").off()
			.last()
			.change(function(){
				//$("ul.image-preview-list li").last()
				//	.find("button.hidden")
				//	.fadeOut()
				//	.removeClass("hidden")
				//	.fadeIn(); //show the remove button
                //
				//$("ul.image-preview-list li").last()
				//	.find("textarea.hidden")
				//	.fadeOut()
				//	.removeClass("hidden")
				//	.fadeIn(); //show the textarea

				preview_file($(this));
				bind_delete();
			});
    });

    function bind_delete(){
        $(".btn-remove").click(function(){
            $(this).parent().fadeOut(600, function(){
                $(this).remove();
            });
        });
    }

    function preview_file(selector) {

        $('.image-preview-list').removeClass('d-none');

        var preview = selector.parent().find(".img-preview")[0]; //onde é feito o preview?
        var file    = selector[0].files[0];
        var reader  = new FileReader();

        reader.addEventListener("load", function () {
            if (reader.result) {
                var new_item = $("ul.image-preview-list li").eq(0).clone();
                new_item.find("img").attr("src", "");
                new_item.appendTo("ul.image-preview-list");

                preview.src = reader.result;

                //smooth transition
                $(preview).transition({
                    y: 10,
                    opacity: 1
                });
            }
        }, false);

        if (file) {
            reader.readAsDataURL(file);
        }
    }

});