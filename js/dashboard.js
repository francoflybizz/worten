function deg2rad (angle) {
    //  discuss at: http://locutus.io/php/deg2rad/
    // original by: Enrique Gonzalez
    // improved by: Thomas Grainger (http://graingert.co.uk)
    //   example 1: deg2rad(45)
    //   returns 1: 0.7853981633974483
    return angle * 0.017453292519943295 // (angle / 180) * Math.PI;
}

window.addEventListener('DOMContentLoaded', function () {
    $('.datepicker').datepicker({
        language: 'pt'
    });

    //var longitude = -9.136278;
    //var latitude = 38.734936;

    var longitude = -9.132655;
    var latitude = 38.746843;

    var map = new google.maps.Map(document.getElementById('map-heat'), {
        center: new google.maps.LatLng(latitude, longitude),
        zoom: 13,
        mapTypeId: 'satellite',
        maxIntensity: 3
    });

    var heatmapData = [];

    var points = [

    ];
    _.each(_.range(25), function () {
        points.push([
           '38.' + _.random(737496, 753704),
           '-9.' + _.random(121530, 136278)
        ]);
    });

    //38.737496,-9.1564385
    //38.727997,-9.1486931
    for (var i = 0, len = points.length; i < len; i++) {
        //var latLngArr = getRandomPoint();

        heatmapData.push(new google.maps.LatLng(points[i][0], points[i][1]));
    }

    var heatmap = new google.maps.visualization.HeatmapLayer({
        data: heatmapData,
        dissipating: false,
        map: map
    });

    heatmap.set('radius', 0.001);

    console.log(heatmap);

    var months = ['Janeiro', 'Fevereiro', 'Março', 'Abril', 'Maio', 'Junho', 'Julho', 'Agosto', 'Setembro', 'Outubro', 'Novembro', 'Dezembro'];
    //var months = ['Jan', 'Fev', 'Mar', 'Abr', 'Mai', 'Jun', 'Jul', 'Ago', 'Set', 'Out', 'Nov', 'Dez'];

    var randomScalingFactor = function () {
        return Math.round(Math.random() * 100);
    };

    var ctx = null;

    //region m3
    var configM3 = {
        type: 'line',
        data: {
            labels: months,
            datasets: [{
                label: 'm³',
                backgroundColor: '#e74c3c',
                borderColor: '#e74c3c',
                data: [
                    randomScalingFactor(),
                    randomScalingFactor(),
                    randomScalingFactor(),
                    randomScalingFactor(),
                    randomScalingFactor(),
                    randomScalingFactor(),
                    randomScalingFactor(),
                    randomScalingFactor(),
                    randomScalingFactor(),
                    randomScalingFactor(),
                    randomScalingFactor(),
                    randomScalingFactor()
                ],
                fill: false
            }]
        },
        options: {
            responsive: true,
            tooltips: {
                mode: 'index',
                intersect: false
            },
            hover: {
                mode: 'nearest',
                intersect: true
            },
            scales: {
                xAxes: [{
                    display: true
                }],
                yAxes: [{
                    display: true
                }]
            }
        }
    };

    ctx = document.getElementById('canvas-volume-m3').getContext('2d');
    window.volumeChartM3 = new Chart(ctx, configM3);
    //endregion

    //region cash
    var configCash = {
        type: 'line',
        data: {
            labels: months,
            datasets: [{
                label: '€',
                backgroundColor: '#e74c3c',
                borderColor: '#e74c3c',
                data: [
                    randomScalingFactor(),
                    randomScalingFactor(),
                    randomScalingFactor(),
                    randomScalingFactor(),
                    randomScalingFactor(),
                    randomScalingFactor(),
                    randomScalingFactor(),
                    randomScalingFactor(),
                    randomScalingFactor(),
                    randomScalingFactor(),
                    randomScalingFactor(),
                    randomScalingFactor()
                ],
                fill: false
            }]
        },
        options: {
            responsive: true,
            tooltips: {
                mode: 'index',
                intersect: false
            },
            hover: {
                mode: 'nearest',
                intersect: true
            },
            scales: {
                xAxes: [{
                    display: true
                }],
                yAxes: [{
                    display: true
                }]
            }
        }
    };

    ctx = document.getElementById('canvas-volume-cash').getContext('2d');
    window.volumeChartCash = new Chart(ctx, configCash);
    //endregion

    $('#filtrar').click(function () {
        configM3.data.datasets.forEach(function (dataset) {
            dataset.data = dataset.data.map(function () {
                return randomScalingFactor();
            });
        });

        window.volumeChartM3.update();

        configCash.data.datasets.forEach(function (dataset) {
            dataset.data = dataset.data.map(function () {
                return randomScalingFactor();
            });
        });

        window.volumeChartCash.update();

        var totalMudancas = Math.round(Math.random() * 100),
            totalM3       = Math.round(Math.random() * 500),
            totalCash     = Math.round(Math.random() * 1000)
        ;

        $('#total-mudancas').html(totalMudancas + ' Mudança' + (totalMudancas > 1 ? 's' : ''));
        $('#total-m3').html(totalM3.toFixed(2) + 'm³');
        $('#total-cash').html(totalCash.toFixed(2) + '€');
    });
});
