var BASE_URL = $("input[name=base_url]").val();

$(document).ready(function(){

	//há 1 elemento de input inicial, activo a partir do click
	//$(".btn-add-image").click(function(){
    //
	//	$("ul.image-preview-list li").last().find("input[type=file]").click(); //abre o popup e cria um handler NESTE input file
	//
	//	$("input#file-trigger").change(function(){
	//		$("ul.image-preview-list li").last().find("button.hidden").fadeOut().removeClass("hidden").fadeIn(); //show the remove button
	//		$("ul.image-preview-list li").last().find("textarea.hidden").fadeOut().removeClass("hidden").fadeIn(); //show the textarea
	//		preview_file($(this));
	//		bind_delete();
	//	});
    //
	//})

});

function bind_delete(){
	$(".btn-remove").click(function(){
		$(this).parent().fadeOut(600, function(){
			$(this).remove();
		});
	});
}

function preview_file(selector) {

	var preview = selector.parent().find(".img-preview")[0]; //onde é feito o preview?
	var file    = selector[0].files[0];
	var reader  = new FileReader();

	reader.addEventListener("load", function () {

		var new_item = $("ul.image-preview-list li").eq(0).clone();
		new_item.find("img").attr("src", "");
		new_item.appendTo("ul.image-preview-list");

		preview.src = reader.result;

		//smooth transition
		$(preview).transition({
			y : 10,
			opacity: 1
		});

		console.log(selector);

		//fade in the textarea
		// var textarea = $(selector).parent().find("textarea");
		// console.log(textarea);
		// textarea.css({"opacity" : 0, "display": "block"});
		// textarea.transition({
		// 	y : 10,
		// 	opacity	: 1
		// })

	}, false);

	if (file) {
		reader.readAsDataURL(file);
	}
}

function psd_showErr(name) {
	var obj = document.getElementById("err_"+name);

	if (!obj) { 
		return;
	}

	new Noty({
		text: $(obj).html(),
		type : 'error'
	}
	).show();


	return;
	//obj.style.display = 'block';
}

function psd_showErr_step1(name) {
	var obj = document.getElementById(name);
	var obj_err = $('#err_'+name);
	if (!obj) { 
		return;
	}
	$('[name='+name+']').val(obj_err.html());
	$('[name='+name+']').css('color','red');
	//obj.val();
}
/*
function psd_showErr(name) {
	var obj = document.getElementById("err_"+name);
	if (!obj) { 
		return;
	}
	obj.style.display = 'block';
}

*/

function psd_hideErr(name) {
	var obj = document.getElementById("err_"+name);
	if (!obj) { 
		return;
	}
	obj.style.display = 'none';
}

function psd_showErr_step1(name) {
	var obj = document.getElementById(name);
	var obj_err = $('#err_'+name);
	if (!obj) { 
		return;
	}
	$('[name='+name+']').val(obj_err.html());
	$('[name='+name+']').css('color','red');
	//obj.val();
}

function psd_hideErr_step1(name) {
	var obj = document.getElementById("err_"+name);
	if (!obj) { 
		return;
	}
	obj.style.display = 'none';
}

function psd_showPayOption(option) {
	var paypal = document.getElementById("paypal");
	var bank = document.getElementById("bank");
//	var check = document.getElementById("check");

paypal.style.display = 'none';
bank.style.display = 'none';
//	check.style.display = 'none';

switch (option) {
	case "paypal":
	paypal.style.display = 'block';
	break;
	default:
	bank.style.display = 'block';
	break;
}
}

// Autor: Gabriel Fróes - www.codigofonte.com.br
// http://codigofonte.uol.com.br/codigo/js-dhtml/validacao/mascara-de-moeda
function MascaraMoeda(objTextBox, SeparadorMilesimo, SeparadorDecimal, e) {
	var sep = 0;
	var key = '';
	var len = 0;
	var len2 = 0;
	var aux = '';
	var aux2 = '';
	var i = 0;
	var j = 0;
	var strCheck = '0123456789';
	var whichCode = (window.Event) ? e.which : e.keyCode;
	if (whichCode == 13) { return true; }
	var t = new String(objTextBox.value);

	if (whichCode == 8) {
		objTextBox.value = t.substring(0, t.length-1);
	}

	key = String.fromCharCode(whichCode); // Valor para o código da Chave

	if (strCheck.indexOf(key) == -1 && whichCode != 9) { return false; } // Chave inválida // wichCode = 9, para funcionar a tabulação
	
	len = objTextBox.value.length;
	for(i = 0; i < len; i++) {
		if ((objTextBox.value.charAt(i) != '0') && (objTextBox.value.charAt(i) != SeparadorDecimal)) break;	
	}
	
	var aux = '';
	for(i = 0; i < len; i++)
		if (strCheck.indexOf(objTextBox.value.charAt(i))!=-1) aux += objTextBox.value.charAt(i);
	aux += key;

	len = aux.length;
	if (len == 0) objTextBox.value = '';
	if (len == 1) objTextBox.value = '0'+ SeparadorDecimal + '0' + aux;
	if (len == 2) objTextBox.value = '0'+ SeparadorDecimal + aux;
	if (len > 2) {
		aux2 = '';
		for (j = 0, i = len - 3; i >= 0; i--) {
			if (j == 3) {
				aux2 += SeparadorMilesimo;
				j = 0;
			}
			aux2 += aux.charAt(i);
			j++;
		}
		objTextBox.value = '';
		len2 = aux2.length;
		for (i = len2 - 1; i >= 0; i--)
			objTextBox.value += aux2.charAt(i);
		objTextBox.value += SeparadorDecimal + aux.substr(len - 2, len);
	}
	return false;
}

// http://forum.imasters.uol.com.br/index.php?/topic/143614-%23-mascara-de-cep-pronta-pra-voces-%23/
function MascaraCodigoPostal(e,src,mask) {
	if(window.event) { _TXT = e.keyCode; }
	else if(e.which) { _TXT = e.which; }

	if(_TXT > 47 && _TXT < 58) {
		var i = src.value.length; 
		var saida = mask.substring(0,1); 
		var texto = mask.substring(i);
		if (texto.substring(0,1) != saida) { 
			src.value += texto.substring(0,1); 
		} 
		return true; 
	} 
	else { 
		if (_TXT != 8) { return false; }
		else { return true; }
	}
}

function Validar(form,step) {
	var ret = true;


	if (step==0) {
		var regex_1 = /^([0-9]{4})-?([0-9]{3})$/;
		var regex_2 = /^([0-9]{4})$/;

		for (var i=0;i < form.length;i++) {
			if (!form[i].name) continue;
			var obj = form[i].name;
		}

		// http://newsourcemedia.com/blog/javascript-non-alphanumeric-characters-regex/
		var regex = /^[a-z' 'A-ZãÃáÁàÀêÊéÉíÍõÕóÓúÚçÇ]+$/;
		if(regex.test(form['originCity'].value)){
			psd_hideErr("originCity");
		} else {
			psd_showErr_step1("originCity");
			ret = false;
		}

		var regex = /^[a-z' 'A-ZãÃáÁàÀêÊéÉíÍõÕóÓúÚçÇ]+$/;
		if(regex.test(form['dest_city'].value)){
			psd_hideErr_step1("dest_city");
		} else {
			psd_showErr_step1("dest_city");
			ret = false;
		}

		/*
		*/
		if(form['data_entrega'].value != '') {
			psd_hideErr_step1("data_entrega");
		} else {
			psd_showErr_step1("data_entrega");
			ret = false;
		}
	}
	else if (step==1) {
		var regex_1 = /^([0-9]{4})-?([0-9]{3})$/;
		var regex_2 = /^([0-9]{4})$/;

		if((regex_1.test(form['dep_zip'].value) || regex_2.test(form['dep_zip'].value)) && form['dep_zip'].value != '' && form['dep_zip'].value!="0000" && form['dep_zip'].value!="0000-000") {
			psd_hideErr("dep_zip");	
		} else {
			psd_showErr("dep_zip");
			ret = false;
		}


		if((regex_1.test(form['dest_zip'].value) || regex_2.test(form['dest_zip'].value)) && form['dest_zip'].value != '' && form['dest_zip'].value!="0000" && form['dest_zip'].value!="0000-000") {
			psd_hideErr("dest_zip");
		} else {
			psd_showErr("dest_zip");
			ret = false;
			console.log("ret = false 285");
		}


		
		var checado_dep = false;
		var checado_dest = false;

		for (var i=0;i < form.length;i++) {
			if (!form[i].name) continue;
			var obj = form[i].name;
		}

		var phone = mobile = false;

		for (var i=0;i<form.length;i++) {
			if (!form[i].name) continue;
			var n = form[i].name;
			if (n == 'originCity'  || n == 'name' || n == 'address' || n == 'address_dest' ||  n == 'taxnumber') {
				if (form[i].value == "") {
					ret =  false;
					console.log("showing n for cicle " + i + " => " + n);
					console.log("----------");
					console.log(form[i]);
					psd_showErr(n);
				}
				else psd_hideErr(n);
			}
			if (n == 'phone') {
				if (form[i].value != "") phone = true;
			}
			if (n == 'mobile') {
				if (form[i].value != "") mobile = true;
			}
			if (n == 'email') {
				if (form[i].value.match("([^@]+)@([^\\.]+)\\.(.*)")) psd_hideErr(n);
				else {
					ret = false;
					console.log("ret = false 320");
					psd_showErr(n);
				}
			}
			if (n == 'dep_lift') {
				if (form[i].checked) checado_dep = true;
			}
			if (n == 'dest_lift') {
				if (form[i].checked) checado_dest = true;
			}
		}
		if (!checado_dep) {
			ret = false;
			console.log("ret = false 334");
			psd_showErr("dep_lift");
		}
		else psd_hideErr("dep_lift");
		if (!checado_dest) {
			ret = false;
			console.log("ret = false 340");
			psd_showErr("dest_lift");
		}
		else psd_hideErr("dest_lift");

		if (phone == false && mobile == false) {
			ret = false;
			console.log("ret = false 347");
			psd_showErr("phone");
		}
		else psd_hideErr("phone");
	}

	else if (step==2) {
		if ( document.getElementById("sem_volume").checked ) {
			var sem_volume = true;
		}else{
			var sem_volume = false;
		}


		var dml = document.forms['form'];
		var len = dml.elements.length;
		var i = 0;
		var validar_items = false;

		for(i=0; i<len; i++) {
			var elemento = dml.elements[i].name;
			if (String(elemento.substring(0,5))=='item_') {
				if (dml.elements[i].value!=0) validar_items = true;
			}

			if (String(elemento.substring(0,12))=='name_outros_') {
				var outros_nome = dml.elements[i].value;
				var outros_quantidade = dml.elements[i+1].value;
				var outros_largura = dml.elements[i+2].value;
				var outros_comprimento = dml.elements[i+3].value;
				var outros_altura = dml.elements[i+4].value;

				if (outros_nome!='' && (outros_quantidade!=0 && outros_quantidade!='') && (outros_largura!=0 && outros_largura!='') && (outros_comprimento!=0 && outros_comprimento!='') && (outros_altura!=0 && outros_altura!='')) var validar_outros = true;
				else if (outros_nome!='' || outros_quantidade!=0 || outros_largura!=0 || outros_comprimento!=0 || outros_altura!=0) var validar_outros = false;
			}

			// Se Empresas, validar as caixas e dias
			if (String(elemento.substring(0,8))=='psd_box_' && String(dml.elements[i+1].name.substring(0,8))=='psd_day_') {
				var psd_box = dml.elements[i].value;
				var psd_day = dml.elements[i+1].value;

				//if ((psd_box!=0 && psd_day!=0) || (psd_box==0 && psd_day==0)) var validar_boxes = true;
				// Dias maiores do que 7, 100722	
				//if ((psd_box == 0 && psd_day == 7) || (psd_box != 0 && psd_day >= 7)) var validar_boxes = true;
				
				
				if ( psd_box == 0 || (psd_box != 0 && psd_day >= 7) ) var validar_boxes = true;
				else var validar_boxes = false;
			}

			if (String(elemento.substring(0,11))=='boxes_nome_') {
				var boxes_nome = dml.elements[i].value;
				var boxes_largura = dml.elements[i+1].value;
				var boxes_comprimento = dml.elements[i+2].value;
				var boxes_altura = dml.elements[i+3].value;
				var boxes_quantidade = dml.elements[i+4].value;

				if (boxes_nome!='' && (boxes_quantidade!=0 && boxes_quantidade!='') && (boxes_largura!=0 && boxes_largura!='') && (boxes_comprimento!=0 && boxes_comprimento!='') && (boxes_altura!=0 && boxes_altura!='')) var validar_packed = true;
				else if (boxes_nome!='' || boxes_quantidade!=0 || boxes_largura!=0 || boxes_comprimento!=0 || boxes_altura!=0) var validar_packed = false;
			}
		}



		if (validar_outros!=undefined && validar_outros==false) {
			psd_hideErr("ItemsClient");
			psd_hideErr("PackedClient");
			psd_hideErr("BoxesClient");
			psd_showErr("OutrosClient");
			ret = false;
		}
		else if (validar_boxes!=undefined && validar_boxes==false) {
			psd_hideErr("ItemsClient");
			psd_hideErr("OutrosClient");
			psd_hideErr("PackedClient");
			psd_showErr("BoxesClient");
			ret = false;
		}
		else if (validar_packed!=undefined && validar_packed==false) {
			psd_hideErr("ItemsClient");
			psd_hideErr("OutrosClient");
			psd_hideErr("BoxesClient");
			psd_showErr("PackedClient");
			ret = false;
		}
		else if (validar_items==false && (validar_outros==undefined || validar_outros==false) && (validar_packed==undefined || validar_packed==false)) {
			psd_hideErr("OutrosClient");
			psd_hideErr("PackedClient");
			psd_hideErr("BoxesClient");
			psd_showErr("ItemsClient");
			ret = false;
		}
		else {
			psd_hideErr("ItemsClient");
			psd_hideErr("OutrosClient");
			psd_hideErr("BoxesClient");
			psd_hideErr("PackedClient");
			ret = true;
		}
		if ( sem_volume ) {
			psd_hideErr("ItemsClient");
			psd_hideErr("OutrosClient");
			psd_hideErr("BoxesClient");
			psd_hideErr("PackedClient");
			ret = true;
		};
	}
	// SEGURO
	else if (step==3) {
		var dml = document.forms['form'];
		var len = dml.elements.length;
		var i = 0;
		var validar_outros_seguro = false;
		var validar_items = true;
		var checado_seguro = false;
		var checado_elevador = false;
		var validar_outros_elevador = false;

		var terms = document.getElementsByName("terms_agree");

		if(terms[0].checked == false){
			//error
			Noty({
				text: "Para avançar, tem de confirmar estar a par das condições de condicionamentos temporários de trânsito.",
				type: "error"
			});
			ret = false;
		}
		else

			for(i=0; i<len; i++) {
				var elemento = dml.elements[i].name;

				if (elemento == 'insurance') {
					if (dml.elements[i].checked) {
						var checado_seguro = true;
						if (dml.elements[i].value == 'yes') validar_outros_seguro = true;
					}
				}

				if (String(elemento.substring(0,12))=='items_value_' || String(elemento.substring(0,12))=='boxes_value_' || String(elemento.substring(0,13))=='packed_value_') {
					if (dml.elements[i].value != "" && dml.elements[i].value != "0,00") validar_items = false;
				}

				if (document.getElementById('err_lift_items') != null) {
					if (elemento == 'lift') {
						if (dml.elements[i].checked) {
							checado_elevador = true;
							if (dml.elements[i].value == 'yes') { validar_outros_elevador = true; }
						}
					}
				}
			}

			if (checado_seguro == false) {
				psd_showErr("insurance");
				ret = false;
			}
			else {
				psd_hideErr("insurance");

				if (validar_outros_seguro == true && validar_items == true) {		
					psd_showErr("insurance_items");
					ret = false;
				}
				else {
					psd_hideErr("insurance_items");
				}
			}

			if (document.getElementById('err_lift_items') != null) {
				if (checado_elevador == false) {
					psd_showErr("lift");
					ret = false;
				}
				else {
					psd_hideErr("lift");

					if (validar_outros_elevador == true) {
						if (document.forms['form'].lift_items.value == "") {
							psd_showErr("lift_items");
							ret = false;
						}
						else psd_hideErr("lift_items");
					}
					else psd_hideErr("lift_items");
				}
			}

		}
		else if (step==4) {
			var dml = document.forms['form'];
			var len = dml.elements.length;
			var i = 0;
			var checado = false;
			var submeter = false;

			for(i=0; i<len; i++) {
				var elemento = dml.elements[i].name;

				if (elemento=='i_agree') {
					if (dml.elements[i].checked) {
						var checado = true;
						if (dml.elements[i].value == 'yes') var submeter = true;
					}
				}
			}

			if (checado == false) {
				psd_showErr("i_agree");
				ret = false;
			}
			else {
				psd_hideErr("i_agree");		
				if (submeter == true) ret = true;
				else ret = false;
			}
		}

		console.log(ret);

		if (ret == true) form.submit();
		else return false;
	}

	function increment(id,valor,increment) {
	// Computador completo (video+cpu+teclado+rato+cabos): Máx. 15
	if (id == 'item_138') {
		if (valor < 15) {
			document.getElementById(id).value = parseInt(valor) + increment;
		}
	}
	else if (id == 'psd_box_c_7' || id == 'psd_box_c_8') {
		document.getElementById(id).value = parseFloat(valor) + parseFloat(0.5);
	}
	else {
		document.getElementById(id).value = parseInt(valor) + increment;	
	}
}

function decrement(id,valor,increment) {
	if (valor > 0) {
		if (id == 'psd_box_c_7' || id == 'psd_box_c_8') {
			document.getElementById(id).value = parseFloat(valor) - parseFloat(0.5);
		}
		else {
			document.getElementById(id).value = parseInt(valor) - increment;
		}	
	}
}

function decrementDays(id,valor,increment) {
	if (valor > 7) {
		document.getElementById(id).value = parseInt(valor) - increment;
	}
}

function isCoin(e,src) {
	if(window.event) _TXT = e.keyCode;
	else if(e.which) _TXT = e.which;
	if(_TXT > 47 && _TXT < 58) {
		return true;
	}
	else { 
		if (_TXT != 8 && _TXT != 9 && _TXT != 46) { return false; }
		else return true;        
	}
}

function isNumber(e,src) {
	if(window.event) _TXT = e.keyCode;
	else if(e.which) _TXT = e.which;

	if(_TXT > 47 && _TXT < 58) {
		return true;
	}
	else { 
		if (_TXT != 8 && _TXT != 9 ) { return false; }
		else return true;        
	}
}


function isAlphaKey(e) {
	var k;
	document.all ? k = e.keyCode : k = e.which;
	return ((k > 64 && k < 91) || (k > 96 && k <= 231) || k == 8 || k == 32 || k == 0);
}

function newWindow(file,window) {
	msgWindow=open(file,window,'scrollbars=1,resizable=no,width=500,height=500');
	if (msgWindow.opener == null) { msgWindow.opener = self; }
}

function setForm(text,displaylink) {
	opener.document.form.lift_items.value = opener.document.form.lift_items.value + text+"\n";

	document.getElementById("ItemLink"+displaylink).style.display = "none";
	document.getElementById("Item"+displaylink).style.display = "block";
}

function Insurance_Yes() {
	var dml = document.forms['form'];
	var len = dml.elements.length;
	var i = 0;

	document.getElementById("SeguroItems").style.display = 'block';
	document.form.insurance_value.disabled = false;
	document.form.insurance_final.disabled = false;
	psd_hideErr('insurance');

	for(i=0; i<len; i++) {
		var elemento = dml.elements[i].name;

		if (String(elemento.substring(0,12))=='items_value_' || String(elemento.substring(0,12))=='boxes_value_' || String(elemento.substring(0,13))=='packed_value_') {
			dml.elements[i].disabled = false;
		}
	}
}

function Insurance_No() {
	var dml = document.forms['form'];
	var len = dml.elements.length;
	var i = 0;

	document.getElementById("SeguroItems").style.display = 'none';
	document.form.insurance_value.disabled = true;
	document.form.insurance_final.disabled = true;
	psd_hideErr('insurance_items');
	psd_hideErr('insurance');

	for(i=0; i<len; i++) {
		var elemento = dml.elements[i].name;

		if (String(elemento.substring(0,12))=='items_value_' || String(elemento.substring(0,12))=='boxes_value_' || String(elemento.substring(0,13))=='packed_value_') {
			dml.elements[i].disabled = true;
		}
	}
}

function Lift_Yes() {
	//document.form.lift_items.disabled = false;
	document.getElementById("ElevadorItems").style.display = 'block';
	psd_hideErr("lift");
}

function Lift_No() {
	//document.form.lift_items.disabled = true;
	document.getElementById("ElevadorItems").style.display = 'none';
	psd_hideErr("lift");
	psd_hideErr('lift_items');
}


function TotalInsurance() {
	var dml = document.forms['form'];
	var len = dml.elements.length;
	var i = 0;
	var soma_items = 0;
	var validar_items = false;
	var insurance_price = parseFloat(document.form.insurance_price.value.replace(',','.'));

	psd_hideErr('insurance_items');

	for(i=0; i<len; i++) {
		var elemento = dml.elements[i].name;
		if (String(elemento.substring(0,11))=='items_value' || String(elemento.substring(0,8))=='outros_o' || String(elemento.substring(0,11))=='boxes_value' || String(elemento.substring(0,12))=='packed_value') {
			if (dml.elements[i].value!=0) {
				var v = dml.elements[i].value.replace('.','');
				soma_items += parseFloat(v.replace(',','.'));
			}
		}
	}

	document.form.insurance_value.value = formatoMoeda(soma_items);

	//document.form.insurance_final.value = formatoMoeda(Math.round(soma_items*(insurance_price/100)));
	var novo = Math.round( soma_items * (insurance_price / 100) * Math.pow(10,1) ) / Math.pow(10,1);
	
	var debug = {
		"soma_items" : soma_items,
		"insurance_price" : insurance_price
	}

	console.log(debug);

	document.form.insurance_final.value = formatoMoeda(novo);
}

function formatoMoeda(num) {
	var x = 0;

	if(num < 0) {
		num = Math.abs(num);
		x = 1;
	}

	if(isNaN(num)) num = "0";
	cents = Math.floor((num*100+0.5)%100);
	num = Math.floor((num*100+0.5)/100).toString();

	if(cents < 10) cents = "0" + cents;

	for (var i = 0; i < Math.floor((num.length-(1+i))/3); i++)
		num = num.substring(0,num.length-(4*i+3))+'.'
	+num.substring(num.length-(4*i+3));
	ret = num + ',' + cents;
	
	if (x == 1) ret = ' - ' + ret;
	return ret;
}

function Pesquisa() {
	var dml = document.forms['form'];
	var len = dml.elements.length;

	var i = 0;
	var checado_q1 = false;
	var checado_q2 = false;
	var checado_q3 = false;
	var checado_q4 = false;
	var checado_q5 = false;
	var validar_outros_q5 = false;
	var ret = true;

	for(i=0; i<len; i++) {
		var elemento = dml.elements[i].name;

		if (elemento == 'question_1') {
			if (dml.elements[i].checked) { checado_q1 = true; }
		}

		if (elemento == 'question_2') {
			if (dml.elements[i].checked) { checado_q2 = true; }
		}

		if (elemento == 'question_3') {
			if (dml.elements[i].checked) { checado_q3 = true; }
		}

		if (elemento == 'question_4') {
			if (dml.elements[i].checked) { checado_q4 = true; }
		}

		if (elemento == 'question_5') {
			if (dml.elements[i].checked) {
				var checado_q5 = true;
				if (dml.elements[i].value == '9') { validar_outros_q5 = true; }
			}
		}
		if (elemento == 'email') {
			if (dml.elements[i].value.match("([^@]+)@([^\\.]+)\\.(.*)")) psd_hideErr('email');
			else {
				ret = false;
				psd_showErr('email');
			}
		}
	}

	if (checado_q1 == false) { psd_showErr('q1'); ret = false; }
	else { psd_hideErr('q1'); }

	if (checado_q2 == false) { psd_showErr('q2'); ret = false; }
	else { psd_hideErr('q2'); }

	if (checado_q3 == false) { psd_showErr('q3'); ret = false; }
	else { psd_hideErr('q3'); }

	if (checado_q4 == false) { psd_showErr('q4'); ret = false; }
	else { psd_hideErr('q4'); }

	if (checado_q5 == false) { psd_showErr('q5'); ret = false; }
	else { psd_hideErr('q5'); }

	if (validar_outros_q5 == true) {
		if (document.form.textarea_5_9.value == '') { psd_showErr('textarea_5_9'); ret = false; }
		else psd_hideErr('textarea_5_9');
	}

	if (document.form.nome.value == '') { psd_showErr('nome'); ret = false; }
	else { psd_hideErr('nome'); }

	if (ret == true) { dml.submit(); }
	else { return false; }
}