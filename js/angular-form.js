(function () {
    'use strict';

    //region vanillajs
    function notify (message, type, theme) {
        if (!theme) {
            theme = 'mint';
        }

        new Noty({
            text: message,
            type: type,
            theme: theme
        }).show();
    }

    function isEmail (email) {
        var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
        return regex.test(email);
    }

    function isTouchDevice () {
        return true == ("ontouchstart" in window || window.DocumentTouch && document instanceof DocumentTouch);
    }

    //endregion

    angular.module(
        'app',
        [
            'ui.mask'
        ]
    )

    //.factory(
    //    'httpRequestInterceptor',
    //    httpRequestInterceptor
    //)
    //
    //.factory(
    //    'ApiEndpoint',
    //    ApiEndpoint
    //)

        .controller(
            'Step2Ctrl',
            Step2Ctrl
        )

        .controller(
            'Step1Ctrl',
            Step1Ctrl
        )

        .config(config)

        .run(run);

    config.$inject = [
        '$httpProvider'
    ];

    function config ($httpProvider) {
        //$httpProvider.interceptors.push('httpRequestInterceptor');
    }

    run.$inject = [
        '$rootScope'
    ];

    function run ($rootScope) {
        if ($(window).width() < 768) {
            $rootScope.breakpoint = 'sm';
        }
        else if ($(window).width() >= 768 && $(window).width() <= 992) {
            $rootScope.breakpoint = 'md';
        }
        else if ($(window).width() > 992 && $(window).width() <= 1200) {
            $rootScope.breakpoint = 'lg';
        }
        else {
            // do something for huge screens
        }
    }

    //region services
    //httpRequestInterceptor.$inject = [
    //    '$rootScope',
    //    '$q'
    //];
    //
    //function httpRequestInterceptor ($rootScope, $q) {
    //    return {
    //        request: function (config) {
    //
    //            if (!$rootScope.totalLoading
    //                || $rootScope.totalLoading == 0
    //            ) {
    //                NProgress.start();
    //                $rootScope.totalLoading = 0;
    //            }
    //
    //            $rootScope.totalLoading += 1;
    //
    //            $rootScope.$broadcast('loading-started');
    //
    //            return config;
    //        },
    //        responseError: function (response) {
    //            if ($rootScope.totalLoading == 1) {
    //                NProgress.done(true);
    //            }
    //
    //            $rootScope.totalLoading -= 1;
    //
    //            $rootScope.$broadcast('loading-error');
    //
    //            return $q.reject(response);
    //        },
    //        response: function (response) {
    //            if ($rootScope.totalLoading == 1) {
    //                NProgress.done(true);
    //            }
    //
    //            $rootScope.totalLoading -= 1;
    //
    //            $rootScope.$broadcast('loading-complete');
    //            return response || $q.when(response);
    //        }
    //    };
    //}

    //function ApiEndpoint () {
    //    return document.querySelector('[type=hidden][name=base_url]').value;
    //}
    //endregion

    //region controllers
    Step2Ctrl.$inject = [
        '$scope'
    ];

    function Step2Ctrl ($scope) {

        var vm = this;

        vm.addCaixasEmbaladas = addCaixasEmbaladas;
        vm.removeCaixasEmbaladas = removeCaixasEmbaladas;

        vm.addOutrosItens = addOutrosItens;
        vm.removeOutrosItens = removeOutrosItens;

        vm.checkLimit = checkLimit;
        vm.selectKit = selectKit;

        vm.getM3 = getM3;
        vm.getTotal = getTotal;

        init();

        return vm;

        function init() {
            vm.caixasEmbaladas = [
                {
                    id: 1
                }
            ];

            vm.outrosItens = [
                {
                    id: 1
                }
            ];

            vm.discount = $('[name=discount]').last().val() || 0;
            vm.m3Price = $('[name=price_m3]').last().val() || 0;
            vm.m3Limit = $('[name=limit_m3]').last().val();

            vm.notifyM3Limit = false;
        }

        function addCaixasEmbaladas () {
            var last = _.last(vm.caixasEmbaladas);
            vm.caixasEmbaladas.push({
                id: last.id + 1
            });
        }

        function removeCaixasEmbaladas (id) {
            vm.caixasEmbaladas = _.reject(vm.caixasEmbaladas, function (item) {
                return item.id == id;
            });
        }

        function addOutrosItens () {
            var last = _.last(vm.outrosItens);
            vm.outrosItens.push({
                id: last.id + 1
            });
        }

        function removeOutrosItens (id) {
            vm.outrosItens = _.reject(vm.outrosItens, function (item) {
                return item.id == id;
            });
        }

        function checkLimit() {
            if (!vm.notifyM3Limit
                && getM3() > vm.m3Limit
            ) {
                notify('A sua mudança excede o limite de ' + vm.m3Limit + 'm³, será remetido ao site da CityMover.', 'info', 'sunset')
                vm.notifyM3Limit = true;
            }
        }

        function selectKit(kit) {
            if (vm.selectedKit
                && vm.selectedKit.id == kit.id
            ) {
                kit = null;
            }

            vm.selectedKit = kit;
        }

        function getM3() {
            var m3 = 0;

            _.each(vm.items, function (items, categoria) {
                _.each(items, function (quantity, item) {
                    var itemData = $('[name=item_' + item + ']').data('item');
                    m3 += parseFloat(itemData.m3) * quantity;
                });
            });

            _.each(vm.outrosItens, function (item) {
                if (item.quantity > 0
                    && item.largura > 0
                    && item.comprimento > 0
                    && item.altura > 0
                ) {
                    m3 += parseFloat((item.largura * item.comprimento * item.altura) * item.quantity);
                }
            });

            _.each(vm.caixasEmbaladas, function (caixa) {
                if (caixa.quantity > 0
                    && caixa.largura > 0
                    && caixa.comprimento > 0
                    && caixa.altura > 0
                ) {
                    m3 += parseFloat((caixa.largura * caixa.comprimento * caixa.altura) * caixa.quantity);
                }
            });

            return m3 || 0;
        }

        function getTotal(withDiscount) {
            var total = (vm.m3Price * getM3()) + (vm.selectedKit ? parseFloat(vm.selectedKit.price) : 0);

            if (withDiscount) {
                //total = total - total * vm.discount / 100;
            }

            return total || 0;
        }
    }

    Step1Ctrl.$inject = [
        '$scope'
    ];

    function Step1Ctrl ($scope) {

        var vm = this;

        init();

        return vm;

        function init() {
            vm.zones = angular.fromJson($('[name=zones]').val());

            vm.notifyPostal = false;
            vm.invalidZone = false;

            _.each(['vm.dest_zip', 'vm.dep_zip'], function (item) {
                $scope.$watch(item, function (val) {
                    if (!val) return;
                    var zip = parseInt(val.substr(0, 4));

                    var zone = _.find(vm.zones, function (z) {
                        console.log(zip);
                        return zip >= parseInt(z.start.substr(0, 4))
                            && zip <= parseInt(z.end.substr(0, 4));
                    });

                    if (!zone) {
                        notify('Código Postal fora da área de atuação, será remetido ao site da CityMover.', 'info', 'sunset')
                        vm.notifyPostal = true;
                        vm.invalidZone = true;
                    } else {
                        vm.invalidZone = false;
                    }
                });
            });
        }
    }

    //endregion
})();
