<?php
define('BASE_PATH', __DIR__);
define('DISCOUNT', 10);
?>
<?php require_once("includes/core.php"); $core = new core; ?>
<!DOCTYPE html>
<html lang="pt" <?php if ($_GET['mod'] == 'form'): ?>ng-app="app" <?php endif; ?>>
<head>
    <meta charset="utf-8">
    <title>cityMover // Worten</title>
    <meta name="author" content="Bright Digital Agency">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.3/css/bootstrap.min.css" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.7.1/css/bootstrap-datepicker.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.7.1/css/bootstrap-datepicker3.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/css/select2.min.css" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/noty/3.1.4/noty.min.css" />
    <link href="https://fonts.googleapis.com/css?family=Lato:400,700" rel="stylesheet" />
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="<?php echo base_url("css/bright.css"); ?>">
    <link rel="stylesheet" href="<?php echo base_url("css/style.css"); ?>">
    <link rel="stylesheet" href="<?php echo base_url("css/citymover.css"); ?>">
    <link rel="stylesheet" href="<?php echo base_url("css/citymover-override.css"); ?>">
    <?php foreach ($core->mod_data->assets['css'] as $css): ?>
        <link rel="stylesheet" href="<?php echo $css; ?>">
    <?php endforeach; ?>
</head>

<div id="notify-messages">
    <?php tools::notify_list(); ?>
</div>

<body class="site-lang-<?= core::lang(); ?>">

<!--[if lte IE 8]>
<p class="chromeframe">You are using an <strong>outdated</strong> browser. Please <a href="//browsehappy.com/">upgrade your browser</a>.</p>
<![endif]-->

<div class="layer"></div>
<!-- Mobile menu overlay mask -->
<!-- Header================================================== -->

<header>
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div id="logo">
                    <a href="https://www.worten.pt" target="_blank" class="logo-partner">
                        <img class="img-responsive" src="<?= base_url('images/logo-worten.svg'); ?>" alt="Worten" data-retina="true">
                    </a>
                    <a href="<?php echo base_url(); ?>" class="logo-citymover ml-20 pull-right">
                        <img class="img-responsive" src="<?= base_url('images/logo-citymover-white.png'); ?>" alt="cityMover" data-retina="true">
                    </a>
                </div>
            </div>
        </div>
    </div>
</header>

<?php $view = empty($core->mod_data->view) ? $core->mod : $core->mod_data->view ?>
<?php echo $core->load_view("includes/views/" . $view . ".php", $core->mod_data) ?>

<div id="copy">
    <div class="container">
        <p class="text-center">© <?php echo date('Y'); ?> <strong>cityMover</strong> - Mudanças, Armazenagem, Tecnologia e Logística</p>
        <p class="text-center"><a href="https://www.bright.pt" target="_blank"><img src="<?php echo base_url('images/bright-logo.png'); ?>" alt="Bright - Digital Agency" title="Bright - Digital Agency"></a></p>
    </div>
</div>

<div id="notify-messages" class="hide">
    <?php tools::notify_list(); ?>
</div>

<input type="hidden" name="base_url" id="base_url" value="<?php echo base_url() ?>">

<?php
$sql = "select home from prices where id = 1";
$price_m3 = current(mysql_fetch_array(mysql_query($sql)));
?>
<input type="hidden" name="price_m3" value="<?php echo $price_m3; ?>">
<input type="hidden" name="limit_m3" value="20">
<input type="hidden" name="discount" value="<?php echo DISCOUNT; ?>">

<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.13.0/umd/popper.min.js"></script>
<script src="https://code.jquery.com/jquery-3.2.1.min.js" integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4=" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.3/js/bootstrap.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/js/select2.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/js/i18n/pt.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/noty/3.1.4/noty.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.7.1/js/bootstrap-datepicker.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.7.1/locales/bootstrap-datepicker.pt.min.js"></script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBCnrXcsGYZIwOwLvsjSR0Kb1vquUovsWw&libraries=visualization"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/underscore.js/1.8.3/underscore-min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/angular.js/1.6.5/angular.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.transit/0.9.12/jquery.transit.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/angular-ui-mask/1.8.7/mask.min.js"></script>
<script src="<?php echo base_url('js/init.js'); ?>"></script>
<script src="<?php echo base_url("js/citymover.old.js") ?>"></script>


<?php foreach ($core->mod_data->assets['js'] as $js): ?>
    <script src="<?php echo $js; ?>"></script>
<?php endforeach; ?>

</body>
</html>
